package com.example.data.interceptor

import com.example.domain.managers.SessionManager
import okhttp3.Interceptor
import okhttp3.Response

class SessionInterceptor constructor(
    private val sessionManager: SessionManager
) : Interceptor {

  override fun intercept(chain: Interceptor.Chain): Response {
    return chain.proceed(chain.request())
  }
}