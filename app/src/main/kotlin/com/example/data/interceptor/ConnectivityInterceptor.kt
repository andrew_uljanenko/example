package com.example.data.interceptor

import com.example.util.ConnectivityInfoHelper
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.ConnectException

class ConnectivityInterceptor : Interceptor {

  @Throws(IOException::class)
  override fun intercept(chain: Interceptor.Chain): Response {
    if (!ConnectivityInfoHelper.isConnected()) {
      throw ConnectException("No internet!")
    }

    return chain.proceed(chain.request())
  }
}