package com.example.data

import com.example.data.UrlConstants.BANNERS
import com.example.data.UrlConstants.BRANDS
import com.example.data.UrlConstants.CATEGORIES
import com.example.data.UrlConstants.HEROES
import com.example.data.UrlConstants.NEW_PRODUCTS
import com.example.data.UrlConstants.RECOMMENDED_PRODUCTS
import com.example.data.model.banners.BannersResponse
import com.example.data.model.brands.BrandsResponse
import com.example.data.model.categories.CategoriesResponse
import com.example.data.model.hero.HeroesResponse
import com.example.data.model.products.ProductsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

  @GET(BANNERS)
  fun getBanners(): Single<BannersResponse>

  @GET(CATEGORIES)
  fun getCategories(): Single<CategoriesResponse>

  @GET(RECOMMENDED_PRODUCTS)
  fun getRecommendedProducts(@Query("id") id: String): Single<ProductsResponse>

  @GET(NEW_PRODUCTS)
  fun getNewProducts(): Single<ProductsResponse>

  @GET(HEROES)
  fun getHeroes(): Single<HeroesResponse>

  @GET(BRANDS)
  fun getBrands(): Single<BrandsResponse>
}