package com.example.data

import com.example.BuildConfig.*

object UrlConstants {

  const val BANNERS = "$API_VERSION/banners"
  const val CATEGORIES = "$API_VERSION/goods-groups"
  const val RECOMMENDED_PRODUCTS = "$API_VERSION/goods/recommended/{id}"
  const val NEW_PRODUCTS = "$API_VERSION/goods/new"
  const val HEROES = "$API_VERSION/heroes"
  const val BRANDS = "$API_VERSION/brands"
}