package com.example.data.model.hero

import com.example.data.model.common.BaseListResponse
import com.example.data.model.common.MetaResponse
import com.example.data.model.common.ObjectStatus

class HeroesResponse(
    status: ObjectStatus?,
    data: List<HeroEntity>?,
    meta: MetaResponse?
) : BaseListResponse<HeroEntity, ObjectStatus>(status, data, meta)