package com.example.data.model.products

data class Image(
    val src: String
)