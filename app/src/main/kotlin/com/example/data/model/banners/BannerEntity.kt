package com.example.data.model.banners

data class BannerEntity(
    val id: Int,
    val bannerType: String?,
    val bannerReferenceId: Int?,
    val bannerTypeId: Int?,
    val imageSrc: String?,
    val link: String?,
    val nameButton: String?
)