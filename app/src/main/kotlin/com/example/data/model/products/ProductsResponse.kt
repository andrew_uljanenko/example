package com.example.data.model.products

import com.example.data.model.common.BaseListResponse
import com.example.data.model.common.MetaResponse
import com.example.data.model.common.ObjectStatus

class ProductsResponse(
    status: ObjectStatus?,
    data: List<ProductEntity>?,
    meta: MetaResponse?
) : BaseListResponse<ProductEntity, ObjectStatus>(status, data, meta)