package com.example.data.model.common

abstract class BaseResponse<out D, out R : ResponseStatus>(
    val status: R?,
    val data: D?,
    val meta: MetaResponse?
)