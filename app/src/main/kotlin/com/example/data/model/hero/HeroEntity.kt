package com.example.data.model.hero

data class HeroEntity(
    val id: Long,
    val name: String?,
    val description: String?,
    val smallImg: String?,
    val bigImg: String?
)