package com.example.data.model.categories

data class CategoryEntity(
    val id: Long,
    val name: String?,
    val description: String?,
    val imageSrc: String?
)