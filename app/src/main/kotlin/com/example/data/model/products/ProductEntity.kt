package com.example.data.model.products

data class ProductEntity(
    val id: Long,
    val name: String?,
    val article: String?,
    val brandId: Int?,
    val categoryId: Int?,
    val code: Int?,
    val description: String?,
    val heroId: Int?,
    val images: List<Image>?,
    val mainImageSrc: String?,
    val price: Int?,
    val priceOld: Int?,
    val rating: Int?,
    val reviews: Int?,
    val videoPromoLink: String?,
    val videoReviewLink: String?,
    val videoUnpackingLink: String?
)