package com.example.data.model.categories

import com.example.data.model.common.BaseListResponse
import com.example.data.model.common.MetaResponse
import com.example.data.model.common.ObjectStatus

class CategoriesResponse(
    status: ObjectStatus?,
    data: List<CategoryEntity>?,
    meta: MetaResponse?
) : BaseListResponse<CategoryEntity, ObjectStatus>(status, data, meta)