package com.example.data.model.banners

import com.example.data.model.common.BaseListResponse
import com.example.data.model.common.MetaResponse
import com.example.data.model.common.ObjectStatus

class BannersResponse(
    status: ObjectStatus?,
    data: List<BannerEntity>?,
    meta: MetaResponse?
) : BaseListResponse<BannerEntity, ObjectStatus>(status, data, meta)