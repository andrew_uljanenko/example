package com.example.data.model.brands

data class BrandEntity(
    val id: Int,
    val name: String?,
    val description: String?,
    val image: String?,
    val text: String?
)