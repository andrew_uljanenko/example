package com.example.data.model.common

abstract class BaseListResponse<out D, out R : ResponseStatus>
constructor(
    status: R?,
    data: List<D>?,
    meta: MetaResponse?
) : BaseResponse<List<D>, R>(status, data, meta)