package com.example.data.model.common

sealed class ResponseStatus

data class ObjectStatus(
    val status: Int,
    val message: String? = null,
    val uniqueErrorId: String? = null,
    val messageCode: String? = null
) : ResponseStatus()

data class SimpleStringStatus(
    val status: String
) : ResponseStatus()