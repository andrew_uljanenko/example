package com.example.data.model.common

data class MetaResponse(
    val itemsCount: Int?,
    val page: Int?,
    val perPage: Int?,
    val totalPages: Int?
)