package com.example.data.model.brands

import com.example.data.model.common.BaseListResponse
import com.example.data.model.common.MetaResponse
import com.example.data.model.common.ObjectStatus

class BrandsResponse(
    status: ObjectStatus?,
    data: List<BrandEntity>?,
    meta: MetaResponse?
) : BaseListResponse<BrandEntity, ObjectStatus>(status, data, meta)