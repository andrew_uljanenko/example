package com.example

import android.app.Activity
import androidx.multidex.MultiDexApplication
import com.example.di.ApiModule
import com.example.di.AppComponent
import com.example.di.DaggerAppComponent
import com.example.di.NavigationModule
import com.example.util.ConnectivityInfoHelper
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class ExampleApplication : MultiDexApplication(), HasActivityInjector {

  @Inject
  lateinit var androidInjector: DispatchingAndroidInjector<Activity>

  private val apiModule = ApiModule(BuildConfig.BASE_API_URL)
  private val navigationModule = NavigationModule()

  val appComponent: AppComponent by lazy {
    DaggerAppComponent
      .builder()
      .application(this)
      .apiModule(apiModule)
      .navigationModule(navigationModule)
      .build()
  }

  override fun onCreate() {
    super.onCreate()

    injectDependencies()
    initCollectivityHelper()
//    initLeakCanary()
    initStetho()
  }

  private fun injectDependencies() {
    appComponent.inject(this)
  }

  private fun initCollectivityHelper() {
    ConnectivityInfoHelper.initialize(this)
  }

  private fun initLeakCanary() {
    if (BuildConfig.DEBUG && !LeakCanary.isInAnalyzerProcess(this)) {
      LeakCanary.install(this)
    }
  }

  private fun initStetho() {
    if (BuildConfig.DEBUG) {
      Stetho.initializeWithDefaults(this)
    }
  }

  override fun activityInjector() = AndroidInjector<Activity> { activity ->
    androidInjector.inject(activity)
  }
}