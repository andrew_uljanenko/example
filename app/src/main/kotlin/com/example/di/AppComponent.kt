package com.example.di

import com.example.ExampleApplication
import com.example.di.module.ActivityBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AndroidSupportInjectionModule::class,
    ActivityBindingModule::class,
    AppModule::class,
    ApiModule::class,
    NavigationModule::class
  ]
)
interface AppComponent {

  fun inject(application: ExampleApplication)


  @Component.Builder
  interface Builder {

    @BindsInstance
    fun application(app: ExampleApplication): Builder

    fun apiModule(module: ApiModule): Builder

    fun navigationModule(module: NavigationModule): Builder

    fun build(): AppComponent
  }
}