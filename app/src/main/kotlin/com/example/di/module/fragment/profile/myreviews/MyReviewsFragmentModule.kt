package com.example.di.module.fragment.profile.myreviews

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.myreviews.MyReviewsFragment
import com.example.presentation.view.profile.myreviews.MyReviewsViewModel
import com.example.presentation.view.profile.myreviews.MyReviewsViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class MyReviewsFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = MyReviewsViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: MyReviewsFragment,
      viewModelFactory: MyReviewsViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(MyReviewsViewModel::class.java)
}