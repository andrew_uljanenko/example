package com.example.di.module.fragment

import com.example.di.module.fragment.authorize.AuthorizeFragmentModule
import com.example.di.module.fragment.authorize.confirmationemail.ConfirmationEmailFragmentModule
import com.example.di.module.fragment.authorize.confirmationphone.ConfirmationPhoneFragmentModule
import com.example.di.module.fragment.authorize.registration.additionalinfo.AdditionalInfoFragmentModule
import com.example.di.module.fragment.authorize.registration.email.RegistrationEmailFragmentModule
import com.example.di.module.fragment.authorize.registration.phone.RegistrationPhoneFragmentModule
import com.example.di.module.fragment.authorize.signin.changepassword.ChangePasswordFragmentModule
import com.example.di.module.fragment.authorize.signin.email.SignInEmailFragmentModule
import com.example.di.module.fragment.authorize.signin.phone.SignInPhoneFragmentModule
import com.example.di.module.fragment.authorize.successaction.SuccessActionFragmentModule
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.AuthorizeFragment
import com.example.presentation.view.authorize.confirmationemail.ConfirmationEmailFragment
import com.example.presentation.view.authorize.confirmationphone.ConfirmationPhoneFragment
import com.example.presentation.view.authorize.registration.additionalinfo.AdditionalInfoFragment
import com.example.presentation.view.authorize.registration.email.RegistrationEmailFragment
import com.example.presentation.view.authorize.registration.phone.RegistrationPhoneFragment
import com.example.presentation.view.authorize.signin.changepassword.ChangePasswordFragment
import com.example.presentation.view.authorize.signin.email.SignInEmailFragment
import com.example.presentation.view.authorize.signin.phone.SignInPhoneFragment
import com.example.presentation.view.authorize.successaction.SuccessActionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface AuthorizeActivityBindingModule {

  @FragmentScope
  @ContributesAndroidInjector(modules = [SignInEmailFragmentModule::class])
  fun contributeSignInEmailFragment(): SignInEmailFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [SignInPhoneFragmentModule::class])
  fun contributeSignInPhoneFragment(): SignInPhoneFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [ChangePasswordFragmentModule::class])
  fun contributeChangePasswordFragment(): ChangePasswordFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [AuthorizeFragmentModule::class])
  fun contributeRegistrationFragment(): AuthorizeFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [RegistrationEmailFragmentModule::class])
  fun contributeRegistrationEmailFragment(): RegistrationEmailFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [RegistrationPhoneFragmentModule::class])
  fun contributeRegistrationPhoneFragment(): RegistrationPhoneFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [AdditionalInfoFragmentModule::class])
  fun contributeAdditionalInfoFragment(): AdditionalInfoFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [ConfirmationPhoneFragmentModule::class])
  fun contributeConfirmationPhoneFragment(): ConfirmationPhoneFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [ConfirmationEmailFragmentModule::class])
  fun contributeConfirmationEmailFragment(): ConfirmationEmailFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [SuccessActionFragmentModule::class])
  fun contributeSuccessActionFragment(): SuccessActionFragment
}