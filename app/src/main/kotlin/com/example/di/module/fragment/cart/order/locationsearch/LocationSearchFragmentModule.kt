package com.example.di.module.fragment.cart.order.locationsearch

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.cart.order.locationsearch.LocationSearchFragment
import com.example.presentation.view.cart.order.locationsearch.LocationSearchViewModel
import com.example.presentation.view.cart.order.locationsearch.LocationSearchViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class LocationSearchFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = LocationSearchViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: LocationSearchFragment,
      viewModelFactory: LocationSearchViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(LocationSearchViewModel::class.java)
}