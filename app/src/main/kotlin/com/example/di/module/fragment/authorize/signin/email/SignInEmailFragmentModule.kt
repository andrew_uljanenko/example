package com.example.di.module.fragment.authorize.signin.email

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.signin.email.SignInEmailFragment
import com.example.presentation.view.authorize.signin.email.SignInEmailViewModel
import com.example.presentation.view.authorize.signin.email.SignInEmailViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SignInEmailFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = SignInEmailViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: SignInEmailFragment,
      viewModelFactory: SignInEmailViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(SignInEmailViewModel::class.java)
}