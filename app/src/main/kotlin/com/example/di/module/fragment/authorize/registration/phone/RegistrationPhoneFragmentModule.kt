package com.example.di.module.fragment.authorize.registration.phone

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.registration.phone.RegistrationPhoneFragment
import com.example.presentation.view.authorize.registration.phone.RegistrationPhoneViewModel
import com.example.presentation.view.authorize.registration.phone.RegistrationPhoneViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class RegistrationPhoneFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = RegistrationPhoneViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: RegistrationPhoneFragment,
      viewModelFactory: RegistrationPhoneViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(RegistrationPhoneViewModel::class.java)
}