package com.example.di.module.fragment.cart.order.delivery

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.cart.CartManager
import com.example.presentation.view.cart.order.delivery.DeliveryFragment
import com.example.presentation.view.cart.order.delivery.DeliveryViewModel
import com.example.presentation.view.cart.order.delivery.DeliveryViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class DeliveryFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService,
      cartManager: CartManager
  ) = DeliveryViewModelFactory(apiService, cartManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: DeliveryFragment,
      viewModelFactory: DeliveryViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(DeliveryViewModel::class.java)
}