package com.example.di.module.fragment.profile.emailsettings

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.emailsettings.EmailSettingsFragment
import com.example.presentation.view.profile.emailsettings.EmailSettingsViewModel
import com.example.presentation.view.profile.emailsettings.EmailSettingsViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class EmailSettingsFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = EmailSettingsViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: EmailSettingsFragment,
      viewModelFactory: EmailSettingsViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(EmailSettingsViewModel::class.java)
}