package com.example.di.module.activity

import androidx.lifecycle.ViewModelProviders
import com.example.di.module.fragment.ScanBarcodeActivityBindingModule
import com.example.di.scope.ActivityScope
import com.example.presentation.view.scanbarcode.ScanBarcodeActivity
import com.example.presentation.view.scanbarcode.ScanBarcodeViewModel
import com.example.presentation.view.scanbarcode.ScanBarcodeViewModelFactory
import dagger.Module
import dagger.Provides

@Module(includes = [ScanBarcodeActivityBindingModule::class])
class ScanBarcodeActivityModule {

  @Provides
  @ActivityScope
  fun provideViewModelFactory() = ScanBarcodeViewModelFactory()

  @Provides
  @ActivityScope
  fun provideViewModel(
      activity: ScanBarcodeActivity,
      viewModelFactory: ScanBarcodeViewModelFactory
  ) = ViewModelProviders.of(activity, viewModelFactory).get(ScanBarcodeViewModel::class.java)
}