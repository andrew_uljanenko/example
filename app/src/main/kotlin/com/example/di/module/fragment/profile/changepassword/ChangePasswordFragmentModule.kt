package com.example.di.module.fragment.profile.changepassword

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.changepassword.ChangePasswordFragment
import com.example.presentation.view.profile.changepassword.ChangePasswordViewModel
import com.example.presentation.view.profile.changepassword.ChangePasswordViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ChangePasswordFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ChangePasswordViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: ChangePasswordFragment,
      viewModelFactory: ChangePasswordViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ChangePasswordViewModel::class.java)
}