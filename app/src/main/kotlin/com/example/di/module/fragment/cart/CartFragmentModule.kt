package com.example.di.module.fragment.cart

import androidx.lifecycle.ViewModelProviders
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.cart.CartManager
import com.example.presentation.view.cart.CartFragment
import com.example.presentation.view.cart.CartViewModel
import com.example.presentation.view.cart.CartViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class CartFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      cartManager: CartManager
  ) = CartViewModelFactory(cartManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: CartFragment,
      viewModelFactory: CartViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(CartViewModel::class.java)
}