package com.example.di.module.fragment.profile.ordershistory

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.ordershistory.OrdersHistoryFragment
import com.example.presentation.view.profile.ordershistory.OrdersHistoryViewModel
import com.example.presentation.view.profile.ordershistory.OrdersHistoryViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class OrdersHistoryFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = OrdersHistoryViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: OrdersHistoryFragment,
      viewModelFactory: OrdersHistoryViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(OrdersHistoryViewModel::class.java)
}