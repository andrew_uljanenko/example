package com.example.di.module.fragment.profile.viewedproducts

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.viewedproducts.ViewedProductsFragment
import com.example.presentation.view.profile.viewedproducts.ViewedProductsViewModel
import com.example.presentation.view.profile.viewedproducts.ViewedProductsViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ViewedProductsFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ViewedProductsViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: ViewedProductsFragment,
      viewModelFactory: ViewedProductsViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ViewedProductsViewModel::class.java)
}