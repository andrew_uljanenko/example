package com.example.di.module.activity

import com.example.R
import com.example.di.module.fragment.AuthorizeActivityBindingModule
import com.example.di.scope.ActivityScope
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.cicerone.androidx.AppNavigator
import com.example.presentation.view.authorize.AuthorizeActivity
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Named

@Module(includes = [AuthorizeActivityBindingModule::class])
class AuthorizeActivityModule {

  @Provides
  @ActivityScope
  @Named(AUTHORIZE_CICERONE)
  fun provideCicerone(): Cicerone<Router> {
    return Cicerone.create()
  }

  @Provides
  @ActivityScope
  @Named(AUTHORIZE_ROUTER)
  fun provideRouter(@Named(AUTHORIZE_CICERONE) cicerone: Cicerone<Router>): Router {
    return cicerone.router
  }

  @Provides
  @ActivityScope
  @Named(AUTHORIZE_NAVIGATOR)
  fun provideNavigator(activity: AuthorizeActivity): Navigator {
    return AppNavigator(activity, activity.supportFragmentManager, R.id.fragmentContainer)
  }

  @Provides
  @ActivityScope
  @Named(AUTHORIZE_CICERONE_MANAGER)
  fun provideCiceroneManager(
      @Named(AUTHORIZE_CICERONE) cicerone: Cicerone<Router>,
      @Named(AUTHORIZE_NAVIGATOR) navigator: Navigator
  ): CiceroneManager {
    return CiceroneManager(cicerone, navigator)
  }

  companion object {
    const val AUTHORIZE_NAVIGATOR = "AUTHORIZE_NAVIGATOR"
    const val AUTHORIZE_CICERONE = "AUTHORIZE_CICERONE"
    const val AUTHORIZE_CICERONE_MANAGER = "AUTHORIZE_CICERONE_MANAGER"
    const val AUTHORIZE_ROUTER = "AUTHORIZE_ROUTER"
  }
}