package com.example.di.module.activity

import com.example.di.NavigationModule.Companion.APP_CICERONE
import com.example.di.scope.ActivityScope
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.cicerone.androidx.AppNavigator
import com.example.presentation.view.splash.SplashActivity
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Named

@Module
class SplashActivityModule {

  @Provides
  @ActivityScope
  fun provideNavigator(view: SplashActivity): Navigator {
    return AppNavigator(view)
  }

  @Provides
  @ActivityScope
  fun provideCiceroneManager(@Named(APP_CICERONE) cicerone: Cicerone<Router>, navigator: Navigator): CiceroneManager {
    return CiceroneManager(cicerone, navigator)
  }
}