package com.example.di.module.fragment

import com.example.di.module.fragment.scanbarcode.ScanResultModule
import com.example.di.scope.FragmentScope
import com.example.presentation.view.scanbarcode.resultdialog.ScanResultDialogFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ScanBarcodeActivityBindingModule {

  @FragmentScope
  @ContributesAndroidInjector(modules = [ScanResultModule::class])
  fun contributeScanResultDialogFragment(): ScanResultDialogFragment
}