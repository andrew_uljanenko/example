package com.example.di.module.fragment.cart.order

import androidx.lifecycle.ViewModelProviders
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.cart.CartManager
import com.example.presentation.view.cart.order.OrderFragment
import com.example.presentation.view.cart.order.OrderViewModel
import com.example.presentation.view.cart.order.OrderViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class OrderFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      cartManager: CartManager
  ) = OrderViewModelFactory(cartManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: OrderFragment,
      viewModelFactory: OrderViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(OrderViewModel::class.java)
}