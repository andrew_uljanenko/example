package com.example.di.module.fragment.cart

import com.example.R
import com.example.di.module.fragment.MainActivityChildFragmentsBindingModule
import com.example.di.scope.FragmentScope
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.cicerone.androidx.AppNavigator
import com.example.presentation.view.cart.CartFragmentContainerFragment
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Named

@Module(includes = [MainActivityChildFragmentsBindingModule::class])
class CartFragmentContainerModule {

  @Provides
  @FragmentScope
  @Named(CART_CICERONE)
  fun provideCicerone(): Cicerone<Router> {
    return Cicerone.create()
  }

  @Provides
  @FragmentScope
  @Named(CART_ROUTER)
  fun provideRouter(@Named(CART_CICERONE) cicerone: Cicerone<Router>): Router {
    return cicerone.router
  }

  @Provides
  @FragmentScope
  @Named(CART_NAVIGATION_HOLDER)
  fun provideNavigatorHolder(@Named(CART_CICERONE) cicerone: Cicerone<Router>): NavigatorHolder {
    return cicerone.navigatorHolder
  }

  @Provides
  @FragmentScope
  @Named(CART_NAVIGATOR)
  fun provideNavigator(view: CartFragmentContainerFragment): Navigator {
    return AppNavigator(view.activity!!, view.childFragmentManager, R.id.fragmentContainer)
  }

  @Provides
  @FragmentScope
  @Named(CART_CICERONE_MANAGER)
  fun provideCiceroneManager(
      @Named(CART_CICERONE) cicerone: Cicerone<Router>,
      @Named(CART_NAVIGATOR) navigator: Navigator
  ): CiceroneManager {
    return CiceroneManager(cicerone, navigator)
  }

  companion object {
    const val CART_CICERONE = "CART_CICERONE"
    const val CART_ROUTER = "CART_ROUTER"
    const val CART_NAVIGATION_HOLDER = "CART_NAVIGATION_HOLDER"
    const val CART_NAVIGATOR = "CART_NAVIGATOR"
    const val CART_CICERONE_MANAGER = "CART_CICERONE_MANAGER"
  }
}