package com.example.di.module.fragment.authorize.signin.changepassword

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.signin.changepassword.ChangePasswordFragment
import com.example.presentation.view.authorize.signin.changepassword.ChangePasswordViewModel
import com.example.presentation.view.authorize.signin.changepassword.ChangePasswordViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ChangePasswordFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ChangePasswordViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: ChangePasswordFragment,
      viewModelFactory: ChangePasswordViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ChangePasswordViewModel::class.java)
}