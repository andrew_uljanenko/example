package com.example.di.module.fragment.authorize.confirmationemail

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.confirmationemail.ConfirmationEmailFragment
import com.example.presentation.view.authorize.confirmationemail.ConfirmationEmailViewModel
import com.example.presentation.view.authorize.confirmationemail.ConfirmationEmailViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ConfirmationEmailFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ConfirmationEmailViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: ConfirmationEmailFragment,
      viewModelFactory: ConfirmationEmailViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ConfirmationEmailViewModel::class.java)
}