package com.example.di.module.activity

import com.example.di.NavigationModule.Companion.APP_CICERONE
import com.example.di.module.fragment.MainActivityBindingModule
import com.example.di.scope.ActivityScope
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.cicerone.androidx.AppNavigator
import com.example.presentation.view.main.MainActivity
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Named

@Module(includes = [MainActivityBindingModule::class])
class MainActivityModule {

  @Provides
  @ActivityScope
  @Named(MAIN_NAVIGATOR)
  fun provideNavigator(view: MainActivity): Navigator {
    return AppNavigator(view)
  }

  @Provides
  @ActivityScope
  @Named(MAIN_CICERONE_MANAGER)
  fun provideCiceroneManager(
      @Named(APP_CICERONE) cicerone: Cicerone<Router>,
      @Named(MAIN_NAVIGATOR) navigator: Navigator
  ): CiceroneManager {
    return CiceroneManager(cicerone, navigator)
  }

  companion object {
    const val MAIN_NAVIGATOR = "MAIN_NAVIGATOR"
    const val MAIN_CICERONE_MANAGER = "MAIN_CICERONE_MANAGER"
  }
}