package com.example.di.module.fragment.profile.childrenprofile

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.childrenprofile.ChildrenProfileFragment
import com.example.presentation.view.profile.childrenprofile.ChildrenProfileViewModel
import com.example.presentation.view.profile.childrenprofile.ChildrenProfileViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ChildrenProfileFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ChildrenProfileViewModelFactory(apiService)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: ChildrenProfileFragment,
      viewModelFactory: ChildrenProfileViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ChildrenProfileViewModel::class.java)
}