package com.example.di.module

import com.example.di.module.activity.AuthorizeActivityModule
import com.example.di.module.activity.MainActivityModule
import com.example.di.module.activity.ScanBarcodeActivityModule
import com.example.di.module.activity.SplashActivityModule
import com.example.di.scope.ActivityScope
import com.example.presentation.view.authorize.AuthorizeActivity
import com.example.presentation.view.main.MainActivity
import com.example.presentation.view.scanbarcode.ScanBarcodeActivity
import com.example.presentation.view.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

  @ActivityScope
  @ContributesAndroidInjector(modules = [SplashActivityModule::class])
  fun contributeSplashActivity(): SplashActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [MainActivityModule::class])
  fun contributeMainActivity(): MainActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [AuthorizeActivityModule::class])
  fun contributeAuthorizeActivity(): AuthorizeActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [ScanBarcodeActivityModule::class])
  fun contributeScanBarcodeActivity(): ScanBarcodeActivity
}