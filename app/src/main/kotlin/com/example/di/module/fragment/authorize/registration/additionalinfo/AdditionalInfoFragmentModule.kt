package com.example.di.module.fragment.authorize.registration.additionalinfo

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.registration.additionalinfo.AdditionalInfoFragment
import com.example.presentation.view.authorize.registration.additionalinfo.AdditionalInfoViewModel
import com.example.presentation.view.authorize.registration.additionalinfo.AdditionalInfoViewMoldeFactory
import dagger.Module
import dagger.Provides

@Module
class AdditionalInfoFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = AdditionalInfoViewMoldeFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: AdditionalInfoFragment,
      viewModelFactory: AdditionalInfoViewMoldeFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(AdditionalInfoViewModel::class.java)
}