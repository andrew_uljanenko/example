package com.example.di.module.fragment.authorize.registration.email

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.registration.email.RegistrationEmailFragment
import com.example.presentation.view.authorize.registration.email.RegistrationEmailViewModel
import com.example.presentation.view.authorize.registration.email.RegistrationEmailViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class RegistrationEmailFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = RegistrationEmailViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: RegistrationEmailFragment,
      viewModelFactory: RegistrationEmailViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(RegistrationEmailViewModel::class.java)
}