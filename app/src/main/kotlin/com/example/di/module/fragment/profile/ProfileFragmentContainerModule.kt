package com.example.di.module.fragment.profile

import com.example.R
import com.example.di.module.fragment.MainActivityChildFragmentsBindingModule
import com.example.di.scope.FragmentScope
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.cicerone.androidx.AppNavigator
import com.example.presentation.view.profile.ProfileFragmentContainerFragment
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Named

@Module(includes = [MainActivityChildFragmentsBindingModule::class])
class ProfileFragmentContainerModule {

  @Provides
  @FragmentScope
  @Named(PROFILE_CICERONE)
  fun provideCicerone(): Cicerone<Router> {
    return Cicerone.create()
  }

  @Provides
  @FragmentScope
  @Named(PROFILE_ROUTER)
  fun provideRouter(@Named(PROFILE_CICERONE) cicerone: Cicerone<Router>): Router {
    return cicerone.router
  }

  @Provides
  @FragmentScope
  @Named(PROFILE_NAVIGATION_HOLDER)
  fun provideNavigatorHolder(@Named(PROFILE_CICERONE) cicerone: Cicerone<Router>): NavigatorHolder {
    return cicerone.navigatorHolder
  }

  @Provides
  @FragmentScope
  @Named(PROFILE_NAVIGATOR)
  fun provideNavigator(view: ProfileFragmentContainerFragment): Navigator {
    return AppNavigator(view.activity!!, view.childFragmentManager, R.id.fragmentContainer)
  }

  @Provides
  @FragmentScope
  @Named(PROFILE_CICERONE_MANAGER)
  fun provideCiceroneManager(
      @Named(PROFILE_CICERONE) cicerone: Cicerone<Router>,
      @Named(PROFILE_NAVIGATOR) navigator: Navigator
  ): CiceroneManager {
    return CiceroneManager(cicerone, navigator)
  }

  companion object {
    const val PROFILE_CICERONE = "PROFILE_CICERONE"
    const val PROFILE_ROUTER = "PROFILE_ROUTER"
    const val PROFILE_NAVIGATION_HOLDER = "PROFILE_NAVIGATION_HOLDER"
    const val PROFILE_NAVIGATOR = "PROFILE_NAVIGATOR"
    const val PROFILE_CICERONE_MANAGER = "PROFILE_CICERONE_MANAGER"
  }
}