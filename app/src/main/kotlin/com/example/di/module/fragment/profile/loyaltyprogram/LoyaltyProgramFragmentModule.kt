package com.example.di.module.fragment.profile.loyaltyprogram

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.AppModule.Companion.APP_SHARED_PREFERENCES
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.profile.loyaltyprogram.LoyaltyProgramFragment
import com.example.presentation.view.profile.loyaltyprogram.LoyaltyProgramViewModel
import com.example.presentation.view.profile.loyaltyprogram.LoyaltyProgramViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class LoyaltyProgramFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService,
      @Named(APP_SHARED_PREFERENCES) sharedPreferences: SharedPreferences
  ) = LoyaltyProgramViewModelFactory(apiService, sharedPreferences)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: LoyaltyProgramFragment,
      viewModelFactory: LoyaltyProgramViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(LoyaltyProgramViewModel::class.java)
}