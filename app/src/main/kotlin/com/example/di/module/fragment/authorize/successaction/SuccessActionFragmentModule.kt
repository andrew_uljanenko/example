package com.example.di.module.fragment.authorize.successaction

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.successaction.SuccessActionFragment
import com.example.presentation.view.authorize.successaction.SuccessActionViewModel
import com.example.presentation.view.authorize.successaction.SuccessActionViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SuccessActionFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = SuccessActionViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: SuccessActionFragment,
      viewModelFactory: SuccessActionViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(SuccessActionViewModel::class.java)
}