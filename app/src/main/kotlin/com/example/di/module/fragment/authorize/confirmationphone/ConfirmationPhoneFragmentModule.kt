package com.example.di.module.fragment.authorize.confirmationphone

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.confirmationphone.ConfirmationPhoneFragment
import com.example.presentation.view.authorize.confirmationphone.ConfirmationPhoneViewModel
import com.example.presentation.view.authorize.confirmationphone.ConfirmationPhoneViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ConfirmationPhoneFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = ConfirmationPhoneViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: ConfirmationPhoneFragment,
      viewModelFactory: ConfirmationPhoneViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ConfirmationPhoneViewModel::class.java)
}