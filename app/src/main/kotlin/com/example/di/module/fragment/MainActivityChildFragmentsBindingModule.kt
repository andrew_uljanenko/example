package com.example.di.module.fragment

import com.example.di.module.fragment.cart.CartFragmentModule
import com.example.di.module.fragment.cart.order.OrderFragmentModule
import com.example.di.module.fragment.cart.order.buyerinfo.BuyerInfoFragmentModule
import com.example.di.module.fragment.cart.order.delivery.DeliveryFragmentModule
import com.example.di.module.fragment.cart.order.locationsearch.LocationSearchFragmentModule
import com.example.di.module.fragment.cart.order.payment.PaymentFragmentModule
import com.example.di.module.fragment.categories.CategoriesFragmentModule
import com.example.di.module.fragment.favourite.FavouriteFragmentModule
import com.example.di.module.fragment.home.HomeFragmentModule
import com.example.di.module.fragment.info.InfoFragmentModule
import com.example.di.module.fragment.info.contacts.ContactsFragmentModule
import com.example.di.module.fragment.info.contacts.callback.CallbackFragmentModule
import com.example.di.module.fragment.profile.ProfileFragmentModule
import com.example.di.module.fragment.profile.changepassword.ChangePasswordFragmentModule
import com.example.di.module.fragment.profile.childrenprofile.ChildrenProfileFragmentModule
import com.example.di.module.fragment.profile.emailsettings.EmailSettingsFragmentModule
import com.example.di.module.fragment.profile.loyaltyprogram.LoyaltyProgramFragmentModule
import com.example.di.module.fragment.profile.myreviews.MyReviewsFragmentModule
import com.example.di.module.fragment.profile.ordershistory.OrdersHistoryFragmentModule
import com.example.di.module.fragment.profile.viewedproducts.ViewedProductsFragmentModule
import com.example.di.module.fragment.search.SearchFragmentModule
import com.example.di.module.fragment.subcategories.SubcategoriesFragmentModule
import com.example.di.module.fragment.successaction.SuccessActionFragmentModule
import com.example.di.scope.ChildFragmentScope
import com.example.presentation.view.buyerinfo.BuyerInfoFragment
import com.example.presentation.view.cart.CartFragment
import com.example.presentation.view.cart.order.OrderFragment
import com.example.presentation.view.cart.order.delivery.DeliveryFragment
import com.example.presentation.view.cart.order.locationsearch.LocationSearchFragment
import com.example.presentation.view.cart.order.payment.PaymentFragment
import com.example.presentation.view.cart.order.successcreation.SuccessOrderCreationFragment
import com.example.presentation.view.categories.CategoriesFragment
import com.example.presentation.view.favourite.FavouriteFragment
import com.example.presentation.view.home.HomeFragment
import com.example.presentation.view.info.InfoFragment
import com.example.presentation.view.info.contacts.ContactsFragment
import com.example.presentation.view.info.contacts.callback.CallbackFragment
import com.example.presentation.view.profile.ProfileFragment
import com.example.presentation.view.profile.changepassword.ChangePasswordFragment
import com.example.presentation.view.profile.childrenprofile.ChildrenProfileFragment
import com.example.presentation.view.profile.emailsettings.EmailSettingsFragment
import com.example.presentation.view.profile.loyaltyprogram.LoyaltyProgramFragment
import com.example.presentation.view.profile.myreviews.MyReviewsFragment
import com.example.presentation.view.profile.ordershistory.OrdersHistoryFragment
import com.example.presentation.view.profile.viewedproducts.ViewedProductsFragment
import com.example.presentation.view.search.SearchFragment
import com.example.presentation.view.subcategories.SubcategoriesFragment
import com.example.presentation.view.successaction.SuccessActionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface MainActivityChildFragmentsBindingModule {

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
  fun contributeHomeFragment(): HomeFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [CartFragmentModule::class])
  fun contributeCartFragment(): CartFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [OrderFragmentModule::class])
  fun contributeOrderFragment(): OrderFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [BuyerInfoFragmentModule::class])
  fun contributeBuyerInfoFragment(): BuyerInfoFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [DeliveryFragmentModule::class])
  fun contributeDeliveryFragment(): DeliveryFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [PaymentFragmentModule::class])
  fun contributePaymentFragment(): PaymentFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [LocationSearchFragmentModule::class])
  fun contributeLocationSearchFragment(): LocationSearchFragment

  @ChildFragmentScope
  @ContributesAndroidInjector
  fun contributeSuccessOrderCreationFragment(): SuccessOrderCreationFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [SuccessActionFragmentModule::class])
  fun contributeSuccessActionFragment(): SuccessActionFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
  fun contributeProfileFragment(): ProfileFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [ChangePasswordFragmentModule::class])
  fun contributeChangePasswordFragment(): ChangePasswordFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [ChildrenProfileFragmentModule::class])
  fun contributeChildrenProfileFragment(): ChildrenProfileFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [LoyaltyProgramFragmentModule::class])
  fun contributeLoyaltyProgramFragment(): LoyaltyProgramFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [OrdersHistoryFragmentModule::class])
  fun contributeOrdersHistoryFragment(): OrdersHistoryFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [MyReviewsFragmentModule::class])
  fun contributeMyReviewsFragment(): MyReviewsFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [ViewedProductsFragmentModule::class])
  fun contributeViewedProductsFragment(): ViewedProductsFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [EmailSettingsFragmentModule::class])
  fun contributeEmailSettingsFragment(): EmailSettingsFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [InfoFragmentModule::class])
  fun contributeInfoFragment(): InfoFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [ContactsFragmentModule::class])
  fun contributeContactsFragment(): ContactsFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [CallbackFragmentModule::class])
  fun contributeCallbackFragment(): CallbackFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [FavouriteFragmentModule::class])
  fun contributeFavouriteFragment(): FavouriteFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [SearchFragmentModule::class])
  fun contributeSearchFragment(): SearchFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [CategoriesFragmentModule::class])
  fun contributeCategoriesFragment(): CategoriesFragment

  @ChildFragmentScope
  @ContributesAndroidInjector(modules = [SubcategoriesFragmentModule::class])
  fun contributeSubcategoriesFragment(): SubcategoriesFragment
}