package com.example.di.module.fragment.cart.order.buyerinfo

import androidx.lifecycle.ViewModelProviders
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.cart.CartManager
import com.example.presentation.view.buyerinfo.BuyerInfoFragment
import com.example.presentation.view.buyerinfo.BuyerInfoViewModel
import com.example.presentation.view.buyerinfo.BuyerInfoViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class BuyerInfoFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      cartManager: CartManager
  ) = BuyerInfoViewModelFactory(cartManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: BuyerInfoFragment,
      viewModelFactory: BuyerInfoViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(BuyerInfoViewModel::class.java)
}