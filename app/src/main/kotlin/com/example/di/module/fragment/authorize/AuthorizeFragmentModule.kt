package com.example.di.module.fragment.authorize

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.AuthorizeFragment
import com.example.presentation.view.authorize.AuthorizeViewModel
import com.example.presentation.view.authorize.AuthorizeViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class AuthorizeFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = AuthorizeViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: AuthorizeFragment,
      viewModelFactory: AuthorizeViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(AuthorizeViewModel::class.java)
}