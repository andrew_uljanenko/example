package com.example.di.module.fragment.profile

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.banner.BannersManager
import com.example.presentation.view.profile.ProfileFragment
import com.example.presentation.view.profile.ProfileViewModel
import com.example.presentation.view.profile.ProfileViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ProfileFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService,
      bannersManager: BannersManager
  ) = ProfileViewModelFactory(apiService, bannersManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: ProfileFragment,
      viewModelFactory: ProfileViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(ProfileViewModel::class.java)
}