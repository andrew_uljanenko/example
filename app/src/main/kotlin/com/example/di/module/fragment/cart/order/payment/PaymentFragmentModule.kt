package com.example.di.module.fragment.cart.order.payment

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.ChildFragmentScope
import com.example.domain.managers.cart.CartManager
import com.example.presentation.view.cart.order.payment.PaymentFragment
import com.example.presentation.view.cart.order.payment.PaymentViewModel
import com.example.presentation.view.cart.order.payment.PaymentViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class PaymentFragmentModule {

  @Provides
  @ChildFragmentScope
  fun provideViewModelFactory(
      apiService: ApiService,
      cartManager: CartManager
  ) = PaymentViewModelFactory(apiService, cartManager)

  @Provides
  @ChildFragmentScope
  fun provideViewModel(
      fragment: PaymentFragment,
      viewModelFactory: PaymentViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(PaymentViewModel::class.java)
}