package com.example.di.module.fragment.authorize.signin.phone

import androidx.lifecycle.ViewModelProviders
import com.example.data.ApiService
import com.example.di.scope.FragmentScope
import com.example.presentation.view.authorize.signin.phone.SignInPhoneFragment
import com.example.presentation.view.authorize.signin.phone.SignInPhoneViewModel
import com.example.presentation.view.authorize.signin.phone.SignInPhoneViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SignInPhoneFragmentModule {

  @Provides
  @FragmentScope
  fun provideViewModelFactory(
      apiService: ApiService
  ) = SignInPhoneViewModelFactory(apiService)

  @Provides
  @FragmentScope
  fun provideViewModel(
      fragment: SignInPhoneFragment,
      viewModelFactory: SignInPhoneViewModelFactory
  ) = ViewModelProviders.of(fragment, viewModelFactory).get(SignInPhoneViewModel::class.java)
}