package com.example.di.module.fragment

import com.example.di.module.fragment.cart.CartFragmentContainerModule
import com.example.di.module.fragment.favourite.FavouriteFragmentContainerModule
import com.example.di.module.fragment.home.HomeFragmentContainerModule
import com.example.di.module.fragment.info.InfoFragmentContainerModule
import com.example.di.module.fragment.profile.ProfileFragmentContainerModule
import com.example.di.scope.FragmentScope
import com.example.presentation.view.cart.CartFragmentContainerFragment
import com.example.presentation.view.favourite.FavouriteFragmentContainerFragment
import com.example.presentation.view.home.HomeFragmentContainerFragment
import com.example.presentation.view.info.InfoFragmentContainerFragment
import com.example.presentation.view.profile.ProfileFragmentContainerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface MainActivityBindingModule {

  @FragmentScope
  @ContributesAndroidInjector(modules = [HomeFragmentContainerModule::class])
  fun contributeHomeFragmentContainerFragment(): HomeFragmentContainerFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [FavouriteFragmentContainerModule::class])
  fun contributeFavouriteFragmentContainerFragment(): FavouriteFragmentContainerFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [CartFragmentContainerModule::class])
  fun contributeCartFragmentContainerFragment(): CartFragmentContainerFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [ProfileFragmentContainerModule::class])
  fun contributeProfileFragmentContainerFragment(): ProfileFragmentContainerFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [InfoFragmentContainerModule::class])
  fun contributeInfoFragmentContainerFragment(): InfoFragmentContainerFragment
}