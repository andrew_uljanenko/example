package com.example.di

import com.example.BuildConfig
import com.example.data.ApiService
import com.example.data.interceptor.CommonRequestParamsInterceptor
import com.example.data.interceptor.ConnectivityInterceptor
import com.example.data.interceptor.SessionInterceptor
import com.example.domain.managers.SessionManager
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule(private val baseUrl: String) {

  companion object {
    const val EXAMPLE_API_CLIENT = "EXAMPLE_API_CLIENT"

    const val DEFAULT_OKHTTP_CLIENT = "DEFAULT_OKHTTP_CLIENT"

    const val LOGGING_INTERCEPTOR = "LOGGING_INTERCEPTOR"
    const val STETHO_INTERCEPTOR = "STETHO_INTERCEPTOR"
    const val CONNECTIVITY_INTERCEPTOR = "CONNECTIVITY_INTERCEPTOR"
    const val COMMON_REQUEST_PARAMS_INTERCEPTOR = "COMMON_REQUEST_PARAMS_INTERCEPTOR"
    const val SESSION_INTERCEPTOR = "SESSION_INTERCEPTOR"

    private const val DATE_FORMAT = "dd-MM-yyyy"

    private const val CONNECT_TIMEOUT = 20L
    private const val WRITE_TIMEOUT = 30L
    private const val READ_TIMEOUT = 30L
  }

  @Provides
  @Singleton
  fun provideJacksonObjectMapper() = ObjectMapper().apply {
    configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
    registerModule(KotlinModule())
    dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)
  }

  @Provides
  @Singleton
  fun provideConverterFactory(objectMapper: ObjectMapper): JacksonConverterFactory {
    return JacksonConverterFactory.create(objectMapper)
  }

  @Provides
  @Singleton
  fun provideCallAdapterFactory(): RxJava2CallAdapterFactory {
    return RxJava2CallAdapterFactory.create()
  }

  @Provides
  @Singleton
  @Named(LOGGING_INTERCEPTOR)
  fun provideLoggingInterceptor(): Interceptor {
    return HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
  }

  @Provides
  @Singleton
  @Named(STETHO_INTERCEPTOR)
  fun provideStethoInterceptor(): Interceptor {
    return StethoInterceptor()
  }

  @Provides
  @Singleton
  @Named(CONNECTIVITY_INTERCEPTOR)
  fun provideConnectivityInterceptor(): Interceptor {
    return ConnectivityInterceptor()
  }

  @Provides
  @Singleton
  @Named(COMMON_REQUEST_PARAMS_INTERCEPTOR)
  fun provideCommonRequestParamsInterceptor(): Interceptor {
    return CommonRequestParamsInterceptor()
  }

  @Provides
  @Singleton
  @Named(SESSION_INTERCEPTOR)
  fun provideSessionInterceptor(sessionManager: SessionManager): Interceptor {
    return SessionInterceptor(sessionManager)
  }

  @Provides
  @Singleton
  @Named(DEFAULT_OKHTTP_CLIENT)
  fun provideDefaultOkHttpClient(
      @Named(CONNECTIVITY_INTERCEPTOR) connectivityInterceptor: Interceptor,
      @Named(LOGGING_INTERCEPTOR) loggingInterceptor: Interceptor,
      @Named(STETHO_INTERCEPTOR) stethoInterceptor: Interceptor,
      @Named(SESSION_INTERCEPTOR) sessionInterceptor: Interceptor,
      @Named(COMMON_REQUEST_PARAMS_INTERCEPTOR) commonQueryParamsInterceptor: Interceptor
  ): OkHttpClient {
    return OkHttpClient.Builder()
      .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
      .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
      .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
      .addInterceptor(connectivityInterceptor)
      .addInterceptor(sessionInterceptor)
      .addInterceptor(commonQueryParamsInterceptor)
      .apply { if (BuildConfig.DEBUG) addInterceptor(loggingInterceptor) }
      .apply { if (BuildConfig.DEBUG) addInterceptor(stethoInterceptor) }
      .build()
  }

  @Provides
  @Singleton
  @Named(EXAMPLE_API_CLIENT)
  fun provideBiRetrofit(
      @Named(DEFAULT_OKHTTP_CLIENT) okHttpClient: OkHttpClient,
      converterFactory: JacksonConverterFactory,
      rxJava2CallAdapterFactory: RxJava2CallAdapterFactory
  ): Retrofit {
    return Retrofit.Builder()
      .baseUrl(baseUrl)
      .addConverterFactory(converterFactory)
      .addCallAdapterFactory(rxJava2CallAdapterFactory)
      .client(okHttpClient)
      .build()
  }

  @Provides
  @Singleton
  fun provideApi(@Named(EXAMPLE_API_CLIENT) retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
  }
}
