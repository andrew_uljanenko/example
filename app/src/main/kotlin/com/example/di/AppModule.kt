package com.example.di

import android.content.Context
import android.content.SharedPreferences
import com.example.ExampleApplication
import com.example.di.module.ActivityBindingModule
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [ActivityBindingModule::class])
class AppModule {

  @Provides
  @Singleton
  fun provideContext(application: ExampleApplication): Context = application


  @Provides
  @Singleton
  @Named(APP_SHARED_PREFERENCES)
  fun provideSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(APP_SHARED_PREFERENCES, Context.MODE_PRIVATE)
  }


  companion object {
    const val APP_SHARED_PREFERENCES = "app_shared_prefs"
  }
}