package com.example.di

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Named
import javax.inject.Singleton

@Module
class NavigationModule {

  @Provides
  @Singleton
  @Named(APP_CICERONE)
  fun provideCicerone(): Cicerone<Router> {
    return Cicerone.create()
  }

  @Provides
  @Singleton
  @Named(APP_ROUTER)
  fun provideRouter(@Named(APP_CICERONE) cicerone: Cicerone<Router>): Router {
    return cicerone.router
  }

  @Provides
  @Singleton
  @Named(APP_NAVIGATION_HOLDER)
  fun provideNavigatorHolder(@Named(APP_CICERONE) cicerone: Cicerone<Router>): NavigatorHolder {
    return cicerone.navigatorHolder
  }


  companion object {
    const val APP_CICERONE = "APP_CICERONE"
    const val APP_ROUTER = "APP_ROUTER"
    const val APP_NAVIGATION_HOLDER = "APP_NAVIGATION_HOLDER"
  }
}