package com.example.presentation.common.widget.recyclerview.adapter.product

import android.view.View
import com.example.R
import com.example.domain.model.product.Product
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.util.extension.getString
import com.example.util.extension.load
import com.example.util.extension.setGone
import com.example.util.extension.setStrikeThru
import com.example.util.extension.setVisible
import com.bumptech.glide.load.resource.bitmap.FitCenter
import kotlinx.android.synthetic.main.lt_product_list_item.*


class RvProductViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  private val fitCenter = FitCenter()

  init {
    tvOldPrice.setStrikeThru()
  }

  fun bindView(
      product: Product,
      onPurchaseClickListener: OnPurchaseClickListener?,
      onProductClickListener: OnProductClickListener?,
      onFavoriteClickListener: OnFavoriteClickListener?
  ) {
    ivImage.load(product.imageSrc, bitmapTransformation = fitCenter)
    tvTitle.text = product.title

    if (product.oldPrice == null) {
      tvOldPrice.setGone()
    } else {
      tvOldPrice.setVisible()
      tvOldPrice.text = product.oldPrice.toString()
    }

    tvPrice.text = getString(R.string.price_format, product.price)
    rbProductRating.rating = product.rating.toFloat()
    tvCommentsCount.text = product.commentsCount.toString()
    cbFavorite.isChecked = product.isInFavorites

    btPurchase.setOnClickListener { onPurchaseClickListener?.invoke(product) }
    itemView.setOnClickListener { onProductClickListener?.invoke(product) }

    cbFavorite.setOnCheckedChangeListener { _, _ ->
      onFavoriteClickListener?.invoke(product)
    }
  }
}