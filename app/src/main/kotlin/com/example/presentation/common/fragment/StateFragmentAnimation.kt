package com.example.presentation.common.fragment

import android.view.animation.Animation
import androidx.fragment.app.Fragment

interface StateFragmentAnimation {

  val inAnimation: Animation
  val outAnimation: Animation

  var playAnimation: Boolean

  fun Fragment.onDisplay() {
    if (playAnimation) {
      view?.startAnimation(inAnimation)
    }
  }

  fun Fragment.onHide() {
    if (playAnimation) {
      view?.startAnimation(outAnimation)
    }
  }

  fun Fragment.clearAnimation() {
    view?.clearAnimation()
  }
}