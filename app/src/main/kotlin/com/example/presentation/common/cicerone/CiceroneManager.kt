package com.example.presentation.common.cicerone

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

class CiceroneManager constructor(
    val cicerone: Cicerone<Router>,
    val navigator: Navigator
) : LifecycleObserver {

  @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
  fun onResume() = cicerone.navigatorHolder.setNavigator(navigator)

  @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
  fun onPause() = cicerone.navigatorHolder.removeNavigator()
}