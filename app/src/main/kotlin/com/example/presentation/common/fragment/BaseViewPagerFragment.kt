package com.example.presentation.common.fragment

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.R

abstract class BaseViewPagerFragment : BaseFragment(), StateFragment {

  private var wasVisibleForUser: Boolean = false
  private var onCompletelyVisibleForUserMethodWasTriggered: Boolean = false

  override val inAnimation: Animation by lazy { AnimationUtils.loadAnimation(activity, R.anim.fade_in) }
  override val outAnimation: Animation by lazy {  AnimationUtils.loadAnimation(activity, R.anim.fade_out) }

  override var playAnimation = false

  protected fun setPlayScreenAnimations(playAnimation: Boolean) {
    this.playAnimation = playAnimation
  }

  /**
   * Called when fragment is completely visible for user for the first time
   * Here data loading should be triggered
   */
  open fun onCompletelyVisibleForUser() {
    wasVisibleForUser = true
    onCompletelyVisibleForUserMethodWasTriggered = true
    super.setUpTitle()
    super.setUpSubTitle()
  }

  open fun onNotVisibleForUser() {
    //no-op
  }

  override fun setUserVisibleHint(isVisibleToUser: Boolean) {
    super.setUserVisibleHint(isVisibleToUser)

    if (isVisibleToUser && isResumed) {
      onCompletelyVisibleForUser()
    } else if (wasVisibleForUser) {
      onNotVisibleForUser()
    }

    onCompletelyVisibleForUserMethodWasTriggered = false
  }

  override fun setUpTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  override fun setUpSubTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  open fun scrollToTop(): Boolean {
    return false
  }

  override fun onResume() {
    super.onResume()
    //Note: setUserVisibleHint called before onCreateView
    if (userVisibleHint && !onCompletelyVisibleForUserMethodWasTriggered) {
      onCompletelyVisibleForUser()
    }

    onCompletelyVisibleForUserMethodWasTriggered = false
  }

  override fun onPause() {
    wasVisibleForUser = false
    clearAnimation()
    onNotVisibleForUser()
    super.onPause()
  }

  override fun willBeDisplayed() {
    onDisplay()
  }

  override fun willBeHidden() {
    onHide()
  }
}
