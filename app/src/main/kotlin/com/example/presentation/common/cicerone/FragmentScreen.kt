package com.example.presentation.common.cicerone

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.presentation.common.cicerone.androidx.AppScreen
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

abstract class FragmentScreen<T : Fragment> : AppScreen() {

  abstract fun getFragmentClass(): KClass<T>

  override fun getScreenKey(): String {
    return getFragmentClass().simpleName!!
  }

  override fun getFragment(): T {
    return newInstance()
  }

  fun newInstance(): T {
    return getFragmentClass().createInstance().apply { arguments = createBundle() }
  }

  protected open fun createBundle(): Bundle? {
    return null
  }

  protected open fun readBundle(fragment: T) {
    //no-op
  }
}