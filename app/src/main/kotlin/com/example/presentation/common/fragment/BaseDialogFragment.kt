package com.example.presentation.common.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import com.example.util.extension.daggerInject

abstract class BaseDialogFragment : DialogFragment() {

  protected open var dontInjectAutomatically = false

  private var rootView: View? = null
  private var layoutResId: Int = -1

  override fun onCreate(savedInstanceState: Bundle?) {
    if (!dontInjectAutomatically) {
      daggerInject()
    }

    super.onCreate(savedInstanceState)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    savedInstanceState?.let {
      layoutResId = it.getInt(ARG_LAYOUT_ID_RES, -1)
    }
  }

  @CallSuper
  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ) = when {
    rootView != null -> rootView
    layoutResId == -1 -> rootView
    else -> {
      val view = inflateRootView(inflater, container, layoutResId)
      createView(view)
      view
    }
  }

  protected open fun createView(view: View?) {
    //no-op
  }

  protected fun inflateRootView(
      inflater: LayoutInflater,
      parent: ViewGroup?,
      @LayoutRes layoutResId: Int
  ): View = inflater.inflate(layoutResId, parent, false)

  @CallSuper
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    rootView = view
    setupUIForDebugMode()
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    outState.putInt(ARG_LAYOUT_ID_RES, layoutResId)
  }

  fun setLayoutResId(@LayoutRes resId: Int) {
    layoutResId = resId
  }

  protected open fun setupDialogSize() {
    val window = dialog?.window ?: return
    val layoutParams = WindowManager.LayoutParams()

    layoutParams.copyFrom(window.attributes)
    layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
    layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT

    window.attributes = layoutParams
  }


  protected open fun setupUIForDebugMode() {
    //no-op
  }

  override fun onResume() {
    super.onResume()
    setupDialogSize()
  }

  override fun onDestroyView() {
    // To prevent an exception on reAttaching this view to the fragment
    (rootView?.parent as? ViewGroup)?.removeView(rootView)

    super.onDestroyView()
  }

  companion object {
    const val ARG_LAYOUT_ID_RES = "ARG_LAYOUT_ID_RES"
  }
}