package com.example.presentation.common.widget.recyclerview.adapter.product

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.product.Product
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvAdapter
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.presentation.view.favourite.list.RvProductEditViewHolder

typealias OnPurchaseClickListener = (Product) -> Unit
typealias OnProductClickListener = (Product) -> Unit
typealias OnFavoriteClickListener = (Product) -> Unit

open class RvProductsAdapter(
    private val itemStyle: ItemType,
    private val itemsLimit: Int = Int.MAX_VALUE
) : BaseRvAdapter<BaseRvViewHolder>() {

  var onProductClickListener: OnProductClickListener? = null
  var onPurchaseClickListener: OnPurchaseClickListener? = null
  var onFavoriteClickListener: OnFavoriteClickListener? = null

  val data = ArrayList<Product>()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder {
    val view = inflate(itemStyle.layoutIdRes, parent)
    return when {
      itemStyle.layoutIdRes == ItemType.FAVOURITE_ITEM_EDIT.layoutIdRes -> RvProductEditViewHolder(view)
      else -> RvProductViewHolder(view)
    }
  }

  override fun getItemCount(): Int {
    return if (data.size > itemsLimit) itemsLimit else data.size
  }

  override fun onBindViewHolder(holder: BaseRvViewHolder, position: Int) {
    val product = data[position]
    when (holder) {
      is RvProductViewHolder -> holder.bindView(product, onPurchaseClickListener, onProductClickListener, onFavoriteClickListener)
      is RvProductEditViewHolder -> holder.bindView(product, onPurchaseClickListener, onProductClickListener, onFavoriteClickListener)
    }
  }


  //To handle shadow touch effect or different layouts
  enum class ItemType(val layoutIdRes: Int) {
    DEFAULT_BG(R.layout.lt_product_list_item_with_default_bg),
    LIGHT_GRAY_BG(R.layout.lt_product_list_item_with_light_gray_bg),
    FAVOURITE_ITEM(R.layout.lt_product_list_item_favourite),
    FAVOURITE_ITEM_EDIT(R.layout.lt_product_list_item_favourite_edit_mode)
  }
}