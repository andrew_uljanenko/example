package com.example.presentation.common.fragment

import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.example.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import javax.inject.Inject

abstract class BaseViewPagerContainerFragment : BaseFragment(), StateFragment, HasSupportFragmentInjector {

  @Inject
  lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

  open lateinit var router: Router

  var wasVisibleForUser: Boolean = false
  var onCompletelyVisibleForUserMethodWasTriggered: Boolean = false

  override val inAnimation: Animation by lazy { AnimationUtils.loadAnimation(activity, R.anim.fade_in) }
  override val outAnimation: Animation by lazy { AnimationUtils.loadAnimation(activity, R.anim.fade_out) }

  override var playAnimation = false

  protected fun setPlayScreenAnimations(playAnimation: Boolean) {
    this.playAnimation = playAnimation
  }

  override fun supportFragmentInjector(): AndroidInjector<Fragment> {
    return supportFragmentInjector
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setLayoutResId(R.layout.lt_fragment_container)
    setPlayScreenAnimations(true)

    setupFirstScreen(childFragmentManager.backStackEntryCount < 1)
  }

  abstract fun getRootScreen(): Screen

  /**
   * Called when fragment is completely visible for user for the first time
   * Here data loading should be triggered
   */
  @CallSuper
  open fun onCompletelyVisibleForUser() {
    wasVisibleForUser = true
    onCompletelyVisibleForUserMethodWasTriggered = true
    (getCurrentChildFragment() as? BaseViewPagerChildFragment)?.onCompletelyVisibleForUser()
  }

  open fun onNotVisibleForUser() {
    (getCurrentChildFragment() as? BaseViewPagerChildFragment)?.onNotVisibleForUser()
  }

  override fun setUserVisibleHint(isVisibleToUser: Boolean) {
    super.setUserVisibleHint(isVisibleToUser)

    if (isVisibleToUser && isResumed) {
      onCompletelyVisibleForUser()
    } else if (wasVisibleForUser) {
      onNotVisibleForUser()
    }

    onCompletelyVisibleForUserMethodWasTriggered = false
  }

  override fun setUpTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  override fun setUpSubTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  override fun onPause() {
    wasVisibleForUser = false
    clearAnimation()
    onNotVisibleForUser()
    super.onPause()
  }

  override fun willBeDisplayed() {
    onDisplay()
  }

  override fun willBeHidden() {
    onHide()
  }

  fun scrollToTop(): Boolean {
    val currentFragment = getCurrentChildFragment()
    if (currentFragment != null && currentFragment is BaseViewPagerFragment) {
      return currentFragment.scrollToTop()
    }

    return false
  }

  protected open fun setupFirstScreen(hasNoFragmentsInBackStack: Boolean) {
    if (hasNoFragmentsInBackStack) {
      router.navigateTo(getRootScreen())
    }
  }

  fun getCurrentChildFragment(): Fragment? {
    return childFragmentManager.findFragmentById(R.id.fragmentContainer)
  }

  override fun onBackPressed(): Boolean {
    return false
  }
}
