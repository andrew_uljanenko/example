package com.example.presentation.common.widget.recyclerview.adapter.common

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface ViewTypeDelegateAdapter<T : RecyclerView.ViewHolder, E> {
  fun onCreateViewHolder(viewGroup: ViewGroup): T
  fun onBindViewHolder(holder: T, position: Int)
  fun onBindViewHolder(holder: T, item: E)
}