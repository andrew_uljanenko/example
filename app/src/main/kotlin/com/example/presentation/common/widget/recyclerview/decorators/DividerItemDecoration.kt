package com.example.presentation.common.widget.recyclerview.decorators

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class DividerItemDecoration(
    context: Context?,
    dividerIdRes: Int,
    private val leftPadding: Int = 0,
    private val rightPadding: Int = leftPadding
) : RecyclerView.ItemDecoration() {

  private val divider: Drawable = ContextCompat.getDrawable(context!!, dividerIdRes)!!

  override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
    val left = leftPadding
    val right = parent.width - rightPadding

    val childCount = parent.childCount
    for (i in 0 until childCount) {
      val child = parent.getChildAt(i)

      val params = child.layoutParams as RecyclerView.LayoutParams

      val top = child.bottom + params.bottomMargin
      val bottom = top + divider.intrinsicHeight

      divider.setBounds(left, top, right, bottom)
      divider.draw(c)
    }
  }
}