package com.example.presentation.common.widget.recyclerview.decorators

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView

class BottomSpaceItemDecoration(
    private val itemPosition: Int,
    @DimenRes private val dimenIdRes: Int
) : RecyclerView.ItemDecoration() {

  override fun getItemOffsets(
      outRect: Rect,
      view: View,
      parent: RecyclerView,
      state: RecyclerView.State
  ) {
    val position = (view.layoutParams as? RecyclerView.LayoutParams)?.viewLayoutPosition ?: -1
    val context = view.context

    if (position == itemPosition && context != null) {
      outRect.bottom = context.resources.getDimensionPixelOffset(dimenIdRes)
    } else {
      super.getItemOffsets(outRect, view, parent, state)
    }
  }
}