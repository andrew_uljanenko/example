package com.example.presentation.common.model

import androidx.lifecycle.MutableLiveData
import com.example.domain.model.common.Response
import com.example.domain.model.common.Status

class ResponseLiveData<T> : MutableLiveData<Response<T>>() {

  fun isLoadingOrSuccess(): Boolean {
    return value?.status == Status.LOADING || value?.status == Status.SUCCESS
  }

  fun isLoading(): Boolean {
    return value?.status == Status.LOADING
  }
}