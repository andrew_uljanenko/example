@file:Suppress("UNNECESSARY_SAFE_CALL")

package com.example.presentation.common.widget

import android.widget.ProgressBar
import com.example.util.animation.Interpolators
import com.example.util.extension.setGone
import com.example.util.extension.setVisible


private const val PROGRESS_BAR_ANIMATION_DURATION = 600L

fun ProgressBar?.showProgressBar() {
  this ?: return

  animate()
    .setInterpolator(Interpolators.ACCELERATE_DECELERATE)
    .setDuration(PROGRESS_BAR_ANIMATION_DURATION)
    .scaleY(1f)
    .withStartAction { this?.setVisible() }
    .start()
}

fun ProgressBar?.hideProgressBar() {
  this ?: return

  animate()
    .setInterpolator(Interpolators.ACCELERATE_DECELERATE)
    .setDuration(PROGRESS_BAR_ANIMATION_DURATION)
    .scaleY(0f)
    .withEndAction { this?.setGone() }
    .start()
}
