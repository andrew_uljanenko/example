package com.example.presentation.common.carousel

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.banner.Banner
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvAdapter

class RvCarouselAdapter : BaseRvAdapter<RvBannerViewHolder>() {

  var banners: List<Banner> = emptyList()

  var onBannerClickListener: OnBannerClickListener? = null

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvBannerViewHolder {
    val view = inflate(R.layout.lt_banner, parent)
    return RvBannerViewHolder(view)
  }

  override fun getItemCount(): Int {
    return banners.size
  }

  override fun onBindViewHolder(holder: RvBannerViewHolder, position: Int) {
    val banner = banners[position]
    holder.bind(banner, onBannerClickListener)
  }
}