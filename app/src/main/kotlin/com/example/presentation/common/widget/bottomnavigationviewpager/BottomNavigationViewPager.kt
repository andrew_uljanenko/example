package com.example.presentation.common.widget.bottomnavigationviewpager

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class BottomNavigationViewPager(
    context: Context,
    attrs: AttributeSet
) : ViewPager(context, attrs) {

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent) = false

  override fun onInterceptTouchEvent(event: MotionEvent) = false
}