package com.example.presentation.common.category

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.category.Category
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvAdapter

class RvCategoriesAdapter(val itemsLimit: Int = Int.MAX_VALUE) : BaseRvAdapter<RvCategoryViewHolder>() {

  val data = ArrayList<Category>()
  var onCategoryClickListener: OnCategoryClickListener? = null

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvCategoryViewHolder {
    val view = inflate(R.layout.lt_category, parent)
    return RvCategoryViewHolder(view)
  }

  override fun getItemCount(): Int {
    return if (data.size > itemsLimit) itemsLimit else data.size
  }

  override fun onBindViewHolder(holder: RvCategoryViewHolder, position: Int) {
    val category = data[position]
    holder.bindView(category, onCategoryClickListener)
  }
}