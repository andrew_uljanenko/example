package com.example.presentation.common.listener

import android.view.animation.Animation

abstract class AnimationListener : Animation.AnimationListener {
  override fun onAnimationRepeat(animation: Animation?) {
    //no-op
  }

  override fun onAnimationEnd(animation: Animation?) {
    //no-op
  }

  override fun onAnimationStart(animation: Animation?) {
    //no-op
  }
}