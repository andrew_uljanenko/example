package com.example.presentation.common.listener

interface BackButtonListener {
  fun onBackPressed(): Boolean
}