package com.example.presentation.common.listener

import androidx.transition.Transition

abstract class TransitionListener : Transition.TransitionListener {
  override fun onTransitionEnd(transition: Transition) {
    //no-op
  }

  override fun onTransitionResume(transition: Transition) {
    //no-op
  }

  override fun onTransitionPause(transition: Transition) {
    //no-op
  }

  override fun onTransitionCancel(transition: Transition) {
    //no-op
  }

  override fun onTransitionStart(transition: Transition) {
    //no-op
  }
}