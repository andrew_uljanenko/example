package com.example.presentation.common.widget.recyclerview.adapter.common

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

abstract class BaseRvDelegateAdapter<T : BaseRvViewHolder, E> : ViewTypeDelegateAdapter<T, E> {

  protected lateinit var context: Context
    private set

  private val layoutInflater: LayoutInflater by lazy { LayoutInflater.from(context) }
  private val handler: Handler by lazy { Handler(Looper.getMainLooper()) }

  protected fun inflate(@LayoutRes layoutResId: Int, parent: ViewGroup): View {
    if (!::context.isInitialized) {
      context = parent.context
    }

    return layoutInflater.inflate(layoutResId, parent, false)
  }

  fun onBindViewHolder(holder: T) {
    onBindViewHolder(holder, -1)
  }

  override fun onBindViewHolder(holder: T, position: Int) {
    //Delete this
  }

  open fun getItemCount(): Int = 0

  protected fun runOnUiThread(runnable: Runnable) {
    handler.post(runnable)
  }
}