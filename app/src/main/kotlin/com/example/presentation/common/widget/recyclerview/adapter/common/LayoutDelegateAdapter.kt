package com.example.presentation.common.widget.recyclerview.adapter.common

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.example.presentation.common.widget.recyclerview.adapter.common.LayoutDelegateAdapter.*

typealias ItemsCountSupplier = () -> Int

class LayoutDelegateAdapter(
    @LayoutRes private val layoutIdRes: Int,
    private val itemsCountSupplier: ItemsCountSupplier? = null
) : BaseRvDelegateAdapter<LayoutViewHolder, Any>() {

  override fun onBindViewHolder(holder: LayoutViewHolder, item: Any) {
    //no-op
  }

  override fun onCreateViewHolder(viewGroup: ViewGroup): LayoutViewHolder {
    val view = inflate(layoutIdRes, viewGroup)
    return LayoutViewHolder(view)
  }

  override fun onBindViewHolder(holder: LayoutViewHolder, position: Int) {
    //no-op
  }

  override fun getItemCount() = itemsCountSupplier?.invoke() ?: 1

  class LayoutViewHolder(containerView: View) : BaseRvViewHolder(containerView)
}