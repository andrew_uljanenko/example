package com.example.presentation.common.category

import android.view.View
import com.example.domain.model.category.Category
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.util.extension.load
import kotlinx.android.synthetic.main.lt_category.*

typealias OnCategoryClickListener = (category: Category) -> Unit

class RvCategoryViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bindView(category: Category, onCategoryClickListener: OnCategoryClickListener?) {
    itemView.setOnClickListener { onCategoryClickListener?.invoke(category) }

    ivCategoryIcon.load(category.imageSrc, true)
    tvCategoryTitle.text = category.name
  }
}