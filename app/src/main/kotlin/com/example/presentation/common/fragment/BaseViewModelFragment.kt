package com.example.presentation.common.fragment

import android.os.Bundle
import com.example.presentation.common.BaseViewModel
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseViewModelFragment<T : BaseViewModel> : BaseFragment() {

  @Inject
  lateinit var viewModel: T

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    if (::viewModel.isInitialized) {
      viewModel.restoreStateIfNeeded(savedInstanceState)
    }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    if (::viewModel.isInitialized) {
      viewModel.onSaveInstanceState(outState)
    }
  }

  fun parentFragmentRouter(): Router? {
    return (parentFragment as? BaseViewPagerContainerFragment)?.router
  }
}