package com.example.presentation.common.listener

interface OnAppBarToggleListener {
  fun collapseAppBar()
  fun expandAppBar()
}