package com.example.presentation.common

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.evernote.android.state.StateSaver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

  private var disposables: CompositeDisposable? = null
    get() {
      if (field == null || field!!.isDisposed) {
        field = CompositeDisposable()
      }

      return field
    }

  protected var isStateful: Boolean = false

  protected fun Disposable.registerDisposable() {
    disposables?.add(this)
  }

  @CallSuper
  protected open fun disposeAll() {
    disposables?.clear()
    disposables = null
  }

  @CallSuper
  override fun onCleared() {
    super.onCleared()
    disposeAll()
  }

  internal fun onSaveInstanceState(outState: Bundle) {
    if (isStateful) {
      StateSaver.saveInstanceState(this, outState)
    }
  }

  internal fun restoreStateIfNeeded(savedInstanceState: Bundle?) {
    if (isStateful) {
      StateSaver.restoreInstanceState(this, savedInstanceState)
    }
  }
}