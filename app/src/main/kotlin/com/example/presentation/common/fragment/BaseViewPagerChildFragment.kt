package com.example.presentation.common.fragment

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.R
import ru.terrakok.cicerone.Router

abstract class BaseViewPagerChildFragment : BaseFragment(), StateFragment {

  override val inAnimation: Animation by lazy { AnimationUtils.loadAnimation(activity, R.anim.fade_in) }
  override val outAnimation: Animation by lazy { AnimationUtils.loadAnimation(activity, R.anim.fade_out) }

  override var playAnimation = false

  protected fun setPlayScreenAnimations(playAnimation: Boolean) {
    this.playAnimation = playAnimation
  }

  /**
   * Called when fragment is completely visible for user for the first time
   * Here data loading should be triggered
   */
  open fun onCompletelyVisibleForUser() {
    super.setUpTitle()
    super.setUpSubTitle()
  }

  open fun onNotVisibleForUser() {
    //no-op
  }

  override fun setUpTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  override fun setUpSubTitle() {
    //Should be empty to prevent calling from super#onResume();
  }

  open fun scrollToTop(): Boolean {
    return false
  }

  override fun onResume() {
    super.onResume()
    val containerFragment = parentFragment as BaseViewPagerContainerFragment

    if (containerFragment.userVisibleHint && !containerFragment.onCompletelyVisibleForUserMethodWasTriggered) {
      onCompletelyVisibleForUser()
    }

    containerFragment.onCompletelyVisibleForUserMethodWasTriggered = false
  }

  override fun onPause() {
    clearAnimation()
    super.onPause()
  }

  override fun willBeDisplayed() {
    onDisplay()
  }

  override fun willBeHidden() {
    onHide()
  }

  fun parentFragmentRouter(): Router? {
    return (parentFragment as? BaseViewPagerContainerFragment)?.router
  }
}
