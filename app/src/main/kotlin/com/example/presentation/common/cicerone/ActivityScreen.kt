package com.example.presentation.common.cicerone

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.presentation.common.cicerone.androidx.AppScreen
import kotlin.reflect.KClass

abstract class ActivityScreen<T : Activity> : AppScreen() {

  abstract fun getActivityClass(): KClass<T>

  override fun getScreenKey(): String {

    return getActivityClass().simpleName!!
  }

  override fun getActivityIntent(context: Context): Intent? {
    val intent = Intent(context, getActivityClass().java)
    createBundle()?.let {
      intent.putExtras(it)
    }

    return intent
  }

  open fun createBundle(): Bundle? {
    return null
  }

  protected open fun readBundle(activity: T) {
    //no-op
  }
}