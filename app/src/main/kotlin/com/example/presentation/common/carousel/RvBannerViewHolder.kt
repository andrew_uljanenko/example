package com.example.presentation.common.carousel

import android.view.View
import com.example.domain.model.banner.Banner
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.util.extension.load
import kotlinx.android.synthetic.main.lt_banner.*

typealias OnBannerClickListener = (Banner) -> Unit

class RvBannerViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(banner: Banner, onBannerClickListener: OnBannerClickListener?) {
    ivBanner.load(banner.imageSrc)
    containerView.setOnClickListener { onBannerClickListener?.invoke(banner) }
  }
}