package com.example.presentation.common.carousel

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.presentation.common.widget.recyclerview.RecyclerView

class AutoScrollRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RecyclerView(context, attrs) {

  companion object {
    private const val AUTO_SCROLL_DELAY = 5000L
    private val autoScrollHandler = Handler(Looper.getMainLooper())
  }

  private var isAutoScrollEnabled = false
  private var autoScrollDelay: Long = AUTO_SCROLL_DELAY
  private val autoScrollRunnable = Runnable {
    val linearLayoutManager = layoutManager as? LinearLayoutManager ?: throw IllegalArgumentException()

    val currentItemPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition()
    val lastItemPosition = linearLayoutManager.itemCount - 1
    val nextItem = if (currentItemPosition < lastItemPosition) currentItemPosition + 1 else 0

    smoothScrollToPosition(nextItem)
    scrollNext()
  }

  override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
    adapter ?: super.dispatchTouchEvent(ev)

    when (ev?.action) {
      MotionEvent.ACTION_DOWN -> cancelAllAutoScrollCallbacks()
      MotionEvent.ACTION_UP -> scrollNext()
    }

    return super.dispatchTouchEvent(ev)
  }

  fun startAutoScroll(autoScrollDelay: Long = AUTO_SCROLL_DELAY) {
    adapter ?: throw IllegalArgumentException()

    isAutoScrollEnabled = true
    this.autoScrollDelay = autoScrollDelay
    scrollNext()
  }

  private fun scrollNext() {
    cancelAllAutoScrollCallbacks()
    if (isAutoScrollEnabled) {
      autoScrollHandler.postDelayed(autoScrollRunnable, autoScrollDelay)
    }
  }

  fun stopAutoScroll() {
    isAutoScrollEnabled = false
    cancelAllAutoScrollCallbacks()
  }

  private fun cancelAllAutoScrollCallbacks() {
    autoScrollHandler.removeCallbacks(autoScrollRunnable)
  }
}