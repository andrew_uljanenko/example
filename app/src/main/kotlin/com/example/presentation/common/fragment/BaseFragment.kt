package com.example.presentation.common.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.example.R
import com.example.presentation.common.activity.BaseActivity
import com.example.presentation.common.listener.BackButtonListener
import com.example.util.extension.daggerInject
import com.example.util.extension.inflate
import com.google.android.material.appbar.AppBarLayout
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment : Fragment(), BackButtonListener {

  protected open var dontInjectAutomatically = false

  private var rootView: View? = null
  private var layoutResId: Int = -1
  private var title: String = ""
  private var subTitle: String? = null
  private var menuIdRes: Int = -1

  private val appBar: AppBarLayout by lazy { activity!!.findViewById<AppBarLayout>(R.id.appBar) }

  private var compositeDisposable: CompositeDisposable? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    if (!dontInjectAutomatically) {
      daggerInject()
    }

    super.onCreate(savedInstanceState)

    if (activity !is BaseActivity) {
      throw IllegalStateException("Activity container should instance of BaseActivity")
    }

    setHasOptionsMenu(true)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    savedInstanceState?.let {
      title = it.getString(ARG_TITLE, "")
      subTitle = it.getString(ARG_SUB_TITLE)
      layoutResId = it.getInt(ARG_LAYOUT_ID_RES, -1)
      menuIdRes = it.getInt(ARG_MENU_ID_RES, -1)
    }
  }

  @CallSuper
  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ) = when {
    rootView != null -> rootView
    layoutResId == -1 -> rootView
    else -> {
      val view = inflateRootView(inflater, container, layoutResId)
      createView(view)
      view
    }
  }

  protected open fun createView(view: View?) {
    //no-op
  }

  protected fun inflateRootView(
      inflater: LayoutInflater,
      parent: ViewGroup?,
      @LayoutRes layoutResId: Int
  ) = parent?.inflate(inflater, layoutResId)

  @CallSuper
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    rootView = view
    setupUIForDebugMode()
  }

  protected fun enableActionBarBackArrow() {
    (activity as BaseActivity).enableActionBarBackArrow()
  }

  protected fun disableActionBarBackArrow() {
    (activity as BaseActivity).disableActionBarBackArrow()
  }

  protected fun disableActionBarCloseButton() {
    (activity as BaseActivity).enableActionBarBackArrowAfterCloseButton()
  }

  protected fun enableActionBarCloseButton() {
    (activity as BaseActivity).enableActionBarBackArrowAsCloseButton()
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    outState.putString(ARG_TITLE, title)
    subTitle?.let { outState.putString(ARG_SUB_TITLE, it) }
    outState.putInt(ARG_MENU_ID_RES, menuIdRes)
    outState.putInt(ARG_LAYOUT_ID_RES, layoutResId)
  }

  fun setTitle(title: String?) {
    this.title = title ?: " "
  }

  fun setTitle(@StringRes titleResId: Int) {
    val title = getString(titleResId)
    setTitle(title)
  }

  fun setSubTitle(subTitle: String?) {
    this.subTitle = subTitle
  }

  fun setSubTitle(@StringRes titleResId: Int) {
    val subTitle = getString(titleResId)
    setSubTitle(subTitle)
  }

  fun setMenuResId(@MenuRes resId: Int) {
    menuIdRes = resId
  }

  fun setLayoutResId(@LayoutRes resId: Int) {
    layoutResId = resId
  }

  protected open fun setUpTitle() {
    (activity as BaseActivity).setTitle(title)
  }

  protected open fun setUpSubTitle() {
    (activity as BaseActivity).setSubTitle(subTitle)
  }

  override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
    super.onCreateOptionsMenu(menu, inflater)
    if (menuIdRes != -1) {
      inflater.inflate(menuIdRes, menu)
    }
  }

  protected open fun setupUIForDebugMode() {
    //no-op
  }

  override fun onResume() {
    super.onResume()
    setUpTitle()
    setUpSubTitle()
  }

  override fun onPause() {
    super.onPause()
    compositeDisposable?.dispose()
  }

  override fun onDestroyView() {
    // To prevent an exception on reAttaching this view to the fragment
    (rootView?.parent as? ViewGroup)?.removeView(rootView)

    super.onDestroyView()
  }

  fun Disposable.registerDisposable() {
    if (compositeDisposable == null) {
      compositeDisposable = CompositeDisposable(this)
    } else {
      compositeDisposable?.add(this)
    }
  }

  companion object {
    const val ARG_TITLE = "ARG_TITLE"
    const val ARG_SUB_TITLE = "ARG_SUB_TITLE"
    const val ARG_LAYOUT_ID_RES = "ARG_LAYOUT_ID_RES"
    const val ARG_MENU_ID_RES = "ARG_MENU_ID_RES"
  }
}