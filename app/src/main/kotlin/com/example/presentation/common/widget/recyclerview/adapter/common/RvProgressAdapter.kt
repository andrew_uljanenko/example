package com.example.presentation.common.widget.recyclerview.adapter.common

import android.view.ViewGroup
import com.example.R

abstract class RvProgressAdapter<T : BaseRvViewHolder, E> : BaseRvListAdapter<BaseRvViewHolder, E>() {

  private var isLoading = false

  private val progressAdapter = LayoutDelegateAdapter(R.layout.lt_list_item_progress)

  abstract fun createView(parent: ViewGroup): T

  abstract fun bindView(holder: T, position: Int)

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRvViewHolder {
    return when (viewType) {
      VIEW_TYPE_LOADING -> progressAdapter.onCreateViewHolder(parent)
      else -> createView(parent)
    }
  }

  @Suppress("UNCHECKED_CAST")
  override fun onBindViewHolder(holder: BaseRvViewHolder, position: Int) {
    if (getItemViewType(position) == VIEW_TYPE_ELEMENT) {
      bindView(holder as T, position)
    }
  }

  override fun getItemCount(): Int {
    return data.size + if (isLoading) 1 else 0
  }

  override fun getItemViewType(position: Int): Int {
    if (isLoading && position == itemCount - 1) return VIEW_TYPE_LOADING

    return VIEW_TYPE_ELEMENT
  }

  fun showProgress() {
    isLoading = true

    runOnUiThread(Runnable { notifyItemInserted(itemCount + 1) })
  }

  fun hideProgress() {
    notifyItemRemoved(itemCount)
    isLoading = false
  }

  companion object {
    const val VIEW_TYPE_LOADING = 1
    const val VIEW_TYPE_ELEMENT = 2
  }
}