package com.example.presentation.common.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.R
import com.example.presentation.common.HasCloseButton
import com.example.presentation.common.listener.BackButtonListener
import com.example.util.extension.daggerInject
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


abstract class BaseFragmentActivity
  : BaseActivity(),
    HasSupportFragmentInjector,
    FragmentManager.OnBackStackChangedListener {

  @Inject
  lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

  override fun onCreate(savedInstanceState: Bundle?) {
    daggerInject()
    super.onCreate(savedInstanceState)

    supportFragmentManager.addOnBackStackChangedListener(this)
  }

  override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment> {
    return fragmentInjector
  }

  override fun onBackPressed() {
    getCurrentFragment().let {
      if (it != null && it is BackButtonListener) {
        val isOnBackPressed = it.onBackPressed()
        if (isOnBackPressed || supportFragmentManager.backStackEntryCount > 1) {
          if (!isOnBackPressed) {
            super.onBackPressed()
          }
          return
        } else {
          finish()
        }
      } else {
        super.onBackPressed()
      }
    }
  }

  override fun onBackStackChanged() {
    val currentFragment = getCurrentFragment()

    if (currentFragment is HasCloseButton) {
      enableActionBarBackArrowAsCloseButton()
    } else {
      if (supportFragmentManager.backStackEntryCount > 1) {
        enableActionBarBackArrowAfterCloseButton()
      } else {
        disableActionBarBackArrow()
      }
    }
  }

  fun getCurrentFragment(): Fragment? {
    return supportFragmentManager.findFragmentById(R.id.fragmentContainer)
  }
}