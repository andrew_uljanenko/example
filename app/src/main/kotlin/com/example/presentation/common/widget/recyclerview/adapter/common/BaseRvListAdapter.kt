package com.example.presentation.common.widget.recyclerview.adapter.common

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRvListAdapter<T : BaseRvViewHolder, E> : RecyclerView.Adapter<T>() {

  abstract val data: List<E>
  protected lateinit var context: Context
    private set

  private val layoutInflater: LayoutInflater by lazy { LayoutInflater.from(context) }

  protected fun inflate(@LayoutRes layoutResId: Int, parent: ViewGroup): View {
    if (!::context.isInitialized) {
      context = parent.context
    }

    return layoutInflater.inflate(layoutResId, parent, false)
  }

  override fun getItemCount(): Int {
    return data.size
  }

  protected fun runOnUiThread(runnable: Runnable) {
    Handler().post(runnable)
  }
}