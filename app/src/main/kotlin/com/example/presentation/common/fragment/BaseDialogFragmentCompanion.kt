package com.example.presentation.common.fragment

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

abstract class BaseDialogFragmentCompanion<T : DialogFragment> {

  abstract fun getDialogFragmentClass(): KClass<T>

  open fun getScreenKey(): String {
    return "${getDialogFragmentClass().simpleName!!}${createBundle()?.hashCode()}"
  }

  fun newInstance(): T = getDialogFragmentClass().createInstance().apply { arguments = createBundle() }

  open fun createBundle(): Bundle? {
    return null
  }

  open fun readBundle(fragment: T) {
    //no-op
  }
}