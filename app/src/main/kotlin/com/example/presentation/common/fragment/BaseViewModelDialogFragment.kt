package com.example.presentation.common.fragment

import android.os.Bundle
import com.example.presentation.common.BaseViewModel
import com.example.util.extension.daggerInject
import javax.inject.Inject

abstract class BaseViewModelDialogFragment<T : BaseViewModel> : BaseDialogFragment() {

  @Inject
  lateinit var viewModel: T

  override fun onCreate(savedInstanceState: Bundle?) {
    daggerInject()
    super.onCreate(savedInstanceState)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    if (::viewModel.isInitialized) {
      viewModel.restoreStateIfNeeded(savedInstanceState)
    }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    if (::viewModel.isInitialized) {
      viewModel.onSaveInstanceState(outState)
    }
  }
}