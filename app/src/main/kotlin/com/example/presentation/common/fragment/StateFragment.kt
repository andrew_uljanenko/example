package com.example.presentation.common.fragment

interface StateFragment : StateFragmentAnimation {
  fun willBeDisplayed()
  fun willBeHidden()
}
