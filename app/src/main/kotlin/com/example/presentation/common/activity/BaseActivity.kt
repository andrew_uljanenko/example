@file:Suppress("unused")

package com.example.presentation.common.activity

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.R
import com.example.util.checkVersionNotLessThan
import com.example.util.extension.getColorRes
import com.example.util.extension.getDrawableRes
import com.evernote.android.state.StateSaver


abstract class BaseActivity : AppCompatActivity() {

  private var mIsBackArrowEnabled: Boolean = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    savedInstanceState?.let {
      mIsBackArrowEnabled = it.getBoolean(ARG_IS_BACK_ARROW_ENABLED, false)
    }

    //Save state
    StateSaver.restoreInstanceState(this, savedInstanceState)
  }

  override fun setContentView(@LayoutRes layoutResID: Int) {
    super.setContentView(layoutResID)
    disableActionBarTitle()
  }

  override fun setTitle(titleId: Int) {
    setTitle(getString(titleId))
  }

  override fun setTitle(title: CharSequence) {
    setTitle(title as String)
  }

  open fun setTitle(title: String) {
    supportActionBar?.title = title
  }

  open fun setSubTitle(subTitle: String?) {
    supportActionBar?.subtitle = subTitle
  }

  open fun disableActionBarTitle() {
    setShowActionBarTitle(false)
  }

  open fun enableActionBarTitle() {
    setShowActionBarTitle(true)
  }

  private fun setShowActionBarTitle(show: Boolean) {
    supportActionBar?.setDisplayShowTitleEnabled(show)
  }

  /**
   * Display ActionBar home button arrow
   */
  open fun enableActionBarBackArrow() {
    setShowActionBarBackArrow(true)
  }

  /**
   * Hide ActionBar home button arrow
   */
  open fun disableActionBarBackArrow() {
    setShowActionBarBackArrow(false)
  }

  private fun setShowActionBarBackArrow(show: Boolean) {
    mIsBackArrowEnabled = show

    supportActionBar?.apply {
      setDisplayHomeAsUpEnabled(show)
      setDisplayShowHomeEnabled(show)
    }
  }

  open fun enableActionBarBackArrowAfterCloseButton() {
    enableActionBarBackArrow()

    supportActionBar?.let {
      val closeIcon = getDrawableRes(R.drawable.ic_arrow_back_black_24dp)

      if (closeIcon != null) {
        val color = getColorRes(R.color.textColorPrimary)
        closeIcon.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        it.setHomeAsUpIndicator(closeIcon)
      }
    }
  }

  /**
   * Display ActionBar home button as cross
   */
  open fun enableActionBarBackArrowAsCloseButton() {
    enableActionBarBackArrow()

    supportActionBar?.let {
      val closeIcon = getDrawableRes(R.drawable.ic_close_black_24dp)

      if (closeIcon != null) {
        val color = getColorRes(R.color.textColorPrimary)
        closeIcon.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        it.setHomeAsUpIndicator(closeIcon)
      }
    }
  }

  open fun setStatusBarTransparent() {
    if (checkVersionNotLessThan(Build.VERSION_CODES.KITKAT)) {
      window?.apply {
        setFlags(
          WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
          WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )
      }
    }
  }

  /**
   * HomeButton (Burger/Arrow) override method
   */
  override fun onSupportNavigateUp(): Boolean {
    if (mIsBackArrowEnabled) {
      onBackPressed()
      return true
    }
    return super.onSupportNavigateUp()
  }

  /**
   * Back button logic for fragment navigation design pattern
   */
  override fun onBackPressed() {
    if (supportFragmentManager.backStackEntryCount > 1) {
      supportFragmentManager.popBackStack()
    } else {
      finish()
    }
  }

  override fun onResume() {
    super.onResume()
    if (mIsBackArrowEnabled) {
      enableActionBarBackArrow()
    } else {
      disableActionBarBackArrow()
    }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    outState.putBoolean(ARG_IS_BACK_ARROW_ENABLED, mIsBackArrowEnabled)
    StateSaver.saveInstanceState(this, outState)
  }

  private companion object {
    const val ARG_IS_BACK_ARROW_ENABLED = "arg_is_back_arrow_enabled"
  }
}