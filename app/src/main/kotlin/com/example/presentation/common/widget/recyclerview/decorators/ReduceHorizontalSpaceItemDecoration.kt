package com.example.presentation.common.widget.recyclerview.decorators

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ReduceHorizontalSpaceItemDecoration(sizeInPx: Int) : RecyclerView.ItemDecoration() {

  private val reduceSize = sizeInPx / 2

  override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
    outRect.right = -reduceSize
    outRect.left = -reduceSize
  }
}