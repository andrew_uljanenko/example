package com.example.presentation.common.widget.bottomnavigationviewpager

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

abstract class BottomNavigationViewPagerAdapter constructor(fm: FragmentManager) : FragmentPagerAdapter(fm) {

  val fragments: SparseArray<Fragment> = SparseArray()

  override fun instantiateItem(container: ViewGroup, position: Int): Any {
    val fragment = super.instantiateItem(container, position) as Fragment
    fragments.put(position, fragment)
    return fragment
  }

  override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
    fragments.delete(position)
    super.destroyItem(container, position, obj)
  }

  fun getRegisteredFragment(position: Int): Fragment? {
    return fragments.get(position)
  }
}