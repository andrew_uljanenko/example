package com.example.presentation.view.profile.common.product

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.product.Product
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvListAdapter

class RvProfileProductsAdapter(
    override val data: List<Product>
) : BaseRvListAdapter<RvOrderProductViewHolder, Product>() {

  var onProductClickListener: OnProductClickListener? = null

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvOrderProductViewHolder {
    val view = inflate(R.layout.lt_item_product, parent)
    return RvOrderProductViewHolder(view)
  }

  override fun onBindViewHolder(holder: RvOrderProductViewHolder, position: Int) {
    holder.bind(data[position], onProductClickListener)
  }

  override fun getItemCount(): Int {
    return data.size
  }
}