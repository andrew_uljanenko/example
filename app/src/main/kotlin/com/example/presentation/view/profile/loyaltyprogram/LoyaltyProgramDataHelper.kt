package com.example.presentation.view.profile.loyaltyprogram

import com.example.domain.model.common.Status
import com.example.util.extension.observe
import com.lonecrab.multistateview.MultiStateView
import kotlinx.android.synthetic.main.fr_loyalty_program.*

interface LoyaltyProgramDataHelper {

  fun LoyaltyProgramFragment.setupLoyaltyCardLiveData() {
    viewModel.loyaltyCardLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.CONTENT
          viewModel.cardNumber = it.data

          setupViewsVisibility()
        }
        else -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.ERROR
          //todo
        }
      }
    }
  }

  fun LoyaltyProgramFragment.setupActivateCardLiveData() {
    viewModel.activateCardLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.CONTENT
          viewModel.cardNumber = "2100568521714" //todo
          setActivatedCardMode()
        }
        else -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.ERROR
          //todo
        }
      }
    }
  }

  fun LoyaltyProgramFragment.setupDetachPhoneNumberLiveData() {
    viewModel.detachPhoneNumberLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.CONTENT
        }
        else -> {
          msvLoyaltyProgram.viewState = MultiStateView.ViewState.ERROR
          //todo
        }
      }
    }
  }

  private fun LoyaltyProgramFragment.setupViewsVisibility() {
    val cardNumber = viewModel.cardNumber

    if (cardNumber.isNullOrEmpty()) {
      setNonActivatedCardMode()
    } else {
      setActivatedCardMode()
    }
  }
}