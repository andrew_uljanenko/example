package com.example.presentation.view.profile.emailsettings

import com.example.domain.subscription.Subscription
import com.example.presentation.view.profile.emailsettings.adapter.RvSubscriptionsAdapter
import com.example.util.extension.clickToast
import kotlinx.android.synthetic.main.fr_email_settings.*

interface EmailSettingsContent {

  var subscriptionsAdapter: RvSubscriptionsAdapter?

  fun EmailSettingsFragment.setupEmailSettingsRecyclerView() {
    if (subscriptionsAdapter == null) {
      subscriptionsAdapter = RvSubscriptionsAdapter()
    }

    rvSubscriptions.adapter = subscriptionsAdapter

    subscriptionsAdapter?.onSubscriptionCheckedChangeListener = { subscription: Subscription, isChecked: Boolean ->
      run {
        clickToast("${subscription.description} $isChecked")
      }
    }
  }

  fun EmailSettingsFragment.populateSubscriptions(subscriptions: List<Subscription>) {
    subscriptionsAdapter?.let {
      it.data.addAll(subscriptions)
      it.notifyDataSetChanged()
    }
  }
}