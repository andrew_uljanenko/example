package com.example.presentation.view.profile.ordershistory

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.HideBottomNavigation
import com.example.presentation.view.profile.ordershistory.adapter.RvOrdersAdapter

class OrdersHistoryFragment : BaseViewPagerViewModelChildFragment<OrdersHistoryViewModel>(),
                              HideBottomNavigation,
                              OrdersHistoryDataHelper,
                              OrdersHistoryContent {

  override var ordersAdapter: RvOrdersAdapter? = null
  override var firstPageLoaded: Boolean = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setTitle(R.string.orders_history_title)
    setLayoutResId(R.layout.fr_orders_history)

    setupOrdersLiveData()

    //todo add empty view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupOrdersRecyclerView()

    viewModel.fetchOrders()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}