package com.example.presentation.view.profile

import android.os.Bundle
import com.example.di.module.fragment.profile.ProfileFragmentContainerModule
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.common.fragment.BaseViewPagerContainerFragment
import com.example.presentation.view.main.MainScreens
import com.example.util.extension.attachToLifecycle
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import javax.inject.Inject
import javax.inject.Named

class ProfileFragmentContainerFragment : BaseViewPagerContainerFragment() {

  @field:[Inject Named(ProfileFragmentContainerModule.PROFILE_ROUTER)]
  override lateinit var router: Router

  @field:[Inject Named(ProfileFragmentContainerModule.PROFILE_CICERONE_MANAGER)]
  lateinit var ciceroneManager: CiceroneManager

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    ciceroneManager.attachToLifecycle(this)
  }

  override fun getRootScreen(): Screen {
    return MainScreens.ProfileScreen()
  }
}