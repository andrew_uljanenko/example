package com.example.presentation.view.profile

import com.example.domain.model.common.Status
import com.example.presentation.common.widget.hideProgressBar
import com.example.presentation.common.widget.showProgressBar
import com.example.util.extension.observe
import kotlinx.android.synthetic.main.lt_toolbar_progress_bar.*

interface ProfileDataHelper {

  fun ProfileFragment.setupProgressLiveData() {
    viewModel.progressLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          pbContent.showProgressBar()
          bannersCarouselSkeletonScreen?.show()
          buyerInfoSkeletonScreen?.show()
        }
        Status.SUCCESS -> {
          pbContent.hideProgressBar()
          it.data?.let { profileResponse ->
            populateOrder(profileResponse.order)
            populateCarousel(profileResponse.banners)
            populateBuyerInfo(profileResponse.user)
          }
        }
        else -> {
          pbContent.hideProgressBar()

          //todo handle error with throwable processor and snackbar with retry
        }
      }
    }
  }
}