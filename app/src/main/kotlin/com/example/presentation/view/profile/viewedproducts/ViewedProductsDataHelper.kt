package com.example.presentation.view.profile.viewedproducts

import com.example.util.extension.observe

interface ViewedProductsDataHelper {

  fun ViewedProductsFragment.setupReviewsLiveData() {
    viewModel.viewedProductsLiveData.observe(this) {
      when (it?.status) {
        com.example.domain.model.common.Status.LOADING -> {
          showProgress()
        }
        com.example.domain.model.common.Status.SUCCESS -> {
          hideProgress()

          it.data?.let { products ->
            populateProducts(products)
          }
        }
        else -> {
          //todo
        }
      }
    }
  }
}