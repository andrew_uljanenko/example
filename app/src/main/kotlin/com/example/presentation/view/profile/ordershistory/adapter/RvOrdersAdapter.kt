package com.example.presentation.view.profile.ordershistory.adapter

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.order.Order
import com.example.presentation.common.widget.recyclerview.adapter.common.RvProgressAdapter
import com.example.presentation.view.profile.common.product.OnProductClickListener

class RvOrdersAdapter : RvProgressAdapter<RvOrderViewHolder, Order>() {

  override val data = ArrayList<Order>()

  var onProductClickListener: OnProductClickListener? = null

  override fun createView(parent: ViewGroup): RvOrderViewHolder {
    val view = inflate(R.layout.lt_item_order, parent)
    return RvOrderViewHolder(view)
  }

  override fun bindView(holder: RvOrderViewHolder, position: Int) {
    holder.bind(data[position], onProductClickListener)
  }
}