package com.example.presentation.view.profile.loyaltyprogram.loyaltycard

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.os.bundleOf
import com.example.R
import com.example.presentation.common.fragment.BaseDialogFragment
import com.example.presentation.common.fragment.BaseDialogFragmentCompanion
import com.example.util.BarcodeUtils
import com.example.util.extension.afterMeasured
import com.example.util.extension.load
import kotlinx.android.synthetic.main.fr_loyalty_card_on_boarding.*
import kotlinx.android.synthetic.main.lt_loyalty_card.*

class LoyaltyCardOnBoardingDialogFragment : BaseDialogFragment() {

  override var dontInjectAutomatically: Boolean = true

  lateinit var cardNumber: String

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    readBundle(this)

    setLayoutResId(R.layout.fr_loyalty_card_on_boarding)

    setStyle(STYLE_NO_TITLE, R.style.AppTheme_DialogFragment_Transparent_Black)
  }

  override fun setupDialogSize() {
    val window = dialog?.window ?: return
    val layoutParams = WindowManager.LayoutParams()

    layoutParams.copyFrom(window.attributes)
    layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
    layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT

    window.attributes = layoutParams
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupViews()
  }

  override fun onStart() {
    super.onStart()
    dialog?.window?.setWindowAnimations(R.style.DialogFragment_Animation_Fade)
  }

  private fun setupViews() {
    tvCardNumber.text = cardNumber
    ivBarcodeImage.afterMeasured { setupBarcodeImage() }
    btnUnderstand.setOnClickListener { dismiss() }
  }

  private fun setupBarcodeImage() {
    val barcodeDrawable = BarcodeUtils.generateBarcodeBitmap(
      cardNumber,
      ivBarcodeImage.width,
      ivBarcodeImage.height
    )

    ivBarcodeImage.load(barcodeDrawable)
  }

  companion object : BaseDialogFragmentCompanion<LoyaltyCardOnBoardingDialogFragment>() {

    private const val ARG_CARD_NUMBER = "card_number"

    override fun getDialogFragmentClass() = LoyaltyCardOnBoardingDialogFragment::class

    fun createBundle(cardNumber: String): Bundle? = bundleOf(
      ARG_CARD_NUMBER to cardNumber
    )

    override fun readBundle(fragment: LoyaltyCardOnBoardingDialogFragment) {
      val args = fragment.arguments ?: throw IllegalArgumentException()
      fragment.cardNumber = args.getString(ARG_CARD_NUMBER)!!
    }
  }
}