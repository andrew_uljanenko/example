package com.example.presentation.view.profile.skeletons

import com.example.R
import com.example.domain.model.user.User
import com.example.presentation.view.main.MainScreens
import com.example.presentation.view.profile.ProfileFragment
import com.ethanhua.skeleton.Skeleton
import com.ethanhua.skeleton.SkeletonScreen
import kotlinx.android.synthetic.main.fr_profile.*
import kotlinx.android.synthetic.main.lt_buyer_info.*

interface ProfileBuyerInfo {

  var buyerInfoSkeletonScreen: SkeletonScreen?

  fun ProfileFragment.setupBuyerInfo() {
    ltBuyerInfo.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.BuyerInfoScreen()) }

    if (buyerInfoSkeletonScreen == null) {
      buyerInfoSkeletonScreen = Skeleton
        .bind(ltBuyerInfo)
        .load(R.layout.lt_buyer_info_skeleton)
        .shimmer(false)
        .color(R.color.shimmerColor)
        .build()
    }
  }

  fun ProfileFragment.populateBuyerInfo(user: User?) {
    buyerInfoSkeletonScreen?.hide()

    user?.let {
      val name = "${user.name} ${user.surname}"
      tvFullName.text = name
      tvPhone.text = user.phone
      tvEmail.text = user.email
    }
  }
}