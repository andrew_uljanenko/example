package com.example.presentation.view.profile.emailsettings.adapter

import android.view.View
import com.example.domain.subscription.Subscription
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import kotlinx.android.synthetic.main.lt_item_subscription.*

typealias OnSubscriptionCheckedChangeListener = (subscription: Subscription, isChecked: Boolean) -> Unit

class RvSubscriptionViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(subscription: Subscription, onSubscriptionCheckedChangeListener: OnSubscriptionCheckedChangeListener?) {
    sSubscription.text = subscription.description
    sSubscription.isChecked = subscription.isSubscribed

    sSubscription.setOnCheckedChangeListener { _, isChecked ->
      onSubscriptionCheckedChangeListener?.invoke(subscription, isChecked)
    }
  }
}