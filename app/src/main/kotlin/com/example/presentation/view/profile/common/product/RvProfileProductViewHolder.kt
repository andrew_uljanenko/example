package com.example.presentation.view.profile.common.product

import android.view.View
import com.example.R
import com.example.domain.model.product.Product
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.util.extension.getString
import com.example.util.extension.load
import kotlinx.android.synthetic.main.it_cart_product.*

typealias OnProductClickListener = (Product) -> Unit

class RvOrderProductViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(
      product: Product,
      onProductClickListener: OnProductClickListener?
  ) {
    ivProductImage.load(product.imageSrc, withPlaceholder = true)
    tvProductName.text = product.title
    tvProductPrice.text = getString(R.string.price_format, product.price)

    product.code?.let { tvBarcode.text = getString(R.string.product_barcode_text, it) }

    containerView.setOnClickListener { onProductClickListener?.invoke(product) }
  }
}