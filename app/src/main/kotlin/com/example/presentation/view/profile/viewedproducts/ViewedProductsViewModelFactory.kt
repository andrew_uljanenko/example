package com.example.presentation.view.profile.viewedproducts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class ViewedProductsViewModelFactory(
    private val apiService: ApiService
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(ViewedProductsViewModel::class.java)) {
      return ViewedProductsViewModel(apiService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}