package com.example.presentation.view.profile.myreviews

import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.domain.model.product.Product
import com.example.domain.model.review.Comment
import com.example.domain.model.review.Review
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class MyReviewsViewModel(
    private val apiService: ApiService
) : BaseViewModel() {

  val reviewsLiveData = ResponseLiveData<List<Review>>()

  var offset: Int = 0

  fun fetchReviews() {
    if (reviewsLiveData.isLoading()) {
      return
    }

    reviewsLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { reviewsLiveData.value = Response.success(mockData()) },
        { reviewsLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  private fun mockData(): List<Review> {
    return null
  }
}