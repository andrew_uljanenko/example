package com.example.presentation.view.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService
import com.example.domain.managers.banner.BannersManager

@Suppress("UNCHECKED_CAST")
class ProfileViewModelFactory(
    private val apiService: ApiService,
    private val bannersManager: BannersManager
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
      return ProfileViewModel(apiService, bannersManager) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}