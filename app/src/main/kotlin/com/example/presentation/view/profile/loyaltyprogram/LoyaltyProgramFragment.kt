package com.example.presentation.view.profile.loyaltyprogram

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.HideBottomNavigation
import com.example.util.extension.setDisabled
import kotlinx.android.synthetic.main.fr_loyalty_program.*

class LoyaltyProgramFragment : BaseViewPagerViewModelChildFragment<LoyaltyProgramViewModel>(),
                               HideBottomNavigation,
                               LoyaltyCard,
                               LoyaltyProgramDataHelper {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setTitle(R.string.loyalty_program_title)
    setLayoutResId(R.layout.fr_loyalty_program)

    setupLoyaltyCardLiveData()
    setupActivateCardLiveData()
    setupDetachPhoneNumberLiveData()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupViews()

    viewModel.fetchData()
  }

  private fun setupViews() {
    initLoyaltyCard()

    //todo
    etPhone.setText("+380953876488")
    etPhone.setDisabled()

    btnDetachNumber.setOnClickListener { viewModel.detachPhoneNumber() }
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}