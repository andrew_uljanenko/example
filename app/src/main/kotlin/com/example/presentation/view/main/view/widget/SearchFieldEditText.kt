package com.example.presentation.view.main.view.widget

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.res.ResourcesCompat
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import androidx.transition.TransitionManager
import androidx.transition.TransitionSet
import com.example.R
import com.example.presentation.common.listener.TransitionListener
import com.example.util.animation.Interpolators

typealias OnVoiceInputClickListener = () -> Unit

class SearchFieldEditText @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.editTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr) {

  var onVoiceInputClickListener: OnVoiceInputClickListener? = null

  var isInSearchMode: Boolean = false
    set(value) {
      field = value
      updateCompoundDrawables()
    }

  private val searchButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.selector_ic_search, null)!!
  private val clearButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.ic_clear_gray, null)!!
  private val voiceInputButtonImage = ResourcesCompat.getDrawable(resources, R.drawable.ic_mic_gray, null)!!

  //Todo fix animation
  private val changeBoundsTransition = TransitionSet()
    .addTransition(ChangeBounds())
    .setInterpolator(Interpolators.ACCELERATE_DECELERATE)
    .addListener(
      object : TransitionListener() {
        override fun onTransitionStart(transition: Transition) {
          if (!isInSearchMode && text.isNullOrEmpty()) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(searchButtonImage, null, null, null)
          }
        }

        override fun onTransitionEnd(transition: Transition) {
          updateCompoundDrawables()
        }
      }
    )

  init {
    updateCompoundDrawables()

    background = ResourcesCompat.getDrawable(resources, R.drawable.shape_search_field, null)
    gravity = Gravity.CENTER_VERTICAL
    imeOptions = EditorInfo.IME_ACTION_SEARCH
    inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS

    setHint(R.string.search_hint)
  }

  override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
    super.onTextChanged(text, start, lengthBefore, lengthAfter)

    updateCompoundDrawables()
  }

  private fun updateCompoundDrawablesAndAnimate() {
    val viewGroup = this.parent as? ViewGroup ?: return

    TransitionManager.beginDelayedTransition(viewGroup, changeBoundsTransition)
  }

  private fun updateCompoundDrawables() {
    val actionDrawable = if (isInSearchMode) {
      if (text.isNullOrEmpty()) voiceInputButtonImage else clearButtonImage
    } else {
      null
    }

    setCompoundDrawablesRelativeWithIntrinsicBounds(searchButtonImage, null, actionDrawable, null)
  }

  @SuppressLint("ClickableViewAccessibility")
  override fun onTouchEvent(event: MotionEvent): Boolean {
    if (event.action == MotionEvent.ACTION_UP) {
      if (event.rawX >= (this.right - this.compoundPaddingRight)) {
        if (text.isNullOrEmpty()) {
          onVoiceInputClickListener?.invoke()
        } else {
          text = null
        }

        return true
      }
    }

    return super.onTouchEvent(event)
  }
}