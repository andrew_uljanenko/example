package com.example.presentation.view.profile.ordershistory.adapter

import android.view.View
import com.example.R
import com.example.domain.model.order.Order
import com.example.domain.model.order.OrderStatus
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.presentation.view.profile.common.product.OnProductClickListener
import com.example.presentation.view.profile.common.product.RvProfileProductsAdapter
import com.example.util.extension.createBoldSpannable
import com.example.util.extension.getColor
import com.example.util.extension.getString
import com.example.util.extension.setDrawableLeft
import kotlinx.android.synthetic.main.lt_item_order.*

class RvOrderViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(order: Order, onProductClickListener: OnProductClickListener?) {
    tvTitle.text = order.ttn
    tvDate.text = order.orderTime

    setupTotalPrice(order)
    setupStatus(order)
    setupProductsAdapter(order, onProductClickListener)
  }

  private fun setupStatus(order: Order) {
    when (order.status) {
      OrderStatus.IN_PROCESSING -> {
        tvOrderStatus.text = getString(R.string.in_processing_status)
        tvOrderStatus.setDrawableLeft(R.drawable.ic_order_status_in_processing)
        tvOrderStatus.setTextColor(getColor(R.color.colorBrandLightBlue))
      }
      OrderStatus.CANCELED -> {
        tvOrderStatus.text = getString(R.string.canceled_status)
        tvOrderStatus.setDrawableLeft(R.drawable.ic_order_status_canceled)
        tvOrderStatus.setTextColor(getColor(R.color.colorRed))
      }
      OrderStatus.DONE -> {
        tvOrderStatus.text = getString(R.string.done_status)
        tvOrderStatus.setDrawableLeft(R.drawable.ic_order_status_done)
        tvOrderStatus.setTextColor(getColor(R.color.colorGreen))
      }
    }
  }

  private fun setupTotalPrice(order: Order) {
    order.totalPrice?.let {
      val totalSum = getString(R.string.price_format, it)
      val result = "${getString(R.string.total_title)} $totalSum"
      tvOrderSum.text = result.createBoldSpannable(totalSum)
    }
  }

  private fun setupProductsAdapter(order: Order, onProductClickListener: OnProductClickListener?) {
    val productsAdapter = RvProfileProductsAdapter(order.goods)
    productsAdapter.onProductClickListener = onProductClickListener

    rvOrderProducts.adapter = productsAdapter
  }
}