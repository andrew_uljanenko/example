package com.example.presentation.view.profile.ordershistory

import com.example.domain.model.order.Order
import com.example.presentation.common.widget.hideProgressBar
import com.example.presentation.common.widget.showProgressBar
import com.example.presentation.view.profile.ordershistory.adapter.RvOrdersAdapter
import com.example.util.extension.clickToast
import com.example.util.extension.onScrollEnd
import kotlinx.android.synthetic.main.fr_orders_history.*
import kotlinx.android.synthetic.main.lt_toolbar_progress_bar.*

interface OrdersHistoryContent {

  var ordersAdapter: RvOrdersAdapter?
  var firstPageLoaded: Boolean

  fun OrdersHistoryFragment.setupOrdersRecyclerView() {
    if (ordersAdapter == null) {
      ordersAdapter = RvOrdersAdapter()
    }
    rvOrders.adapter = ordersAdapter
    rvOrders.onScrollEnd {
      if (!viewModel.ordersLiveData.isLoading()) {
        viewModel.fetchOrders()
      }
    }

    ordersAdapter?.onProductClickListener = { clickToast("click product")}
  }

  fun OrdersHistoryFragment.populateOrders(orders: List<Order>) {
    ordersAdapter?.let {
      it.data.addAll(orders)
      it.notifyDataSetChanged()
    }
  }

  fun OrdersHistoryFragment.showProgress() {
    if (!firstPageLoaded) {
      pbContent.showProgressBar()
    } else {
      ordersAdapter?.showProgress()
    }
  }

  fun OrdersHistoryFragment.hideProgress() {
    if (!firstPageLoaded) {
      firstPageLoaded = true
      pbContent.hideProgressBar()
      rvOrders.scheduleLayoutAnimation()
    } else {
      ordersAdapter?.hideProgress()
    }
  }
}