package com.example.presentation.view.main

import androidx.core.util.forEach
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.example.R
import com.example.presentation.common.fragment.StateFragment
import com.example.presentation.common.widget.bottomnavigationviewpager.BottomNavigationViewPagerAdapter
import com.example.presentation.view.main.view.widget.VpMainAdapter
import com.example.util.extension.afterMeasured
import kotlinx.android.synthetic.main.ac_main.*

interface MainViewPager {

  fun MainActivity.setupMainViewPager() {
    var adapter = getViewPagerAdapter()

    if (adapter == null) {
      adapter = VpMainAdapter(supportFragmentManager)
      vpContent.adapter = adapter
      vpContent.offscreenPageLimit = VpMainAdapter.ITEMS_COUNT

      vpContent.afterMeasured {
        adapter.fragments.forEach { _, fragment ->
          fragment.childFragmentManager.addOnBackStackChangedListener(this@setupMainViewPager)
        }
      }

      vpContent.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
          //no-op
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
          //no-op
        }

        override fun onPageSelected(position: Int) {
          animateFragment(getCurrentBottomNavigationFragment() as? StateFragment)

          bottomNavigation.selectedItemId = when (position) {
            VpMainAdapter.HOME_ITEM_POSITION -> R.id.navigation_home
            VpMainAdapter.CART_ITEM_POSITION -> R.id.navigation_cart
            VpMainAdapter.FAVORITES_ITEM_POSITION -> R.id.navigation_favorites
            VpMainAdapter.PROFILE_ITEM_POSITION -> R.id.navigation_profile
            VpMainAdapter.INFO_ITEM_POSITION -> R.id.info
            else -> throw IllegalArgumentException()
          }
        }
      })
    }
  }

  fun MainActivity.getViewPagerAdapter(): BottomNavigationViewPagerAdapter? {
    return vpContent.adapter as? BottomNavigationViewPagerAdapter
  }

  fun MainActivity.getCurrentBottomNavigationFragment(): Fragment? {
    val pagerAdapter = getViewPagerAdapter()
    return pagerAdapter?.getRegisteredFragment(vpContent.currentItem)
  }

  fun MainActivity.getCurrentFragmentChildFragmentManager(): FragmentManager? {
    val currentBottomNavigationFragment = getCurrentBottomNavigationFragment()
    return currentBottomNavigationFragment?.childFragmentManager
  }

  fun MainActivity.getCurrentChildFragment(): Fragment? {
    return getCurrentFragmentChildFragmentManager()?.findFragmentById(R.id.fragmentContainer)
  }

  private fun animateFragment(fragment: StateFragment?) {
    fragment?.willBeDisplayed()
  }
}