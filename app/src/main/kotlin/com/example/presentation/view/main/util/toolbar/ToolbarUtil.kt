@file:Suppress("unused")

package com.example.presentation.view.main.util.toolbar

import android.app.Activity
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Build
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageButton
import androidx.annotation.ColorInt
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.appcompat.widget.ActionMenuView
import androidx.appcompat.widget.Toolbar
import com.example.util.animation.ColorChangeAnimator
import com.example.util.animation.OnColorChangeListener
import com.example.util.viewBackGroundColor


object ToolbarUtil {

  /**
   * Changing layout_toolbar_themed style
   */
  fun colorizeToolbar(
      activity: Activity,
      toolbar: Toolbar,
      @ColorInt newToolbarBgColor: Int,
      @ColorInt newToolbarTextColor: Int,
      @ColorInt toolbarIconsColor: Int,
      animate: Boolean = true,
      onToolbarColorChangeListener: OnColorChangeListener? = null
  ) {
    colorizeToolbarIcons(toolbar, newToolbarTextColor, toolbarIconsColor)

    //Changing the bg color, color of title and subtitle.
    toolbar.setTitleTextColor(newToolbarTextColor)
    toolbar.setSubtitleTextColor(newToolbarTextColor)

    val toolbarBgColor = toolbar.viewBackGroundColor()
    if (toolbarBgColor == newToolbarBgColor) {
      return
    }

    //Change status bar color
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
      activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }

    //Change toolbar color
    if (!animate) {
      changeToolbarAndStatusBarColor(newToolbarBgColor, toolbar, activity)
    } else {
      ColorChangeAnimator.changeColor(toolbarBgColor, newToolbarBgColor) {
        changeToolbarAndStatusBarColor(it, toolbar, activity)
        onToolbarColorChangeListener?.invoke(it)
      }
    }
  }

  private fun changeToolbarAndStatusBarColor(
      color: Int,
      toolbar: Toolbar,
      activity: Activity
  ) {
    toolbar.setBackgroundColor(color)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      activity.window.statusBarColor = color
    }
  }

  private fun colorizeToolbarIcons(toolbar: ViewGroup, @ColorInt actionButtonColor: Int, @ColorInt menuItemsColor: Int) {
    val menuItemsColorFilter = PorterDuffColorFilter(menuItemsColor, PorterDuff.Mode.SRC_ATOP)
    val actionButtonColorFilter = PorterDuffColorFilter(actionButtonColor, PorterDuff.Mode.SRC_ATOP)

    for (i in 0 until toolbar.childCount) {
      val v = toolbar.getChildAt(i) ?: continue

      when (v) {
        is ImageButton -> v.drawable.colorFilter = actionButtonColorFilter //Action Bar back button
        is ActionMenuView -> findMenuItemsAndPaint(v, menuItemsColorFilter, menuItemsColor)
        is ViewGroup -> colorizeToolbarIcons(v, actionButtonColor, menuItemsColor)
      }
    }
  }

  private fun findMenuItemsAndPaint(actionMenuView: ActionMenuView, colorFilter: ColorFilter, color: Int) {
    //Change text color
    for (i in 0 until actionMenuView.menu.size()) {
      val menuItem = actionMenuView.menu.getItem(i)

      menuItem.title = SpannableString(menuItem.title).apply {
        setSpan(ForegroundColorSpan(color), 0, length, 0)
      }
    }

    //Change icons color
    for (i in 0 until actionMenuView.childCount) {
      //Changing the color of any ActionMenuViews - icons that
      //are not back button, nor text, nor overflow menu icon.
      val innerView = actionMenuView.getChildAt(i)

      if (innerView is ActionMenuItemView) {
        paintActionMenuItem(innerView, colorFilter, color)
      }
    }
  }

  private fun paintActionMenuItem(innerView: ActionMenuItemView, colorFilter: ColorFilter, color: Int) {
    val drawablesCount = innerView.compoundDrawables.size
    innerView.post { innerView.setTextColor(color) }
    for (i in 0 until drawablesCount) {
      if (innerView.compoundDrawables[i] != null) {
        //Important to put the color filter in separate thread,
        //by adding it to the message queue
        //Won't work otherwise.
        innerView.post {
          val icon = innerView.compoundDrawables[i]
          icon?.colorFilter = colorFilter
        }
      }
    }
  }
}