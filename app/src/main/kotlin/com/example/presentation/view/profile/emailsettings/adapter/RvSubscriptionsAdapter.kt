package com.example.presentation.view.profile.emailsettings.adapter

import android.view.ViewGroup
import com.example.R
import com.example.domain.subscription.Subscription
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvListAdapter

class RvSubscriptionsAdapter : BaseRvListAdapter<RvSubscriptionViewHolder, Subscription>() {

  override val data = ArrayList<Subscription>()

  var onSubscriptionCheckedChangeListener: OnSubscriptionCheckedChangeListener? = null

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvSubscriptionViewHolder {
    val view = inflate(R.layout.lt_item_subscription, parent)
    return RvSubscriptionViewHolder(view)
  }

  override fun onBindViewHolder(holder: RvSubscriptionViewHolder, position: Int) {
    holder.bind(data[position], onSubscriptionCheckedChangeListener)
  }
}