package com.example.presentation.view.profile.myreviews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class MyReviewsViewModelFactory(
    private val apiService: ApiService
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(MyReviewsViewModel::class.java)) {
      return MyReviewsViewModel(apiService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}