package com.example.presentation.view.profile.myreviews.adapter.comments

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.review.Comment
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvListAdapter

class RvCommentsAdapter(
    override val data: List<Comment>
) : BaseRvListAdapter<RvCommentViewHolder, Comment>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvCommentViewHolder {
    val view = inflate(R.layout.lt_item_comment, parent)
    return RvCommentViewHolder(view)
  }

  override fun onBindViewHolder(holder: RvCommentViewHolder, position: Int) {
    holder.bind(data[position])
  }

  override fun getItemCount(): Int {
    return data.size
  }
}