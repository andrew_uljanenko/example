package com.example.presentation.view.profile.model

import com.example.domain.model.banner.Banner
import com.example.domain.model.order.Order
import com.example.domain.model.user.User

data class ProfileResponse(
    val banners: List<Banner>,
    val user: User?,
    val order: Order?
)