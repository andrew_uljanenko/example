package com.example.presentation.view.profile.myreviews.adapter.comments

import android.view.View
import com.example.domain.model.review.Comment
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import kotlinx.android.synthetic.main.lt_item_comment.*
import kotlinx.android.synthetic.main.lt_like_dislike.*

class RvCommentViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(comment: Comment) {
    tvComment.text = comment.text
    tvLikeCount.text = comment.likeCount.toString()
    tvDislikeCount.text = comment.dislikeCount.toString()
    rbCommentRating.rating = comment.rating.toFloat()
  }
}