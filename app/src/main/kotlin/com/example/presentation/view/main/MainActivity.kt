package com.example.presentation.view.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.forEach
import androidx.core.view.get
import androidx.fragment.app.FragmentManager.*
import com.example.R
import com.example.di.NavigationModule.Companion.APP_ROUTER
import com.example.di.module.activity.MainActivityModule.Companion.MAIN_CICERONE_MANAGER
import com.example.presentation.common.activity.BaseFragmentActivity
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.view.main.manager.ShakeManager
import com.example.util.KeyboardListenerManager
import com.example.util.extension.attachToLifecycle
import com.example.util.extension.setGone
import com.example.util.extension.setOnClickListener
import com.example.util.extension.setVisible
import com.google.android.material.bottomnavigation.BottomNavigationView.*
import kotlinx.android.synthetic.main.ac_main.*
import kotlinx.android.synthetic.main.lt_toolbar.*
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Named


class MainActivity
  : BaseFragmentActivity(),
    OnNavigationItemSelectedListener,
    OnNavigationItemReselectedListener,
    BottomNavigationRouter,
    SearchField,
    MainBackStackProcessor,
    OnBackStackChangedListener,
    KeyboardListenerManager.OnKeyboardToggleListener,
    MainViewPager,
    WelcomeAnimation {

  @field:[Inject Named(MAIN_CICERONE_MANAGER)]
  lateinit var ciceroneManager: CiceroneManager

  @field:[Inject Named(APP_ROUTER)]
  lateinit var router: Router

  private lateinit var keyboardListenerManager: KeyboardListenerManager

  private lateinit var shakeManager: ShakeManager

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.ac_main)

    setupToolbar()
    setupKeyboardListenerManager()
    setupShakeManager()
    setupBottomNavigation()
    setupCicerone()
    setupMainViewPager()
    setupSearchFieldView()
    setupWelcomeAnimation()
  }

  private fun setupToolbar() {
    setSupportActionBar(toolbar)

    arrayOf(ivBackArrow, ivCloseButton).setOnClickListener {
      onBackPressed()
    }
  }

  private fun setupKeyboardListenerManager() {
    keyboardListenerManager = KeyboardListenerManager(this, this)
    keyboardListenerManager.attachToLifecycle(this)
  }

  private fun setupShakeManager() {
    shakeManager = ShakeManager(this)
    shakeManager.attachToLifecycle(this)
  }

  private fun setupBottomNavigation() {
    bottomNavigation.setOnNavigationItemSelectedListener(this)
    bottomNavigation.setOnNavigationItemReselectedListener(this)

    (bottomNavigation[0] as ViewGroup).forEach {
      val activeLabel: View = it.findViewById(com.google.android.material.R.id.largeLabel)
      if (activeLabel is TextView) {
        activeLabel.setPadding(0, 0, 0, 0)
      }
    }
  }

  private fun setupCicerone() {
    ciceroneManager.attachToLifecycle(this)
  }

  override fun setTitle(title: String) {
    tvToolbarTitle.text = title
  }

  override fun setSubTitle(subTitle: String?) {
    tvToolbarSubtitle.text = subTitle

    if (subTitle.isNullOrEmpty()) {
      tvToolbarSubtitle.setGone()
    } else {
      tvToolbarSubtitle.setVisible()
    }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    val navigationResult = routeBottomNavigationMenuItem(item)

    if (navigationResult) {
      adjustViewDependingOnCurrentFragment()
    }

    return true
  }

  override fun onNavigationItemReselected(item: MenuItem) {
    routeReselectedBottomNavigationMenuItem()
  }

  override fun onKeyboardToggle(opened: Boolean) {
    adjustViewDependingOnKeyboardToggle(opened)
  }

  override fun onBackStackChanged() {
    adjustViewDependingOnCurrentFragment()
  }

  override fun onBackPressed() {
    processBackPress()
  }
}