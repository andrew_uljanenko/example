package com.example.presentation.view.profile.viewedproducts.adapter

import android.view.View
import com.example.domain.model.product.Product
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.presentation.view.profile.common.product.OnProductClickListener
import com.example.presentation.view.profile.common.product.RvProfileProductsAdapter
import com.example.presentation.view.profile.model.ViewedProduct
import kotlinx.android.synthetic.main.lt_item_viewed_product.*

class RvViewedProductViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(viewedProduct: ViewedProduct, onProductClickListener: OnProductClickListener?) {
    tvTitle.text = viewedProduct.date
    setupProductsAdapter(viewedProduct.products, onProductClickListener)
  }

  private fun setupProductsAdapter(
      products: List<Product>,
      onProductClickListener: OnProductClickListener?
  ) {
    val productsAdapter = RvProfileProductsAdapter(products)
    productsAdapter.onProductClickListener = onProductClickListener
    rvProducts.adapter = productsAdapter
  }
}