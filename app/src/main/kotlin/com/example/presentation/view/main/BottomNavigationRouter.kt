package com.example.presentation.view.main

import android.view.MenuItem
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerContainerFragment
import com.example.presentation.view.main.view.widget.VpMainAdapter.Companion.CART_ITEM_POSITION
import com.example.presentation.view.main.view.widget.VpMainAdapter.Companion.FAVORITES_ITEM_POSITION
import com.example.presentation.view.main.view.widget.VpMainAdapter.Companion.HOME_ITEM_POSITION
import com.example.presentation.view.main.view.widget.VpMainAdapter.Companion.INFO_ITEM_POSITION
import com.example.presentation.view.main.view.widget.VpMainAdapter.Companion.PROFILE_ITEM_POSITION
import kotlinx.android.synthetic.main.ac_main.*

interface BottomNavigationRouter {

  fun MainActivity.routeBottomNavigationMenuItem(item: MenuItem): Boolean {
    val itemPosition = when (item.itemId) {
      R.id.navigation_home -> HOME_ITEM_POSITION
      R.id.navigation_cart -> CART_ITEM_POSITION
      R.id.navigation_favorites -> FAVORITES_ITEM_POSITION
      R.id.navigation_profile -> PROFILE_ITEM_POSITION
      R.id.navigation_info -> INFO_ITEM_POSITION
      else -> throw IllegalArgumentException()
    }

    if (itemPosition != vpContent.currentItem) {
      vpContent.setCurrentItem(itemPosition, false)
      return true
    } else {
      return false
    }
  }

  fun MainActivity.routeReselectedBottomNavigationMenuItem() {
    getCurrentBottomNavigationFragment()?.let {
      if (it is BaseViewPagerContainerFragment) {
        val rootScreen = it.getRootScreen()

        if (it.childFragmentManager.backStackEntryCount == 1) {
          it.scrollToTop()
        } else {
          it.router.backTo(rootScreen)
        }
      }
    }
  }
}