package com.example.presentation.view.main

import android.view.MotionEvent
import com.example.presentation.common.fragment.BaseViewPagerContainerFragment
import kotlinx.android.synthetic.main.lt_toolbar.*

interface SearchField {

  fun MainActivity.setupSearchFieldView() {
    etSearch.setOnTouchListener { _, event ->
      if (event.action != MotionEvent.ACTION_UP) {
        return@setOnTouchListener false
      }

      if (getCurrentChildFragment() !is HasExpandedSearchField) {
        (getCurrentBottomNavigationFragment() as BaseViewPagerContainerFragment).router.navigateTo(MainScreens.SearchScreen())
      }

      return@setOnTouchListener false
    }
  }
}