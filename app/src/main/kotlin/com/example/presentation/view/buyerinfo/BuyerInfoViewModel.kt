package com.example.presentation.view.buyerinfo

import com.example.domain.managers.cart.CartManager
import com.example.domain.model.common.Response
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class BuyerInfoViewModel(
    private val cartManager: CartManager
) : BaseViewModel() {

  val saveLiveData = ResponseLiveData<String>()

  fun save() {
    if (saveLiveData.isLoading()) {
      return
    }

    saveLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { saveLiveData.value = Response.success("") },
        { saveLiveData.value = Response.error(Throwable("")) }
      )
      .registerDisposable()
  }
}