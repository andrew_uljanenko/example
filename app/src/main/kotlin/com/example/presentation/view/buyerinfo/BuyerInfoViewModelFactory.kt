package com.example.presentation.view.buyerinfo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.domain.managers.cart.CartManager

@Suppress("UNCHECKED_CAST")
class BuyerInfoViewModelFactory(
    private val cartManager: CartManager
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(BuyerInfoViewModel::class.java)) {
      return BuyerInfoViewModel(cartManager) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}