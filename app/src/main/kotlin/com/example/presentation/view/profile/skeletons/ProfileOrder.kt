package com.example.presentation.view.profile.skeletons

import com.example.domain.model.order.Order
import com.example.presentation.view.profile.ProfileFragment
import com.example.util.extension.load
import com.example.util.extension.setVisible
import kotlinx.android.synthetic.main.fr_profile.*
import kotlinx.android.synthetic.main.lt_profile_orders.*

interface ProfileOrder {

  fun ProfileFragment.populateOrder(order: Order?) {
    order?.let {
      ltProfileOrders.setVisible()

      //todo
      ivOrderImage.load(
        "https://example.com/uploaded-images/products_photos/size_400/516624_2.jpg",
        withPlaceholder = true
      )
    }

  }
}