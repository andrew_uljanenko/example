package com.example.presentation.view.profile.changepassword

import com.example.domain.model.common.Status
import com.example.presentation.view.main.MainScreens
import com.example.presentation.view.successaction.SuccessActionMode
import com.example.util.extension.observe
import com.lonecrab.multistateview.MultiStateView
import kotlinx.android.synthetic.main.fr_profile_change_password.*

interface ChangePasswordResponse {

  fun ChangePasswordFragment.setupSaveLiveData() {
    viewModel.saveLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvChangePassword.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvChangePassword.viewState = MultiStateView.ViewState.CONTENT

          parentFragmentRouter()?.replaceScreen(MainScreens.SuccessActionScreen(SuccessActionMode.CHANGE_PASSWORD))
        }
        else -> {
          msvChangePassword.viewState = MultiStateView.ViewState.ERROR
        }
      }
    }
  }
}