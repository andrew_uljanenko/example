package com.example.presentation.view.profile.viewedproducts

import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.domain.model.product.Product
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.presentation.view.profile.model.ViewedProduct
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class ViewedProductsViewModel(
    private val apiService: ApiService
) : BaseViewModel() {

  val viewedProductsLiveData = ResponseLiveData<List<ViewedProduct>>()

  var offset: Int = 0

  fun fetchProducts() {
    if (viewedProductsLiveData.isLoading()) {
      return
    }

    viewedProductsLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { viewedProductsLiveData.value = Response.success(mockData()) },
        { viewedProductsLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  private fun mockData(): List<ViewedProduct> {
    return null
  }

}