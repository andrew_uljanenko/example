package com.example.presentation.view.profile.myreviews

import com.example.util.extension.observe

interface MyReviewsDataHelper {

  fun MyReviewsFragment.setupReviewsLiveData() {
    viewModel.reviewsLiveData.observe(this) {
      when (it?.status) {
        com.example.domain.model.common.Status.LOADING -> {
          showProgress()
        }
        com.example.domain.model.common.Status.SUCCESS -> {
          hideProgress()

          it.data?.let { reviews ->
            populateReviews(reviews)
          }
        }
        else -> {
          //todo
        }
      }
    }
  }
}