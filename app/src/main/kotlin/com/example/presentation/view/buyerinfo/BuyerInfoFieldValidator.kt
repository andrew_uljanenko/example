package com.example.presentation.view.buyerinfo

import com.example.R
import com.example.util.validator.utils.TextInputLayoutValidator
import com.example.util.validator.validators.EmailValidator
import com.example.util.validator.validators.PhoneValidator
import kotlinx.android.synthetic.main.fr_buyer_info.*

interface BuyerInfoFieldValidator {

  fun BuyerInfoFragment.registerFieldValidators() {
    TextInputLayoutValidator
      .forView(tilEmail)
      .addValidator(EmailValidator(getString(R.string.incorrect_email_error)))
      .build()
      .registerValidator()

    TextInputLayoutValidator
      .forView(tilPhone)
      .addValidator(PhoneValidator(getString(R.string.incorrect_phone_number_error)))
      .build()
      .registerValidator()
  }
}