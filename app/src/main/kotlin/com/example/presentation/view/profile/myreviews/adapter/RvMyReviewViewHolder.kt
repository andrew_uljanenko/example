package com.example.presentation.view.profile.myreviews.adapter

import android.view.View
import com.example.R
import com.example.domain.model.product.Product
import com.example.domain.model.review.Comment
import com.example.domain.model.review.Review
import com.example.presentation.common.widget.recyclerview.adapter.common.BaseRvViewHolder
import com.example.presentation.view.profile.common.product.OnProductClickListener
import com.example.presentation.view.profile.myreviews.adapter.comments.RvCommentsAdapter
import com.example.util.extension.getString
import com.example.util.extension.load
import kotlinx.android.synthetic.main.lt_item_product.*
import kotlinx.android.synthetic.main.lt_item_my_review.*

class RvMyReviewViewHolder(override val containerView: View) : BaseRvViewHolder(containerView) {

  fun bind(review: Review, onProductClickListener: OnProductClickListener?) {
    tvTitle.text = review.date
    bindProduct(review.good, onProductClickListener)
    setupCommentsAdapter(review.comments)
  }

  private fun bindProduct(
      product: Product,
      onProductClickListener: OnProductClickListener?
  ) {
    ivProductImage.load(product.imageSrc, withPlaceholder = true)
    tvProductName.text = product.title
    tvProductPrice.text = getString(R.string.price_format, product.price)

    product.code?.let { tvBarcode.text = getString(R.string.product_barcode_text, it) }

    containerView.setOnClickListener { onProductClickListener?.invoke(product) }
  }

  private fun setupCommentsAdapter(comments: List<Comment>) {
    val commentsAdapter = RvCommentsAdapter(comments)
    rvComments.adapter = commentsAdapter
  }
}