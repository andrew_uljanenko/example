package com.example.presentation.view.profile.ordershistory

import com.example.domain.model.common.Status
import com.example.util.extension.observe

interface OrdersHistoryDataHelper {

  fun OrdersHistoryFragment.setupOrdersLiveData() {
    viewModel.ordersLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          showProgress()
        }
        Status.SUCCESS -> {
          hideProgress()

          it.data?.let { orders ->
            populateOrders(orders)
          }
        }
        else -> {
          //todo
        }
      }
    }
  }
}