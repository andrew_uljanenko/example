package com.example.presentation.view.profile

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.carousel.RvCarouselAdapter
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.MainScreens
import com.example.presentation.view.profile.skeletons.ProfileBanner
import com.example.presentation.view.profile.skeletons.ProfileBuyerInfo
import com.example.presentation.view.profile.skeletons.ProfileOrder
import com.ethanhua.skeleton.SkeletonScreen
import kotlinx.android.synthetic.main.lt_profile_menu.*

class ProfileFragment : BaseViewPagerViewModelChildFragment<ProfileViewModel>(),
                        ProfileBanner,
                        ProfileBuyerInfo,
                        ProfileOrder,
                        ProfileDataHelper {

  override var carouselAdapter: RvCarouselAdapter? = null
  override var bannersCarouselSkeletonScreen: SkeletonScreen? = null
  override var buyerInfoSkeletonScreen: SkeletonScreen? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setLayoutResId(R.layout.fr_profile)
    setTitle(R.string.my_profile_title)

    setupProgressLiveData()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupViews()
    setupListeners()
  }

  override fun onCompletelyVisibleForUser() {
    super.onCompletelyVisibleForUser()

    viewModel.fetchDataIfNeeded()
  }

  private fun setupListeners() {
    llChangePassword.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.ChangePasswordScreen()) }
    llChildrenProfile.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.ChildrenProfileScreen()) }
    llLoyaltyProgram.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.LoyaltyProgramScreen()) }
    llOrdersHistory.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.OrdersHistoryScreen()) }
    llMyFeedbacks.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.MyReviewsScreen()) }
    llViewedProducts.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.ViewedProductsScreen()) }
    llEmailSettings.setOnClickListener { parentFragmentRouter()?.navigateTo(MainScreens.EmailSettingsScreen()) }
  }

  private fun setupViews() {
    setupCarousel()
    setupBuyerInfo()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}