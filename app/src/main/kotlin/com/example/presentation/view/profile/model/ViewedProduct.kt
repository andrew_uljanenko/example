package com.example.presentation.view.profile.model

import com.example.domain.model.product.Product

data class ViewedProduct(
    val date: String,
    val products: List<Product>
)