package com.example.presentation.view.profile.changepassword

import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class ChangePasswordViewModel(
    private val apiService: ApiService
) : BaseViewModel() {

  val saveLiveData = ResponseLiveData<String>()

  fun save(oldPassword: String, newPassword: String) {
    if (saveLiveData.isLoading()) {
      return
    }

    saveLiveData.value = Response.loading()

    Observable.timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { saveLiveData.value = Response.success() },
        { saveLiveData.value = Response.error() }
      )
      .registerDisposable()
  }
}