package com.example.presentation.view.profile.loyaltyprogram

import android.graphics.drawable.TransitionDrawable
import com.example.R
import com.example.presentation.view.profile.loyaltyprogram.loyaltycard.LoyaltyCardDialogFragment
import com.example.presentation.view.profile.loyaltyprogram.loyaltycard.LoyaltyCardOnBoardingDialogFragment
import com.example.util.BarcodeUtils
import com.example.util.extension.afterMeasured
import com.example.util.extension.getDrawableRes
import com.example.util.extension.load
import com.example.util.extension.setGone
import com.example.util.extension.setVisible
import com.example.util.extension.showDialog
import com.example.util.handlerPostDelayed
import kotlinx.android.synthetic.main.fr_loyalty_program.*
import kotlinx.android.synthetic.main.lt_loyalty_card.*
import java.util.concurrent.TimeUnit

interface LoyaltyCard {

  fun LoyaltyProgramFragment.setNonActivatedCardMode() {
    cvLoyaltyCard.elevation = 0f
    tvCardNumber.text = getString(R.string.empty_loyalty_card_number)
    ivBarcodeImage.setGone()

    tvHintMessage.setGone()
    tvDoNotHaveACardTitle.setVisible()
    btnActivateCard.setVisible()

    clCardContainer.setOnClickListener { }
    btnActivateCard.setOnClickListener { viewModel.activateCard() }
  }

  fun LoyaltyProgramFragment.setActivatedCardMode() {
    val cardNumber = viewModel.cardNumber ?: return

    cvLoyaltyCard.elevation = resources.getDimension(R.dimen.loyalty_card_corner_radius)
    tvCardNumber.text = cardNumber

    ivBarcodeImage.setVisible()
    ivBarcodeImage.afterMeasured {
      setupBarcodeImage(cardNumber)
    }

    tvHintMessage.setVisible()
    tvDoNotHaveACardTitle.setGone()
    btnActivateCard.setGone()

    clCardContainer.setOnClickListener { showLoyaltyCardFragment(cardNumber) }

    setupChangeColorAnimation()
    scheduleOnBoardingScreen(cardNumber)
  }

  fun LoyaltyProgramFragment.initLoyaltyCard() {
    cvLoyaltyCard.elevation = 0f
    clCardContainer.background = getDrawableRes(R.drawable.loyalty_card_transition)
    tvCardNumber.text = getString(R.string.empty_loyalty_card_number)
    ivBarcodeImage.setGone()
  }

  private fun LoyaltyProgramFragment.setupChangeColorAnimation() {
    val transition: TransitionDrawable = clCardContainer.background as TransitionDrawable
    transition.startTransition(600)

  }

  private fun LoyaltyProgramFragment.scheduleOnBoardingScreen(cardNumber: String) {
    if (viewModel.wasOnBoardingScreenShown()) return

    handlerPostDelayed(1, TimeUnit.SECONDS) {
      context ?: return@handlerPostDelayed

      showDialog(
        LoyaltyCardOnBoardingDialogFragment(),
        LoyaltyCardOnBoardingDialogFragment.getScreenKey(),
        LoyaltyCardOnBoardingDialogFragment.createBundle(cardNumber)
      )

      viewModel.setOnBoardingScreenWasShown()
    }
  }

  private fun LoyaltyProgramFragment.setupBarcodeImage(cardNumber: String) {
    val barcodeDrawable = BarcodeUtils.generateBarcodeBitmap(
      cardNumber,
      ivBarcodeImage.width,
      ivBarcodeImage.height
    )

    ivBarcodeImage.load(barcodeDrawable)
  }

  private fun LoyaltyProgramFragment.showLoyaltyCardFragment(cardNumber: String) {
    showDialog(
      LoyaltyCardDialogFragment(),
      LoyaltyCardDialogFragment.getScreenKey(),
      LoyaltyCardDialogFragment.createBundle(cardNumber)
    )
  }
}