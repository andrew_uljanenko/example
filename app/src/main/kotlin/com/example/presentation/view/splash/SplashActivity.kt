package com.example.presentation.view.splash

import android.os.Bundle
import com.example.di.NavigationModule.Companion.APP_ROUTER
import com.example.presentation.common.activity.BaseActivity
import com.example.presentation.common.cicerone.CiceroneManager
import com.example.presentation.view.GeneralScreens
import com.example.util.extension.attachToLifecycle
import com.example.util.extension.daggerInject
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import javax.inject.Named


class SplashActivity : BaseActivity() {

  @Inject
  lateinit var ciceroneManager: CiceroneManager

  @field:[Inject Named(APP_ROUTER)]
  lateinit var router: Router


  override fun onCreate(savedInstanceState: Bundle?) {
    daggerInject()
    super.onCreate(savedInstanceState)

    ciceroneManager.attachToLifecycle(this)
    router.replaceScreen(GeneralScreens.MainScreen())
  }
}