package com.example.presentation.view.profile

import com.example.data.ApiService
import com.example.domain.managers.banner.BannersManager
import com.example.domain.model.banner.Banner
import com.example.domain.model.common.Response
import com.example.domain.model.order.Order
import com.example.domain.model.user.User
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.presentation.view.profile.model.ProfileResponse
import com.example.util.extension.applySchedulers
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

class ProfileViewModel(
    private val apiService: ApiService,
    private val bannersManager: BannersManager
) : BaseViewModel() {

  val progressLiveData = ResponseLiveData<ProfileResponse>()

  fun fetchDataIfNeeded() {
    if (progressLiveData.isLoadingOrSuccess()) {
      return
    }

    progressLiveData.value = Response.loading()

    val timerSingle = Single.timer(3, TimeUnit.SECONDS) // todo delete

    Single.zip(bannersManager.get(), timerSingle, BiFunction {
        banners: List<Banner>, t2: Long -> return@BiFunction ProfileResponse(banners, mockUser(), mockOrder())
    })
      .applySchedulers()
      .subscribe(
        { progressLiveData.value = Response.success(it) },
        { progressLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  private fun mockOrder(): Order? {
    return null
  }

  private fun mockUser(): User? {
    return User(
      "Василий",
      "ул. Киевская",
      "Киев",
      "test@gmail.com",
      "",
      "Олегович",
      "+380959888733",
      "Афанасьев"
    )
  }
}