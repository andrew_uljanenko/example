package com.example.presentation.view.profile.viewedproducts

import com.example.presentation.common.widget.hideProgressBar
import com.example.presentation.common.widget.showProgressBar
import com.example.presentation.view.profile.model.ViewedProduct
import com.example.presentation.view.profile.viewedproducts.adapter.RvViewedProductsAdapter
import com.example.util.extension.clickToast
import com.example.util.extension.onScrollEnd
import kotlinx.android.synthetic.main.fr_viewed_products.*
import kotlinx.android.synthetic.main.lt_toolbar_progress_bar.*

interface ViewedProductsContent {

  var productsAdapter: RvViewedProductsAdapter?
  var firstPageLoaded: Boolean

  fun ViewedProductsFragment.setupViewedProductsRecyclerView() {
    if (productsAdapter == null) {
      productsAdapter = RvViewedProductsAdapter()
    }
    rvViewedProducts.adapter = productsAdapter
    rvViewedProducts.onScrollEnd {
      if (!viewModel.viewedProductsLiveData.isLoading()) {
        viewModel.fetchProducts()
      }
    }

    productsAdapter?.onProductClickListener = { clickToast("click product") }
  }

  fun ViewedProductsFragment.populateProducts(viewedProducts: List<ViewedProduct>) {
    productsAdapter?.let {
      it.data.addAll(viewedProducts)
      it.notifyDataSetChanged()
    }
  }

  fun ViewedProductsFragment.showProgress() {
    if (!firstPageLoaded) {
      pbContent.showProgressBar()
    } else {
      productsAdapter?.showProgress()
    }
  }

  fun ViewedProductsFragment.hideProgress() {
    if (!firstPageLoaded) {
      firstPageLoaded = true
      pbContent.hideProgressBar()
      rvViewedProducts.scheduleLayoutAnimation()
    } else {
      productsAdapter?.hideProgress()
    }
  }
}