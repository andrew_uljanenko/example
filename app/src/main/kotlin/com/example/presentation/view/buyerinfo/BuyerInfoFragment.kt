package com.example.presentation.view.buyerinfo

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.example.R
import com.example.presentation.view.main.HideBottomNavigation
import com.example.util.extension.clickToast
import com.example.util.validator.FieldValidatorViewPagerViewModelChildFragment
import kotlinx.android.synthetic.main.lt_delivery_address.*

class BuyerInfoFragment : FieldValidatorViewPagerViewModelChildFragment<BuyerInfoViewModel>(),
                          BuyerInfoFieldValidator,
                          BuyerInfoResponse,
                          HideBottomNavigation {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setLayoutResId(R.layout.fr_buyer_info)
    setMenuResId(R.menu.save)
    setTitle(R.string.buyer_info_title)

    setupSaveLiveData()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupListeners()
  }

  override fun setupFieldValidators() {
    super.setupFieldValidators()

    registerFieldValidators()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    if (item?.itemId == R.id.action_save) {
      validateAndSave()
      return true
    }

    return super.onOptionsItemSelected(item)
  }

  private fun validateAndSave() {
    if (validateFields()) {
      viewModel.save()
    }
  }

  private fun setupListeners() {
    btnDetermineLocation.setOnClickListener { clickToast("Click") }
    clCity.setOnClickListener { clickToast("Click") }
    clStreet.setOnClickListener { clickToast("Click") }
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}