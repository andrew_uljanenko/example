package com.example.presentation.view.profile.emailsettings

import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.domain.subscription.Subscription
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class EmailSettingsViewModel(
    private val apiService: ApiService
) : BaseViewModel() {

  val emailSettingsLiveData = ResponseLiveData<List<Subscription>>()

  fun fetchSettings() {
    if (emailSettingsLiveData.isLoadingOrSuccess()) {
      return
    }

    emailSettingsLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { emailSettingsLiveData.value = Response.success(mockData()) },
        { emailSettingsLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  private fun mockData(): List<Subscription> {
    return arrayListOf(
      Subscription(
        1,
        "2",
        "Рекомендации на основе предыдущих покупок",
        true
      ),
      Subscription(
        1,
        "2",
        "Рекомендации и статьи",
        false
      ),
      Subscription(
        1,
        "2",
        "Акции и специальные предложения",
        false
      ),
      Subscription(
        1,
        "2",
        "Новости",
        true
      )
    )
  }
}