package com.example.presentation.view.profile.loyaltyprogram

import android.content.SharedPreferences
import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class LoyaltyProgramViewModel(
    private val apiService: ApiService,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel() {

  val loyaltyCardLiveData = ResponseLiveData<String>()
  val activateCardLiveData = ResponseLiveData<String>()
  val detachPhoneNumberLiveData = ResponseLiveData<String>()

  var cardNumber: String? = null

  fun fetchData() {
    if (loyaltyCardLiveData.isLoading()) {
      return
    }

    loyaltyCardLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { loyaltyCardLiveData.value = Response.success() },
        { loyaltyCardLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  fun activateCard() {
    if (activateCardLiveData.isLoading()) {
      return
    }

    activateCardLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { activateCardLiveData.value = Response.success() },
        { activateCardLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  fun detachPhoneNumber() {
    if (detachPhoneNumberLiveData.isLoading()) {
      return
    }

    detachPhoneNumberLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { detachPhoneNumberLiveData.value = Response.success() },
        { detachPhoneNumberLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  fun setOnBoardingScreenWasShown() {
    //todo uncomment before release
//    sharedPreferences
//      .edit()
//      .putBoolean(PREF_ON_BOARDING_SCREEN_WAS_SHOWN, true)
//      .apply()
  }

  fun wasOnBoardingScreenShown(): Boolean {
    return sharedPreferences.getBoolean(PREF_ON_BOARDING_SCREEN_WAS_SHOWN, false)
  }

  companion object {
    const val PREF_ON_BOARDING_SCREEN_WAS_SHOWN = "pref_on_boarding_screen_was_shown"
  }
}