package com.example.presentation.view.profile.changepassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class ChangePasswordViewModelFactory(
    private val apiService: ApiService
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(ChangePasswordViewModel::class.java)) {
      return ChangePasswordViewModel(apiService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}