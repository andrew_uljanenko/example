package com.example.presentation.view.profile.emailsettings

import com.example.domain.model.common.Status
import com.example.util.extension.observe
import com.lonecrab.multistateview.MultiStateView
import kotlinx.android.synthetic.main.fr_email_settings.*

interface EmailSettingsDataHelper {

  fun EmailSettingsFragment.setupEmailSettingsLiveData() {
    viewModel.emailSettingsLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvEmailSettings.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvEmailSettings.viewState = MultiStateView.ViewState.CONTENT

          it.data?.let { subscriptions ->
            populateSubscriptions(subscriptions)
          }
        }
        else -> {
          msvEmailSettings.viewState = MultiStateView.ViewState.ERROR

          //todo
        }
      }
    }
  }
}