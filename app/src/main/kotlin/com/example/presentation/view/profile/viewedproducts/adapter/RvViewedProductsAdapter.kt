package com.example.presentation.view.profile.viewedproducts.adapter

import android.view.ViewGroup
import com.example.R
import com.example.presentation.common.widget.recyclerview.adapter.common.RvProgressAdapter
import com.example.presentation.view.profile.common.product.OnProductClickListener
import com.example.presentation.view.profile.model.ViewedProduct

class RvViewedProductsAdapter : RvProgressAdapter<RvViewedProductViewHolder, ViewedProduct>() {

  override val data = ArrayList<ViewedProduct>()

  var onProductClickListener: OnProductClickListener? = null

  override fun createView(parent: ViewGroup): RvViewedProductViewHolder {
    val view = inflate(R.layout.lt_item_viewed_product, parent)
    return RvViewedProductViewHolder(view)
  }

  override fun bindView(holder: RvViewedProductViewHolder, position: Int) {
    holder.bind(data[position], onProductClickListener)
  }
}