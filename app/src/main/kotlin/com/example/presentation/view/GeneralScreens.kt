package com.example.presentation.view

import com.example.presentation.common.cicerone.ActivityScreen
import com.example.presentation.view.authorize.AuthorizeActivity
import com.example.presentation.view.main.MainActivity
import com.example.presentation.view.scanbarcode.ScanBarcodeActivity
import com.example.presentation.view.splash.SplashActivity


object GeneralScreens {

  class SplashScreen : ActivityScreen<SplashActivity>() {
    override fun getActivityClass() = SplashActivity::class
  }

  class MainScreen : ActivityScreen<MainActivity>() {
    override fun getActivityClass() = MainActivity::class
  }

  class AuthorizeScreen : ActivityScreen<AuthorizeActivity>() {
    override fun getActivityClass() = AuthorizeActivity::class
  }

  class ScanBarcodeScreen : ActivityScreen<ScanBarcodeActivity>() {
    override fun getActivityClass() = ScanBarcodeActivity::class
  }
}