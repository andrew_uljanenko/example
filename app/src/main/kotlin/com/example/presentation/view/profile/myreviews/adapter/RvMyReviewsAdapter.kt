package com.example.presentation.view.profile.myreviews.adapter

import android.view.ViewGroup
import com.example.R
import com.example.domain.model.review.Review
import com.example.presentation.common.widget.recyclerview.adapter.common.RvProgressAdapter
import com.example.presentation.view.profile.common.product.OnProductClickListener

class RvMyReviewsAdapter : RvProgressAdapter<RvMyReviewViewHolder, Review>() {

  override val data = ArrayList<Review>()

  var onProductClickListener: OnProductClickListener? = null

  override fun createView(parent: ViewGroup): RvMyReviewViewHolder {
    val view = inflate(R.layout.lt_item_my_review, parent)
    return RvMyReviewViewHolder(view)
  }

  override fun bindView(holder: RvMyReviewViewHolder, position: Int) {
    holder.bind(data[position], onProductClickListener)
  }
}