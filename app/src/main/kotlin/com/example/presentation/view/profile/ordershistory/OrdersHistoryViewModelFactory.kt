package com.example.presentation.view.profile.ordershistory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class OrdersHistoryViewModelFactory(
    private val apiService: ApiService
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(OrdersHistoryViewModel::class.java)) {
      return OrdersHistoryViewModel(apiService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}