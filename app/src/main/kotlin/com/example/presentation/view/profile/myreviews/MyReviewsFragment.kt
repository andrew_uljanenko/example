package com.example.presentation.view.profile.myreviews

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.HideBottomNavigation
import com.example.presentation.view.profile.myreviews.adapter.RvMyReviewsAdapter

class MyReviewsFragment : BaseViewPagerViewModelChildFragment<MyReviewsViewModel>(),
                          HideBottomNavigation,
                          MyReviewsDataHelper,
                          MyReviewsContent {

  override var reviewsAdapter: RvMyReviewsAdapter? = null
  override var firstPageLoaded: Boolean = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setTitle(R.string.my_feedbacks_title)
    setLayoutResId(R.layout.fr_my_reviews)

    setupReviewsLiveData()

    //todo add empty view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupMyReviewsRecyclerView()

    viewModel.fetchReviews()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}