package com.example.presentation.view.buyerinfo

import com.example.domain.model.common.Status
import com.example.util.extension.clickToast
import com.example.util.extension.observe
import com.lonecrab.multistateview.MultiStateView
import kotlinx.android.synthetic.main.fr_buyer_info.*

interface BuyerInfoResponse {

  fun BuyerInfoFragment.setupSaveLiveData() {
    viewModel.saveLiveData.observe(this) {
      when (it?.status) {
        Status.LOADING -> {
          msvBuyerInfo.viewState = MultiStateView.ViewState.BLOCKING_PROGRESS
        }
        Status.SUCCESS -> {
          msvBuyerInfo.viewState = MultiStateView.ViewState.CONTENT
          parentFragmentRouter()?.exit()
        }
        else -> {
          msvBuyerInfo.viewState = MultiStateView.ViewState.ERROR

          clickToast("Error")
        }
      }
    }
  }
}