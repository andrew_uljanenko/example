package com.example.presentation.view.profile.skeletons

import androidx.recyclerview.widget.PagerSnapHelper
import com.example.R
import com.example.domain.model.banner.Banner
import com.example.presentation.common.carousel.RvCarouselAdapter
import com.example.presentation.common.widget.recyclerview.decorators.ReduceHorizontalSpaceItemDecoration
import com.example.presentation.view.profile.ProfileFragment
import com.example.util.extension.clickToast
import com.example.util.extension.initRecyclerViewIfNeeded
import com.ethanhua.skeleton.Skeleton
import com.ethanhua.skeleton.SkeletonScreen
import kotlinx.android.synthetic.main.lt_profile_banner.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper

interface ProfileBanner {

  var carouselAdapter: RvCarouselAdapter?
  var bannersCarouselSkeletonScreen: SkeletonScreen?

  fun ProfileFragment.setupCarousel() {
    if (carouselAdapter == null) {

      carouselAdapter = rvSuggestionsCarousel.initRecyclerViewIfNeeded {
        it.isNestedScrollingEnabled = false

        val carouselItemSpacingReduceSize = resources.getDimensionPixelSize(R.dimen.shadow_height) + (it.paddingStart + it.paddingEnd) / 2
        val reduceSizeDecorator = ReduceHorizontalSpaceItemDecoration(carouselItemSpacingReduceSize)
        it.addItemDecoration(reduceSizeDecorator)

        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(it)

        OverScrollDecoratorHelper.setUpOverScroll(it, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL)

        RvCarouselAdapter()
      }
    }

    carouselAdapter?.onBannerClickListener = {
      clickToast(it.link!!) //todo
    }

    if (bannersCarouselSkeletonScreen == null) {
      bannersCarouselSkeletonScreen = Skeleton
        .bind(rvSuggestionsCarousel)
        .adapter(carouselAdapter)
        .load(R.layout.lt_banner)
        .shimmerLayout(R.layout.lt_match_parent_shimmer)
        .shimmer(false)
        .color(R.color.shimmerColor)
        .count(1)
        .build()
    }
  }

  fun ProfileFragment.populateCarousel(banners: List<Banner>) {
    bannersCarouselSkeletonScreen?.hide()
    carouselAdapter?.banners = banners

    carouselScrollToMiddle()
    rvSuggestionsCarousel.startAutoScroll()
  }

  private fun ProfileFragment.carouselScrollToMiddle() {
    val centralPosition = (carouselAdapter?.itemCount ?: 2) / 2 - 1

    if (centralPosition != 0) {
      rvSuggestionsCarousel.scrollToPosition(centralPosition)
    }
  }
}