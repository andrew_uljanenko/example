package com.example.presentation.view.main

import android.os.Bundle
import androidx.core.os.bundleOf
import com.example.domain.model.category.Category
import com.example.presentation.common.cicerone.FragmentScreen
import com.example.presentation.view.buyerinfo.BuyerInfoFragment
import com.example.presentation.view.cart.CartFragment
import com.example.presentation.view.cart.model.CartProduct
import com.example.presentation.view.cart.order.OrderFragment
import com.example.presentation.view.cart.order.delivery.DeliveryFragment
import com.example.presentation.view.cart.order.locationsearch.LocationSearchFragment
import com.example.presentation.view.cart.order.payment.PaymentFragment
import com.example.presentation.view.cart.order.successcreation.SuccessOrderCreationFragment
import com.example.presentation.view.categories.CategoriesFragment
import com.example.presentation.view.favourite.FavouriteFragment
import com.example.presentation.view.home.HomeFragment
import com.example.presentation.view.info.InfoFragment
import com.example.presentation.view.info.contacts.ContactsFragment
import com.example.presentation.view.info.contacts.callback.CallbackFragment
import com.example.presentation.view.profile.ProfileFragment
import com.example.presentation.view.profile.changepassword.ChangePasswordFragment
import com.example.presentation.view.profile.childrenprofile.ChildrenProfileFragment
import com.example.presentation.view.profile.emailsettings.EmailSettingsFragment
import com.example.presentation.view.profile.loyaltyprogram.LoyaltyProgramFragment
import com.example.presentation.view.profile.myreviews.MyReviewsFragment
import com.example.presentation.view.profile.ordershistory.OrdersHistoryFragment
import com.example.presentation.view.profile.viewedproducts.ViewedProductsFragment
import com.example.presentation.view.search.SearchFragment
import com.example.presentation.view.subcategories.SubcategoriesFragment
import com.example.presentation.view.successaction.SuccessActionFragment
import com.example.presentation.view.successaction.SuccessActionMode

object MainScreens {

  class HomeScreen : FragmentScreen<HomeFragment>() {
    override fun getFragmentClass() = HomeFragment::class
  }

  class FavouriteScreen : FragmentScreen<FavouriteFragment>() {
    override fun getFragmentClass() = FavouriteFragment::class
  }

  class CartScreen : FragmentScreen<CartFragment>() {
    override fun getFragmentClass() = CartFragment::class
  }

  class OrderScreen(private val products: List<CartProduct>) : FragmentScreen<OrderFragment>() {
    override fun getFragmentClass() = OrderFragment::class

    override fun createBundle(): Bundle? {
      return bundleOf(
        ARG_PRODUCTS to products
      )
    }

    companion object {
      private const val ARG_PRODUCTS = "arg_order_products"

      fun readBundle(fragment: OrderFragment) {
        val args = fragment.arguments ?: return
        val viewModel = fragment.viewModel

        viewModel.products = args.getParcelableArrayList(ARG_PRODUCTS)!!
      }
    }
  }

  class BuyerInfoScreen : FragmentScreen<BuyerInfoFragment>() {
    override fun getFragmentClass() = BuyerInfoFragment::class
  }

  class DeliveryScreen : FragmentScreen<DeliveryFragment>() {
    override fun getFragmentClass() = DeliveryFragment::class
  }

  class PaymentScreen : FragmentScreen<PaymentFragment>() {
    override fun getFragmentClass() = PaymentFragment::class
  }

  class LocationSearchScreen : FragmentScreen<LocationSearchFragment>() {
    override fun getFragmentClass() = LocationSearchFragment::class
  }

  class SuccessOrderCreationScreen : FragmentScreen<SuccessOrderCreationFragment>() {
    override fun getFragmentClass() = SuccessOrderCreationFragment::class
  }

  class ProfileScreen : FragmentScreen<ProfileFragment>() {
    override fun getFragmentClass() = ProfileFragment::class
  }

  class ChangePasswordScreen : FragmentScreen<ChangePasswordFragment>() {
    override fun getFragmentClass() = ChangePasswordFragment::class
  }

  class LoyaltyProgramScreen : FragmentScreen<LoyaltyProgramFragment>() {
    override fun getFragmentClass() = LoyaltyProgramFragment::class
  }

  class OrdersHistoryScreen : FragmentScreen<OrdersHistoryFragment>() {
    override fun getFragmentClass() = OrdersHistoryFragment::class
  }

  class MyReviewsScreen : FragmentScreen<MyReviewsFragment>() {
    override fun getFragmentClass() = MyReviewsFragment::class
  }

  class ViewedProductsScreen : FragmentScreen<ViewedProductsFragment>() {
    override fun getFragmentClass() = ViewedProductsFragment::class
  }

  class EmailSettingsScreen : FragmentScreen<EmailSettingsFragment>() {
    override fun getFragmentClass() = EmailSettingsFragment::class
  }

  class InfoScreen : FragmentScreen<InfoFragment>() {
    override fun getFragmentClass() = InfoFragment::class
  }

  class ContactsScreen : FragmentScreen<ContactsFragment>() {
    override fun getFragmentClass() = ContactsFragment::class
  }

  class CallbackScreen : FragmentScreen<CallbackFragment>() {
    override fun getFragmentClass() = CallbackFragment::class
  }
}