package com.example.presentation.view.profile.myreviews

import com.example.domain.model.review.Review
import com.example.presentation.common.widget.hideProgressBar
import com.example.presentation.common.widget.showProgressBar
import com.example.presentation.view.profile.myreviews.adapter.RvMyReviewsAdapter
import com.example.util.extension.clickToast
import com.example.util.extension.onScrollEnd
import kotlinx.android.synthetic.main.fr_my_reviews.*
import kotlinx.android.synthetic.main.lt_toolbar_progress_bar.*

interface MyReviewsContent {

  var reviewsAdapter: RvMyReviewsAdapter?
  var firstPageLoaded: Boolean

  fun MyReviewsFragment.setupMyReviewsRecyclerView() {
    if (reviewsAdapter == null) {
      reviewsAdapter = RvMyReviewsAdapter()
    }
    rvMyReviews.adapter = reviewsAdapter
    rvMyReviews.onScrollEnd {
      if (!viewModel.reviewsLiveData.isLoading()) {
        viewModel.fetchReviews()
      }
    }

    reviewsAdapter?.onProductClickListener = { clickToast("click product") }
  }

  fun MyReviewsFragment.populateReviews(reviews: List<Review>) {
    reviewsAdapter?.let {
      it.data.addAll(reviews)
      it.notifyDataSetChanged()
    }
  }

  fun MyReviewsFragment.showProgress() {
    if (!firstPageLoaded) {
      pbContent.showProgressBar()
    } else {
      reviewsAdapter?.showProgress()
    }
  }

  fun MyReviewsFragment.hideProgress() {
    if (!firstPageLoaded) {
      firstPageLoaded = true
      pbContent.hideProgressBar()
      rvMyReviews.scheduleLayoutAnimation()
    } else {
      reviewsAdapter?.hideProgress()
    }
  }
}