package com.example.presentation.view.main.view.widget

import androidx.fragment.app.FragmentManager
import com.example.presentation.common.widget.bottomnavigationviewpager.BottomNavigationViewPagerAdapter
import com.example.presentation.view.cart.CartFragmentContainerFragment
import com.example.presentation.view.favourite.FavouriteFragmentContainerFragment
import com.example.presentation.view.home.HomeFragmentContainerFragment
import com.example.presentation.view.info.InfoFragmentContainerFragment
import com.example.presentation.view.profile.ProfileFragmentContainerFragment

class VpMainAdapter(fm: FragmentManager) : BottomNavigationViewPagerAdapter(fm) {

  override fun getItem(position: Int) = when (position) {
    HOME_ITEM_POSITION -> HomeFragmentContainerFragment()
    CART_ITEM_POSITION -> CartFragmentContainerFragment()
    FAVORITES_ITEM_POSITION -> FavouriteFragmentContainerFragment()
    PROFILE_ITEM_POSITION -> ProfileFragmentContainerFragment()
    INFO_ITEM_POSITION -> InfoFragmentContainerFragment()
    else -> throw IllegalArgumentException()
  }

  override fun getCount() = ITEMS_COUNT

  companion object {
    const val ITEMS_COUNT = 5

    const val HOME_ITEM_POSITION = 0
    const val CART_ITEM_POSITION = 1
    const val FAVORITES_ITEM_POSITION = 2
    const val PROFILE_ITEM_POSITION = 3
    const val INFO_ITEM_POSITION = 4
  }
}