package com.example.presentation.view.profile.emailsettings

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.HideBottomNavigation
import com.example.presentation.view.profile.emailsettings.adapter.RvSubscriptionsAdapter

class EmailSettingsFragment : BaseViewPagerViewModelChildFragment<EmailSettingsViewModel>(),
                              HideBottomNavigation,
                              EmailSettingsContent,
                              EmailSettingsDataHelper {
  override var subscriptionsAdapter: RvSubscriptionsAdapter? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setTitle(R.string.email_settings_title)
    setLayoutResId(R.layout.fr_email_settings)

    setupEmailSettingsLiveData()
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupEmailSettingsRecyclerView()

    viewModel.fetchSettings()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}