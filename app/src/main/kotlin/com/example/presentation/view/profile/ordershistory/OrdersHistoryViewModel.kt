package com.example.presentation.view.profile.ordershistory

import com.example.data.ApiService
import com.example.domain.model.common.Response
import com.example.domain.model.order.Order
import com.example.domain.model.order.OrderStatus
import com.example.domain.model.product.Product
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.model.ResponseLiveData
import com.example.util.extension.applySchedulers
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class OrdersHistoryViewModel(
    private val apiService: ApiService
) : BaseViewModel() {

  val ordersLiveData = ResponseLiveData<List<Order>>()

  var offset: Int = 0

  fun fetchOrders() {
    if (ordersLiveData.isLoading()) {
      return
    }

    ordersLiveData.value = Response.loading()

    Observable
      .timer(1, TimeUnit.SECONDS)
      .applySchedulers()
      .subscribe(
        { ordersLiveData.value = Response.success(mockData()) },
        { ordersLiveData.value = Response.error(it) }
      )
      .registerDisposable()
  }

  private fun mockData(): List<Order> {
    return null
  }

  private fun mockProducts(): List<Product> {
    return null
  }
}