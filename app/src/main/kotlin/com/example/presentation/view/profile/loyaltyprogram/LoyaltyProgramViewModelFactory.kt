package com.example.presentation.view.profile.loyaltyprogram

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class LoyaltyProgramViewModelFactory(
    private val apiService: ApiService,
    private val sharedPreferences: SharedPreferences
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(LoyaltyProgramViewModel::class.java)) {
      return LoyaltyProgramViewModel(apiService, sharedPreferences) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}