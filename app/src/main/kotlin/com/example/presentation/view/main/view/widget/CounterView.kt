package com.example.presentation.view.main.view.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.R
import com.example.util.extension.setDisabled
import com.example.util.extension.setEnabled

typealias OnCountChangeListener = (count: Int) -> Unit

class CounterView @JvmOverloads constructor(
    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

  private lateinit var btnMinus: ImageView
  private lateinit var btnPlus: ImageView
  private lateinit var tvCount: TextView

  var onCountChangeListener: OnCountChangeListener? = null

  var minCount: Int = 1

  init {
    initViews(context)
  }

  private fun initViews(context: Context?) {
    val view = View.inflate(context, R.layout.lt_counter_view, this)

    btnMinus = view.findViewById(R.id.btnMinus)
    btnPlus = view.findViewById(R.id.btnPlus)
    tvCount = view.findViewById(R.id.tvCount)

    btnMinus.setOnClickListener { onMinusClick() }
    btnPlus.setOnClickListener { onPlusClick() }
  }

  public fun getCount(): Int {
    val countString = tvCount.text.toString()
    return if (!countString.isEmpty()) countString.toInt() else 0
  }

  public fun setCount(count: Int) {
    tvCount.text = count.toString()
  }

  private fun onMinusClick() {
    val count = getCount() - 1

    if (count >= minCount) {
      tvCount.text = count.toString()
      onCountChangeListener?.invoke(count)
    }
    updateButtons(count)
  }

  private fun onPlusClick() {
    val count = getCount() + 1

    tvCount.text = count.toString()
    onCountChangeListener?.invoke(count)
    updateButtons(count)
  }

  private fun updateButtons(count: Int) {
    if (count == minCount) {
      btnMinus.setDisabled()
    } else {
      btnMinus.setEnabled()
    }
  }
}