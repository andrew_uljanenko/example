package com.example.presentation.view.main.manager

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.presentation.view.profile.loyaltyprogram.loyaltycard.LoyaltyCardDialogFragment
import com.example.util.ShakeDetector
import com.example.util.extension.showDialog
import java.lang.ref.WeakReference

class ShakeManager(private val activity: AppCompatActivity) : LifecycleObserver {

  private var sensorManager = activity.getSystemService(Context.SENSOR_SERVICE) as? SensorManager
  private var accelerometer = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
  private var shakeDetector = ShakeDetector()

  private var loyaltyCardDialogFragment: WeakReference<LoyaltyCardDialogFragment> = WeakReference(
    LoyaltyCardDialogFragment()
  )
    get() {
      if (field.get() == null) {
        field = WeakReference(LoyaltyCardDialogFragment())
      }

      return field
    }

  init {
    initListener()
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
  fun registerShakeDetector() {
    sensorManager?.registerListener(shakeDetector, accelerometer, SensorManager.SENSOR_DELAY_UI)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
  fun unregisterShakeDetector() {
    sensorManager?.unregisterListener(shakeDetector)
  }

  private fun initListener() {
    shakeDetector.onShakeListener = { showLoyaltyCardDialog() }
  }

  private fun showLoyaltyCardDialog() {
    //todo
    activity.showDialog(
      loyaltyCardDialogFragment.get(),
      LoyaltyCardDialogFragment.getScreenKey(),
      LoyaltyCardDialogFragment.createBundle("2100568521714")
    )
  }
}