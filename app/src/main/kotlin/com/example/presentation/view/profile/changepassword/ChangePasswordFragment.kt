package com.example.presentation.view.profile.changepassword

import android.os.Bundle
import android.view.MenuItem
import com.example.R
import com.example.presentation.view.main.HideBottomNavigation
import com.example.util.extension.hideKeyboard
import com.example.util.validator.FieldValidatorViewPagerViewModelChildFragment
import kotlinx.android.synthetic.main.fr_profile_change_password.*

class ChangePasswordFragment : FieldValidatorViewPagerViewModelChildFragment<ChangePasswordViewModel>(),
                               HideBottomNavigation,
                               ChangePasswordFieldValidator,
                               ChangePasswordResponse {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setLayoutResId(R.layout.fr_profile_change_password)
    setTitle(R.string.change_password_title)
    setMenuResId(R.menu.save)

    setupSaveLiveData()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    if (item?.itemId == R.id.action_save) {
      validateAndSave()
      return true
    }

    return super.onOptionsItemSelected(item)
  }

  private fun validateAndSave() {
    if (validateFields()) {
      hideKeyboard()
      viewModel.save(etOldPassword.text.toString(), etNewPassword.text.toString())
    }
  }

  override fun setupFieldValidators() {
    super.setupFieldValidators()

    registerFieldValidators()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}