package com.example.presentation.view.profile.viewedproducts

import android.os.Bundle
import android.view.View
import com.example.R
import com.example.presentation.common.fragment.BaseViewPagerViewModelChildFragment
import com.example.presentation.view.main.HideBottomNavigation
import com.example.presentation.view.profile.viewedproducts.adapter.RvViewedProductsAdapter

class ViewedProductsFragment : BaseViewPagerViewModelChildFragment<ViewedProductsViewModel>(),
                               HideBottomNavigation,
                               ViewedProductsContent,
                               ViewedProductsDataHelper {

  override var productsAdapter: RvViewedProductsAdapter? = null
  override var firstPageLoaded: Boolean = false

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setTitle(R.string.visited_products_title)
    setLayoutResId(R.layout.fr_viewed_products)

    setupReviewsLiveData()

    //todo add empty view
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    setupViewedProductsRecyclerView()

    viewModel.fetchProducts()
  }

  override fun onBackPressed(): Boolean {
    parentFragmentRouter()?.exit()
    return true
  }
}