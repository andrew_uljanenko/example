package com.example.presentation.view.main

import android.graphics.Color
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.R
import com.example.presentation.common.HasCloseButton
import com.example.presentation.common.fragment.BaseViewPagerContainerFragment
import com.example.presentation.common.listener.BackButtonListener
import com.example.presentation.view.main.view.widget.VpMainAdapter
import com.example.util.extension.getColorRes
import com.example.util.extension.openKeyboard
import com.example.util.extension.setGone
import com.example.util.extension.setVisible
import kotlinx.android.synthetic.main.ac_main.*
import kotlinx.android.synthetic.main.lt_toolbar.*

interface MainBackStackProcessor {

  fun MainActivity.adjustViewDependingOnCurrentFragment() {
    val currentBottomNavigationFragment = getCurrentBottomNavigationFragment() ?: return
    val currentFragment = getCurrentChildFragment() ?: return

    setupToolbarHomeIcons(currentBottomNavigationFragment, currentFragment)
    setupToolbar(currentFragment)
    setupBottomNavigation(currentFragment)
    setupSearchField(currentFragment)
    setupOptionsMenu(currentFragment)
  }

  fun MainActivity.adjustViewDependingOnKeyboardToggle(opened: Boolean) {
    if (opened) {
      bottomNavigation.setGone()
    } else {
      val currentFragment = getCurrentChildFragment() ?: return
      setupBottomNavigation(currentFragment)
    }
  }

  private fun MainActivity.setupToolbar(currentFragment: Fragment) {
    if (currentFragment is HasColorizedStatusBar) {
      window.statusBarColor = getColorRes(R.color.colorBrandPink)
    } else {
      window.statusBarColor = getColorRes(R.color.statusBarColor)
    }

    if (currentFragment is HideToolbar) {
      flToolbarHomeButtons.setGone()
      toolbar.setBackgroundColor(Color.TRANSPARENT)
      appBar.elevation = 0f
    } else {
      flToolbarHomeButtons.setVisible()
      toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
      appBar.elevation = resources.getDimension(R.dimen.toolbar_elevation)
    }
  }

  private fun MainActivity.setupOptionsMenu(currentFragment: Fragment) {
    val countViewPagerFragments = getViewPagerAdapter()?.fragments?.size() ?: return

    for (i in 0..countViewPagerFragments) {
      val fragment = getViewPagerAdapter()?.fragments?.valueAt(i)
      val childFragments = fragment?.childFragmentManager?.fragments ?: continue

      for (childFragment in childFragments) {
        if (childFragment != currentFragment) {
          childFragment.setHasOptionsMenu(false)
        }
      }
    }

    currentFragment.setHasOptionsMenu(true)
    invalidateOptionsMenu()
  }

  /**
   * navigate to Home Screen if main ViewPager item is not Home Screen, otherwise exit
   */
  private fun MainActivity.navigateToHomeOrExit() {
    if (vpContent.currentItem == VpMainAdapter.HOME_ITEM_POSITION) {
      val currentBottomNavigationFragment = getCurrentBottomNavigationFragment() as BaseViewPagerContainerFragment

      val wasScrolled = currentBottomNavigationFragment.scrollToTop()
      if (!wasScrolled) {
        finish()
      }

    } else {
      vpContent.setCurrentItem(VpMainAdapter.HOME_ITEM_POSITION, false)
      adjustViewDependingOnCurrentFragment()
    }
  }

  /**
   * Process back press
   */
  fun MainActivity.processBackPress() {
    val currentBottomNavigationFragment = getCurrentBottomNavigationFragment()
    val currentChildFragment = getCurrentChildFragment()
    val childFragmentBackStackEntryCount = currentBottomNavigationFragment?.childFragmentManager?.backStackEntryCount ?: 0

    if (childFragmentBackStackEntryCount > 1) {
      if (currentChildFragment !is BackButtonListener || !currentChildFragment.onBackPressed()) {
        navigateToHomeOrExit()
      }
    } else {
      navigateToHomeOrExit()
    }
  }

  /**
   * Toolbar home icons
   */
  private fun MainActivity.setupToolbarHomeIcons(
      currentBottomNavigationFragment: Fragment,
      currentFragment: Fragment
  ) {
    if (currentFragment is HasCloseButton) {
      ivCloseButton.setVisible()
      ivBackArrow.setGone()
      ivLogo.setGone()
      return
    } else {
      ivCloseButton.setGone()
    }

    if (currentBottomNavigationFragment.childFragmentManager.backStackEntryCount > 1) {
      ivCloseButton.setGone()
      ivBackArrow.setVisible()
      ivLogo.setGone()
      return
    } else {
      ivBackArrow.setGone()
    }

    if (currentFragment is HasToolbarAppLogo) {
      ivCloseButton.setGone()
      ivBackArrow.setGone()
      ivLogo.setVisible()
    } else {
      ivLogo.setGone()
    }
  }

  /**
   * Bottom navigation
   */
  private fun MainActivity.setupBottomNavigation(currentFragment: Fragment) {
    if (currentFragment is HideBottomNavigation) {
      bottomNavigation.setGone()
    } else {
      bottomNavigation.setVisible()
    }
  }

  /**
   * Search field
   */
  private fun MainActivity.setupSearchField(currentFragment: Fragment) {
    if (currentFragment is HasSearchField) {
      etSearch.setVisible()
      tvToolbarTitle.setGone()
    } else {
      etSearch.setGone()
      tvToolbarTitle.setVisible()
    }

    if (currentFragment is HasExpandedSearchField) {
      etSearch.isInSearchMode = true

      etSearch.openKeyboard()
    } else {
      etSearch.isInSearchMode = false

      (etSearch.parent as ViewGroup).requestFocus()
    }
  }
}