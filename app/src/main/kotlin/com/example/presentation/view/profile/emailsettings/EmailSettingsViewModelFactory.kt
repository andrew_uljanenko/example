package com.example.presentation.view.profile.emailsettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.data.ApiService

@Suppress("UNCHECKED_CAST")
class EmailSettingsViewModelFactory(
    private val apiService: ApiService
) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(EmailSettingsViewModel::class.java)) {
      return EmailSettingsViewModel(apiService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}