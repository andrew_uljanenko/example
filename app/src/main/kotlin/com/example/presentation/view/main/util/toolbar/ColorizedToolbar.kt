package com.example.presentation.view.main.util.toolbar

interface ColorizedToolbar {
  fun getToolbarBgColorRes(): Int
  fun getToolbarTitleColor(): Int
  fun getToolbarIconsColor(): Int
  fun isStatusBarLight(): Boolean
}