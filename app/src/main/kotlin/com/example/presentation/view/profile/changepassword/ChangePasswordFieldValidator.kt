package com.example.presentation.view.profile.changepassword

import com.example.R
import com.example.util.validator.FieldWrapper
import com.example.util.validator.utils.TextInputLayoutValidator
import com.example.util.validator.validators.EqualityValidator
import com.example.util.validator.validators.RequiredValidator
import kotlinx.android.synthetic.main.fr_profile_change_password.*

interface ChangePasswordFieldValidator {

  fun ChangePasswordFragment.registerFieldValidators() {
    TextInputLayoutValidator
      .forView(tilOldPassword)
      .addValidator(RequiredValidator(getString(R.string.empty_password_error)))
      .build()
      .registerValidator()

    TextInputLayoutValidator
      .forView(tilNewPassword)
      .addValidator(RequiredValidator(getString(R.string.empty_password_error)))
      .build()
      .registerValidator()

    TextInputLayoutValidator
      .forView(tilRepeatNewPassword)
      .addValidator(
        EqualityValidator(
          getString(R.string.passwords_do_not_match_error),
          FieldWrapper(tilNewPassword, etNewPassword)
        )
      )
      .build()
      .registerValidator()
  }
}