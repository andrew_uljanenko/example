package com.example.domain.model.brand

data class Brand(
    val id: Int,
    val name: String?,
    val description: String?,
    val image: String?,
    val text: String?
)