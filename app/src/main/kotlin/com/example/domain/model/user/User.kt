package com.example.domain.model.user

data class User(
    val name: String,
    val address: String,
    val city: String,
    val email: String,
    val loyaltyDCardId: String,
    val patronymic: String,
    val phone: String,
    val surname: String
)