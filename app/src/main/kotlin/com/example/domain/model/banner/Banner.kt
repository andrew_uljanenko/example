package com.example.domain.model.banner

data class Banner(
    val id: Int,
    val bannerType: String?,
    val bannerReferenceId: Int?,
    val bannerTypeId: Int?,
    val imageSrc: String?,
    val link: String?,
    val nameButton: String?
)