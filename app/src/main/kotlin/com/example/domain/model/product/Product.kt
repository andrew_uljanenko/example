package com.example.domain.model.product

data class Product(
    val id: Long,
    val title: String,
    val code: Int?,
    val imageSrc: String,
    val commentsCount: Int,
    val oldPrice: Int?,
    val price: Int,
    val rating: Int,
    val isInFavorites: Boolean,
    var isSelected: Boolean = false
)