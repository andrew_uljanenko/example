package com.example.domain.subscription

data class Subscription(
    val id: Long,
    val code: String,
    val description: String,
    val isSubscribed: Boolean
)