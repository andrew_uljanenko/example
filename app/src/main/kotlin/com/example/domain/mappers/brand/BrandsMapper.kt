package com.example.domain.mappers.brand

import com.example.data.model.brands.BrandEntity
import com.example.domain.model.brand.Brand
import io.reactivex.functions.Function

class BrandsMapper : Function<BrandEntity, Brand> {

  override fun apply(t: BrandEntity): Brand {
    return Brand(
      t.id,
      t.name,
      t.description,
      t.image,
      t.text
    )
  }
}