package com.example.domain.mappers.banner

import com.example.data.model.banners.BannerEntity
import com.example.domain.model.banner.Banner
import io.reactivex.functions.Function

class BannerMapper : Function<BannerEntity, Banner> {

  override fun apply(t: BannerEntity): Banner {
    return Banner(
      t.id,
      t.bannerType,
      t.bannerReferenceId,
      t.bannerTypeId,
      t.imageSrc,
      t.link,
      t.nameButton
    )
  }
}