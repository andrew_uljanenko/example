package com.example.domain.managers.banner

import com.example.data.ApiService
import com.example.data.model.banners.BannersResponse
import com.example.domain.mappers.banner.BannerMapper
import com.example.domain.model.banner.Banner
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BannersManager @Inject constructor(private val apiService: ApiService) {

  fun get(): Single<List<Banner>> {
    return StoreBuilder.key<Int, BannersResponse>()
      .fetcher { apiService.getBanners() }
      .networkBeforeStale()
      .open()
      .get(1)
      .toObservable()
      .flatMapIterable { it.data }
      .map { BannerMapper().apply(it) }
      .toList()
  }
}