package com.example.domain.managers.brand

import com.example.data.ApiService
import com.example.data.model.brands.BrandsResponse
import com.example.domain.mappers.brand.BrandsMapper
import com.example.domain.model.brand.Brand
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BrandsManager @Inject constructor(private val apiService: ApiService) {

  fun get(): Single<List<Brand>> {
    return StoreBuilder.key<Int, BrandsResponse>()
      .fetcher { apiService.getBrands() }
      .networkBeforeStale()
      .open()
      .get(1)
      .toObservable()
      .flatMapIterable { it.data }
      .map { BrandsMapper().apply(it) }
      .toList()
  }
}