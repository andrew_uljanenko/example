package com.example.util.validator.listeners

interface OnHideErrorListener {
  fun onHideError()
}