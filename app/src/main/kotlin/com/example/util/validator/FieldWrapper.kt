package com.example.util.validator

import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout

class FieldWrapper(val inputLayout: TextInputLayout, val editText: EditText)