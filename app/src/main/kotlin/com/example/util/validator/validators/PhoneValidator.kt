package com.example.util.validator.validators

import java.io.Serializable

class PhoneValidator(
    errorMessage: String
) : StringValidator(errorMessage), Serializable {

  override fun validate(input: String): Boolean {
    return LengthValidator(errorMessage, MIN_LENGTH, MAX_LENGTH).validate(input)
  }

  companion object {
    const val MIN_LENGTH = 10
    const val MAX_LENGTH = 13
  }
}