package com.example.util.validator.exceptions;

public class ValidationException extends Exception {

    private final String mErrorMessage;

    public ValidationException(String errorMessage, String errorCause) {
        super(errorCause);
        mErrorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}