package com.example.util.validator.listeners

import com.example.util.validator.exceptions.ValidationException

interface OnErrorListener {
  fun onShowError(t: ValidationException)
}