package com.example.util.validator.validators

import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class LengthValidator(
    errorMessage: String,
    private val minLength: Int,
    private val maxLength: Int? = null
) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    if (input.length < minLength || (maxLength != null && input.length > this.maxLength)) {
      throw InvalidLengthException(errorMessage)
    }
    return true
  }

  class InvalidLengthException(errorMessage: String) : ValidationException(
    errorMessage,
    "Too short string"
  )
}