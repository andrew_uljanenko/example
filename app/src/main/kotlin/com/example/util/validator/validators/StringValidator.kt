package com.example.util.validator.validators

import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

abstract class StringValidator(val errorMessage: String) : Serializable {
  var nextValidator: StringValidator? = null
    private set

  @Throws(ValidationException::class)
  protected fun throwNewValidationException(message: String) {
    throw ValidationException(errorMessage, message)
  }

  @Throws(ValidationException::class)
  abstract fun validate(input: String): Boolean

  fun addNextValidator(nextValidator: StringValidator) {
    this.nextValidator = nextValidator
  }
}