@file:Suppress("unused")

package com.example.util.validator

import com.example.util.validator.exceptions.ValidationException
import com.example.util.validator.listeners.OnErrorListener
import com.example.util.validator.listeners.OnFirstErrorListener
import com.example.util.validator.listeners.OnHideErrorListener
import com.example.util.validator.validators.StringValidator

class FieldValidator private constructor() {

  private var mValidator: StringValidator? = null

  private var mValidateStringSupplier: (() -> String)? = null
  private var mFieldIdSupplier: (() -> Int)? = null
  private var mOnFirstErrorListener: OnFirstErrorListener? = null
  private var mOnErrorListener: OnErrorListener? = null
  private var mOnHideErrorListener: OnHideErrorListener? = null

  val fieldId: Int?
    get() = mFieldIdSupplier?.invoke()

  /**
   * This method starts field validation chain
   *
   * @return `false` if some of field validators test has not been passed
   */
  fun validate(): Boolean {
    mValidator ?: return true

    val validationString = mValidateStringSupplier!!.invoke()

    var currentValidator = mValidator
    while (currentValidator != null) {
      try {
        currentValidator.validate(validationString)
        currentValidator = currentValidator.nextValidator
      } catch (exception: ValidationException) {
        mOnErrorListener?.onShowError(exception)
        return false
      }

    }

    mOnHideErrorListener?.onHideError()
    return true
  }

  /**
   * @return `true` if [OnFirstErrorListener] is present
   */
  internal fun triggerOnFirstErrorListenerIfPresent(): Boolean {
    mOnFirstErrorListener?.onScroll()
    return mOnFirstErrorListener != null
  }

  /**
   * This method trigger [OnHideErrorListener] if it has been set
   */
  fun hideError() {
    mOnHideErrorListener?.onHideError()
  }

  class Builder private constructor() {
    private var mValidatorHead: StringValidator? = null
    private var mValidatorTail: StringValidator? = null

    private var mValidateStringSupplier: (() -> String)? = null
    private var mFieldIdSupplier: (() -> Int)? = null
    private var mOnFirstErrorListener: OnFirstErrorListener? = null
    private var mOnErrorListener: OnErrorListener? = null
    private var mOnHideErrorListener: OnHideErrorListener? = null

    /**
     * Add new validator to the end of validators chain
     *
     * @param addValidator    flag that can be used bu UI factory that defines whether validator will
     * be added to the validators chain or not
     * @param stringValidator with the specific rule to be processed. Value can be null, but note
     * that at least one validator should be present in the validators chain
     */
    fun addValidator(stringValidator: StringValidator?, addValidator: Boolean = true): Builder {
      if (stringValidator == null || !addValidator) {
        return this
      }

      if (mValidatorHead == null) {
        mValidatorHead = stringValidator
        mValidatorTail = stringValidator
      } else {
        mValidatorTail!!.addNextValidator(stringValidator)
        mValidatorTail = stringValidator
      }

      return this
    }

    /**
     * Method that takes a supplier that defines a rule of how to return an actual string to be
     * validated.
     *
     * @param validateStringSupplier supplier that returns string that has to be validated
     */
    fun stringToCheck(validateStringSupplier: () -> String): Builder {
      mValidateStringSupplier = validateStringSupplier
      return this
    }

    /**
     * Method that takes a supplier that defines a rule of how to return an field id
     */
    fun fieldId(fieldIdSupplier: () -> Int): Builder {
      mFieldIdSupplier = fieldIdSupplier
      return this
    }

    /**
     * [OnFirstErrorListener] will be triggered by [ValidateProcessor.validate]
     * when the first validator from the validators set has been failed.
     *
     * @param onFirstErrorListener listener that notifies when the first validator from the
     * validators set has been failed
     */
    fun onFirstErrorInErrorSet(onFirstErrorListener: () -> Unit): Builder {
      mOnFirstErrorListener = object : OnFirstErrorListener {
        override fun onScroll() {
          onFirstErrorListener.invoke()
        }
      }
      return this
    }

    /**
     * [OnErrorListener] will be triggered when some validator has been failed.
     *
     * @param onErrorListener listener that notifies when some validator of the field
     * validation chain has been failed
     */
    fun onError(onErrorListener: (ValidationException) -> Unit): Builder {
      mOnErrorListener = object : OnErrorListener {
        override fun onShowError(t: ValidationException) {
          onErrorListener.invoke(t)
        }
      }
      return this
    }

    /**
     * [OnHideErrorListener] will be triggered when all validators tests in field validation
     * chain has been passed successfully and error message can be hided if it is present
     *
     * @param onHideErrorListener listener that notifies when all validators tests in field
     * validation chain has been passed successfully
     */
    fun onHideError(onHideErrorListener: () -> Unit): Builder {
      mOnHideErrorListener = object : OnHideErrorListener {
        override fun onHideError() {
          onHideErrorListener.invoke()
        }
      }
      return this
    }

    fun build(): FieldValidator {
      mValidatorHead ?: throw NullPointerException("One validator must be added at least!!!")

      val fieldValidator = FieldValidator()

      fieldValidator.mValidator = mValidatorHead
      fieldValidator.mValidateStringSupplier = mValidateStringSupplier
      fieldValidator.mFieldIdSupplier = mFieldIdSupplier
      fieldValidator.mOnFirstErrorListener = mOnFirstErrorListener
      fieldValidator.mOnErrorListener = mOnErrorListener
      fieldValidator.mOnHideErrorListener = mOnHideErrorListener

      return fieldValidator
    }

    companion object {
      fun builder() = Builder()
    }
  }
}