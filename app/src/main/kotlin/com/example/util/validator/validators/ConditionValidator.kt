package com.example.util.validator.validators

import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class ConditionValidator(
    errorMessage: String,
    private val supplier: (String) -> Boolean
) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    if (supplier.invoke(input)) {
      throw ConditionException(errorMessage)
    }
    return true
  }

  class ConditionException(errorMessage: String) : ValidationException(
    errorMessage,
    "Input does not match condition"
  )
}