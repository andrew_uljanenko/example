package com.example.util.validator

import com.example.util.validator.listeners.OnFirstErrorListener

object ValidateProcessor {

  /**
   * Process set of validators by calling on each [FieldValidator.validate] method. Also
   * will trigger [OnFirstErrorListener] when the first validator from the validators set has
   * been failed.
   *
   * @param validators set of validators
   * @return `false` if at least one validator has been failed
   */
  fun validate(validators: Map<Int, FieldValidator>): Boolean {
    var result = true
    var firstError = false

    for (entry in validators.entries) {
      val validator = entry.value

      result = result and validator.validate()
      if (!result && !firstError && validator.triggerOnFirstErrorListenerIfPresent()) {
        firstError = true
      }
    }

    return result
  }
}