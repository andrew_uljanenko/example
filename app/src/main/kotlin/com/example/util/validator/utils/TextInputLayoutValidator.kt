package com.example.util.validator.utils

import com.example.util.validator.FieldValidator
import com.google.android.material.textfield.TextInputLayout

object TextInputLayoutValidator {

  fun forView(textInputLayout: TextInputLayout): FieldValidator.Builder {
    return FieldValidator.Builder
      .builder()
      .stringToCheck { textInputLayout.editText?.text.toString() }
      .fieldId { textInputLayout.editText!!.id }
      .onHideError { textInputLayout.isErrorEnabled = false }
      .onFirstErrorInErrorSet { textInputLayout.editText!!.requestFocus() }
      .onError {
        textInputLayout.isErrorEnabled = true
        textInputLayout.error = it.errorMessage
      }
  }
}