package com.example.util.validator.validators

import android.text.TextUtils
import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class RequiredValidator(errorMessage: String) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    if (TextUtils.isEmpty(input)) {
      throw RequiredException(errorMessage)
    }
    return true
  }

  class RequiredException(errorMessage: String) : ValidationException(
    errorMessage,
    "Field is required"
  )
}