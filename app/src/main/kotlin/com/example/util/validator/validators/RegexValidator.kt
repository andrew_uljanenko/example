package com.example.util.validator.validators

import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable
import java.util.regex.Pattern

class RegexValidator(
    errorMessage: String,
    private val mRegex: String,
    private val shouldMatch: Boolean = false
) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    val isPatternMatches = Pattern.compile(mRegex)
      .matcher(input)
      .matches()

    if ((shouldMatch && !isPatternMatches) || (!shouldMatch && isPatternMatches)) {
      throwNewValidationException("Field does not matches the pattern")
    }

    return true
  }
}