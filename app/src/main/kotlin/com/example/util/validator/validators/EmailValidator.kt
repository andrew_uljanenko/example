package com.example.util.validator.validators

import android.text.TextUtils
import android.util.Patterns
import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class EmailValidator(errorMessage: String) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    if (TextUtils.isEmpty(input) || !Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
      throwNewValidationException("Invalid email")
    }
    return true
  }
}