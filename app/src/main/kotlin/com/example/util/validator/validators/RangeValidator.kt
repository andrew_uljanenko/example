package com.example.util.validator.validators

import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class RangeValidator(
    errorMessage: String,
    private val min: Double? = null,
    private val max: Double? = null
) : StringValidator(errorMessage), Serializable {

  constructor(
      errorMessage: String,
      min: Int? = null,
      max: Int? = null
  ) : this(errorMessage, min?.toDouble(), max?.toDouble())

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    val doubleInput = input.toDoubleOrNull() ?: throw InvalidRangeException(errorMessage)

    if ((min != null && min > doubleInput) || (max != null && max < doubleInput)) {
      throw InvalidRangeException(errorMessage)
    }

    return true
  }

  class InvalidRangeException(errorMessage: String) : ValidationException(
    errorMessage,
    "Invalid range"
  )
}