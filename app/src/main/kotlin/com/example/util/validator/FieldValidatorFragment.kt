package com.example.util.validator

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.annotation.CallSuper
import com.example.presentation.common.BaseViewModel
import com.example.presentation.common.fragment.BaseViewModelFragment
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.TextViewAfterTextChangeEvent
import io.reactivex.disposables.CompositeDisposable

abstract class FieldValidatorFragment<T : BaseViewModel> : BaseViewModelFragment<T>() {

  private lateinit var fieldsCompositeDisposable: CompositeDisposable
  var fieldValidators: MutableMap<Int, FieldValidator> = LinkedHashMap()

  protected open fun getValidationFields(): Array<EditText>? = null

  fun FieldValidator.registerValidator() {
    fieldId ?: throw IllegalArgumentException("Implement getId method in builder")
    fieldValidators[fieldId!!] = this
  }

  @CallSuper
  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setupFieldValidators()
  }

  @CallSuper
  protected open fun setupFieldValidators() {
    clearFieldValidators()
  }

  protected fun clearFieldValidators() {
    if (fieldValidators.isNotEmpty()) {
      fieldValidators.clear()
    }
  }

  /**
   * Process text changes with field validator
   */
  private fun processTextChanges(event: TextViewAfterTextChangeEvent) {
    val view = event.view()
    val fieldValidator = fieldValidators[view.id]
    fieldValidator?.hideError()
  }

  /**
   * Process focus changes with field validator
   */
  private fun onEditTextFocusChange(view: View, hasFocus: Boolean) {
    if (!hasFocus) {
      val fieldValidator = fieldValidators[view.id]
      fieldValidator?.validate()
    }
  }

  private fun initOnFocusChangeListeners() {
    val validationFields = getValidationFields() ?: return

    for (it in validationFields) {
      it.setOnFocusChangeListener(::onEditTextFocusChange)
    }
  }

  private fun clearOnFocusChangeListeners() {
    val validationFields = getValidationFields() ?: return

    for (it in validationFields) {
      it.onFocusChangeListener = null
    }
  }

  private fun initOnTextChangeEventsListeners() {
    val validationFields = getValidationFields() ?: return

    disposeFieldValidators()
    fieldsCompositeDisposable = CompositeDisposable()

    for (it in validationFields) {
      fieldsCompositeDisposable.add(
        RxTextView
          .afterTextChangeEvents(it)
          .skipInitialValue()
          .subscribe(::processTextChanges)
      )
    }
  }

  protected fun validateFields(): Boolean {
    return ValidateProcessor.validate(fieldValidators)
  }

  override fun onResume() {
    super.onResume()
    initOnTextChangeEventsListeners()
    initOnFocusChangeListeners()
  }

  override fun onPause() {
    super.onPause()
    disposeFieldValidators()
    clearOnFocusChangeListeners()
  }

  private fun disposeFieldValidators() {
    if (::fieldsCompositeDisposable.isInitialized && !fieldsCompositeDisposable.isDisposed) {
      fieldsCompositeDisposable.dispose()
    }
  }
}