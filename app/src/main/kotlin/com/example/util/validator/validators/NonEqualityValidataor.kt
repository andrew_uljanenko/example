package com.example.util.validator.validators

import com.example.util.validator.FieldWrapper
import com.example.util.validator.exceptions.ValidationException
import java.io.Serializable

class NonEqualityValidataor(
    errorMessage: String,
    private val mFieldWrapper: FieldWrapper
) : StringValidator(errorMessage), Serializable {

  @Throws(ValidationException::class)
  override fun validate(input: String): Boolean {
    if (input == mFieldWrapper.editText.text.toString()) {
      throw EqualityValidator.RequiredException(errorMessage)
    }
    return true
  }

  class RequiredException(errorMessage: String) : ValidationException(
    errorMessage,
    "Field is required"
  )
}
