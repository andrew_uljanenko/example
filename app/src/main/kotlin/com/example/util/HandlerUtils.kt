package com.example.util

import android.os.Handler
import java.util.concurrent.TimeUnit

fun handlerPost(f: () -> Unit) {
  Handler().post(f::invoke)
}

fun handlerPostDelayed(time: Long, timeUnit: TimeUnit = TimeUnit.MILLISECONDS, f: () -> Unit) {
  Handler().postDelayed({ f.invoke() }, timeUnit.toMillis(time))
}