package com.example.util

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

typealias OnShakeListener = () -> Unit

class ShakeDetector : SensorEventListener {

  var onShakeListener: OnShakeListener? = null

  private var shakeTimestamp: Long = 0
  private var shakeCount: Int = 0

  override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
  }

  override fun onSensorChanged(event: SensorEvent) {
    if (onShakeListener != null) {
      val x = event.values[0]
      val y = event.values[1]
      val z = event.values[2]

      val gX = x / SensorManager.GRAVITY_EARTH
      val gY = y / SensorManager.GRAVITY_EARTH
      val gZ = z / SensorManager.GRAVITY_EARTH

      val gForce = Math.sqrt((gX * gX + gY * gY + gZ * gZ).toDouble())

      if (gForce > SHAKE_THRESHOLD_GRAVITY) {
        val now = System.currentTimeMillis()

        if (shakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
          return
        }

        if (shakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
          shakeCount = 0
        }

        shakeTimestamp = now
        shakeCount++

        if (shakeCount >= MIN_SHAKE_COUNT)
          onShakeListener?.invoke()
      }
    }
  }

  companion object {
    private const val SHAKE_THRESHOLD_GRAVITY = 2.7f
    private const val SHAKE_SLOP_TIME_MS = 500
    private const val SHAKE_COUNT_RESET_TIME_MS = 3000
    private const val MIN_SHAKE_COUNT = 2
  }
}