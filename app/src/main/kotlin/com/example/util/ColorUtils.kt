package com.example.util


import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import android.view.View
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import com.example.R

@ColorInt
fun Context.getColorAccent(): Int {
  return getColorByAttr(R.attr.colorAccent)
}

@ColorInt
fun Context.getTextColorPrimary(): Int {
  return getColorByAttr(android.R.attr.textColorPrimary)
}

@ColorInt
fun Context.getColorByAttr(@AttrRes attrResId: Int): Int {
  val typedValue = TypedValue()

  val a = obtainStyledAttributes(typedValue.data, intArrayOf(attrResId))
  val color = a.getColor(0, 0)

  a.recycle()

  return color
}

@ColorInt
fun Int.makeColorDarker(factor: Float): Int {
  val a = Color.alpha(this)
  val r = Color.red(this)
  val g = Color.green(this)
  val b = Color.blue(this)

  return Color.argb(
    a,
    Math.max((r * factor).toInt(), 0),
    Math.max((g * factor).toInt(), 0),
    Math.max((b * factor).toInt(), 0)
  )
}

@ColorInt
fun Int.makeColorLighter(factor: Float): Int {
  val a = Color.alpha(this)
  val r = Color.red(this)
  val g = Color.green(this)
  val b = Color.blue(this)

  return Color.argb(
    a,
    Math.min((r + r * factor).toInt(), 255),
    Math.min((g + g * factor).toInt(), 255),
    Math.min((b + b * factor).toInt(), 255)
  )
}

@ColorInt
fun Int.adjustAlpha(factor: Float): Int {
  val a = Color.alpha(this)
  val r = Color.red(this)
  val g = Color.green(this)
  val b = Color.blue(this)

  return Color.argb(Math.round(a * factor), r, g, b)
}

@ColorInt
fun View.viewBackGroundColor(): Int {
  return (background as ColorDrawable).color
}