package com.example.util.extension

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner

fun LifecycleObserver.attachToLifecycle(lifecycleOwner: LifecycleOwner) {
  lifecycleOwner.lifecycle.addObserver(this)
}

fun LifecycleObserver.detachFromLifecycle(lifecycleOwner: LifecycleOwner) {
  lifecycleOwner.lifecycle.removeObserver(this)
}