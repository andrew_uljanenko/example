package com.example.util.extension

import android.content.Context
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.annotation.FloatRange
import androidx.annotation.LayoutRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.transition.AutoTransition
import androidx.transition.Transition
import androidx.transition.TransitionManager

fun View.rotate(
    @FloatRange(from = 0.0, to = 180.0) fromDegrees: Float,
    @FloatRange(from = 0.0, to = 180.0) toDegrees: Float
) {
  RotateAnimation(
      fromDegrees,
      toDegrees,
      Animation.RELATIVE_TO_SELF,
      0.5f,
      Animation.RELATIVE_TO_SELF,
      0.5f
  ).apply {
    duration = 200
    interpolator = AccelerateDecelerateInterpolator()
    fillAfter = true

    startAnimation(this)
  }
}

fun ConstraintLayout.applyTransitionFrom(
    context: Context?,
    @LayoutRes idRes: Int,
    transition: Transition = AutoTransition()
) {
  val constraintSet1 = ConstraintSet()
  constraintSet1.clone(this)

  val constraintSet2 = ConstraintSet()
  constraintSet2.clone(context, idRes)

  TransitionManager.beginDelayedTransition(this, transition)
  constraintSet2.applyTo(this)
}