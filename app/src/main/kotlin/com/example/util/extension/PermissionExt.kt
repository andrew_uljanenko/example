package com.example.util.extension

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.R
import pub.devrel.easypermissions.EasyPermissions

fun Context.hasCameraPermission(): Boolean {
  return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)
}

fun Fragment.showPermissionIsDisabledDialog(message: String) {
  activity?.let {
    val dialog = AlertDialog.Builder(it)
      .setMessage(message)
      .setNegativeButton(getString(android.R.string.ok)) { dialogInterface, _ -> dialogInterface.dismiss() }
      .setPositiveButton(getString(R.string.settings_button)) { _, _ ->

        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
          .setData(Uri.fromParts("package", it.packageName, null))
          .also { intent -> startActivityForResult(intent, RC_APP_SETTINGS) }
      }
      .create()

    if (isVisible) {
      dialog.show()
    }
  }
}

fun Activity.showPermissionIsDisabledDialog(message: String) {
  val dialog = AlertDialog.Builder(this)
    .setMessage(message)
    .setNegativeButton(getString(android.R.string.ok)) { dialogInterface, _ ->
      run {
        dialogInterface.dismiss()
        this.finish()
      }
    }
    .setPositiveButton(getString(R.string.settings_button)) { _, _ ->

      Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        .setData(Uri.fromParts("package", packageName, null))
        .also { intent -> startActivityForResult(intent, RC_APP_SETTINGS) }
    }
    .create()

  if (!isFinishing) {
    dialog.show()
  }
}

const val RC_APP_SETTINGS = 3030