package com.example.util.extension

fun Float.round(precision: Int = 1): Float {
  val scale = Math.pow(10.0, precision.toDouble()).toFloat()
  return Math.round(this * scale) / scale
}

fun Double.round(precision: Int = 1): Double {
  val scale = Math.pow(10.0, precision.toDouble())
  return Math.round(this * scale) / scale
}

fun ClosedRange<Int>.random() = (Math.random() * (endInclusive - start) + start).toInt()

fun Double.toString(digitsAfterPoint: Int) = "%.${digitsAfterPoint}f".format(this)