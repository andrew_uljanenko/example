package com.example.util.extension

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import com.example.BuildConfig

fun ViewGroup.inflate(
    inflater: LayoutInflater,
    @LayoutRes idRes: Int,
    attachToRoot: Boolean = false
): View = inflater.inflate(idRes, this, attachToRoot)

fun ViewGroup.inflate(
    @LayoutRes idRes: Int,
    attachToRoot: Boolean = false
) = inflate(LayoutInflater.from(context), idRes, attachToRoot)

fun View.isVisible() = visibility == View.VISIBLE
fun View.isGone() = visibility == View.GONE
fun View.isInvisible() = visibility == View.INVISIBLE

fun View.setVisible() {
  visibility = View.VISIBLE
}

fun View.setGone() {
  visibility = View.GONE
}

fun View.setInvisible() {
  visibility = View.INVISIBLE
}

fun View.setDisabled() {
  isEnabled = false
}

fun View.setEnabled() {
  isEnabled = true
}

fun View.setVisibleElseGone(f: () -> Boolean) {
  if (f.invoke()) setVisible() else setGone()
}

fun setGroupVisible(vararg views: View) = views.forEach { it.setVisible() }
fun setGroupGone(vararg views: View) = views.forEach { it.setGone() }
fun setGroupInvisible(vararg views: View) = views.forEach { it.setInvisible() }
/**
 * only in DEBUG build flavor set [View.OnLongClickListener] which initialize default values for
 * [TextView]'s
 */
fun View.initDebugValuesByLongClick(vararg pairs: Pair<TextView, String>, f: (() -> Unit)? = null) {
  if (BuildConfig.DEBUG) {
    setOnLongClickListener {
      f?.invoke()
      for (pair in pairs) pair.first.text = pair.second
      true
    }
  }
}

fun TextView.setDrawableLeft(drawable: Drawable?) = setCompoundDrawablesWithIntrinsicBounds(drawable, compoundDrawables[1], compoundDrawables[2], compoundDrawables[3])
fun TextView.setDrawableTop(drawable: Drawable?) = setCompoundDrawablesWithIntrinsicBounds(compoundDrawables[0], drawable, compoundDrawables[2], compoundDrawables[3])
fun TextView.setDrawableRight(drawable: Drawable?) = setCompoundDrawablesWithIntrinsicBounds(compoundDrawables[0], compoundDrawables[1], drawable, compoundDrawables[3])
fun TextView.setDrawableBottom(drawable: Drawable?) = setCompoundDrawablesWithIntrinsicBounds(compoundDrawables[0], compoundDrawables[1], compoundDrawables[2], drawable)

fun TextView.setDrawableLeft(@DrawableRes drawableRes: Int) = setDrawableLeft(ContextCompat.getDrawable(context, drawableRes))
fun TextView.setDrawableTop(@DrawableRes drawableRes: Int) = setDrawableTop(ContextCompat.getDrawable(context, drawableRes))
fun TextView.setDrawableRight(@DrawableRes drawableRes: Int) = setDrawableRight(ContextCompat.getDrawable(context, drawableRes))
fun TextView.setDrawableBottom(@DrawableRes drawableRes: Int) = setDrawableBottom(ContextCompat.getDrawable(context, drawableRes))

fun Array<out View>.setOnClickListener(l: (view: View) -> Unit) = forEach { it.setOnClickListener(l::invoke) }

inline fun <T: View> T.afterMeasured(crossinline f: T.() -> Unit) {
  viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
    override fun onGlobalLayout() {
      if (measuredWidth > 0 && measuredHeight > 0) {
        viewTreeObserver.removeOnGlobalLayoutListener(this)
        f()
      }
    }
  })
}