package com.example.util.extension

import android.app.Activity
import android.widget.Toast
import androidx.fragment.app.Fragment

fun Activity.clickToast(message: String = "Click") {
  Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Fragment.clickToast(message: String = "Click") {
  Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}