package com.example.util.extension

import android.graphics.Paint
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat


fun TextView.isEllipsized(): Boolean = layout != null && layout.lineCount > maxLines

fun EditText.setSelectionEnd(): Unit = setSelection(text.length)

fun EditText.setTextWithSelectionEnd(text: CharSequence?) {
  setText(text)
  post { setSelectionEnd() }
}

fun TextView.getValue(): String? = this.text?.toString()

fun TextView.replace(oldValue: String, newValue: String, ignoreCase: Boolean = false) {
  this.text = this.getValue()?.replace(oldValue, newValue, ignoreCase)
}

fun TextView.setTextColorIdRes(@ColorRes colorIdRes: Int) {
  setTextColor(ContextCompat.getColor(context, colorIdRes))
}

fun TextView.setStrikeThru() {
  paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}