package com.example.util.extension

import android.view.View
import androidx.core.widget.NestedScrollView

fun NestedScrollView.onScrollEnd(f: () -> Unit) {
  setOnScrollChangeListener { v: NestedScrollView?, _: Int, scrollY: Int, _: Int, _: Int ->
    v ?: return@setOnScrollChangeListener

    val view = v.getChildAt(v.childCount - 1) as View
    val diff = view.bottom - (v.height + scrollY)

    if (diff == 0) {
      f.invoke()
    }
  }
}