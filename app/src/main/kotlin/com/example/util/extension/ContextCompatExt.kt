package com.example.util.extension

import android.app.Activity
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat.*
import androidx.fragment.app.Fragment

fun Activity.getColorRes(@ColorRes resId: Int) = getColor(this, resId)

fun Fragment.getColorRes(@ColorRes resId: Int) = context!!.let { getColor(it, resId) }

fun Activity.getDrawableRes(@DrawableRes resId: Int) = getDrawable(this, resId)

fun Fragment.getDrawableRes(@DrawableRes resId: Int) = context!!.let { getDrawable(it, resId) }