package com.example.util.extension

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

inline fun String.whileMatchThenElse(
    regex: Regex,
    doIfMatch: (String) -> String,
    doIfNotMatch: (String) -> String
): String {
  var result = this

  while (regex.containsMatchIn(result)) {
    result = doIfMatch.invoke(result)
  }

  return doIfNotMatch.invoke(result)
}

inline fun String.ifContainsMatchOrElse(
    regex: Regex,
    doIfMatch: (String) -> String,
    doIfNotMatch: (String) -> String
) = when {
  regex.containsMatchIn(this) -> doIfMatch.invoke(this)
  else -> doIfNotMatch.invoke(this)
}

inline fun String.ifContainsMatch(
    regex: Regex,
    doIfMatch: (String) -> String
) = when {
  regex.containsMatchIn(this) -> doIfMatch.invoke(this)
  else -> this
}

fun String.remove(
    oldVal: String
) = this.replace(oldVal.toRegex(), "")

fun String.remove(
    regex: Regex
) = this.replace(regex, "")

fun String.last4Symbols() = if (length <= 4) this else substring(length - 4, length)

fun String?.formatWithCurrencySymbol(currencySymbol: String?) = String.format(
  "%s%s",
  currencySymbol,
  this
)

fun String?.toSubStrings(subStringMaxLength: Int): List<String> {
  this ?: return emptyList()

  if (length <= subStringMaxLength) {
    return ArrayList<String>().apply { add(this@toSubStrings) }
  }

  val subStringsCount = Math.ceil(length / 4.0).toInt()
  val subStrings = ArrayList<String>(subStringsCount)

  for (i in 0 until subStringsCount) {
    val subStringStartIndex = i * subStringMaxLength
    val subStringEndIndex = subStringStartIndex + subStringMaxLength

    val subString = substring(subStringStartIndex, if (subStringEndIndex > length) length else subStringEndIndex)
    subStrings.add(subString)
  }

  return subStrings
}

fun String.createBoldSpannable(boldText: String, ignoreCase: Boolean = true): SpannableStringBuilder {
  val spannableString = SpannableStringBuilder(this)

  if (this.contains(boldText, ignoreCase)) {
    val startIndex = this.indexOf(boldText, ignoreCase = true)
    val endIndex = startIndex + boldText.length

    spannableString.setSpan(StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
  }

  return spannableString
}

fun String.createHiddenEmail(): String {
  return this.replace("(^[^@]{3}|(?!^)\\G)[^@]".toRegex(), "$1*")
}