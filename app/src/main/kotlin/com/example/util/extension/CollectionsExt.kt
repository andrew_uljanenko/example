package com.example.util.extension

fun <T> List<T>?.isNullOrEmpty() = this?.isEmpty() ?: true
fun <K, V> Map<K, V>?.isNullOrEmpty() = this?.isEmpty() ?: true

fun <T> List<T>?.notNullButEmpty() = this != null && this.isEmpty()
fun <K, V> Map<K, V>?.notNullButEmpty() = this != null && this.isEmpty()