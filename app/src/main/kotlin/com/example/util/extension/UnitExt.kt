package com.example.util.extension

import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import androidx.fragment.app.Fragment

fun Context.dpToPx(value: Int) = (value * (getDensity(this) / DisplayMetrics.DENSITY_DEFAULT)).toInt()
fun Context.dpToPx(value: Float) = (value * (getDensity(this) / DisplayMetrics.DENSITY_DEFAULT)).toInt()

fun Fragment.dpToPx(value: Int) = (value * (getDensity(context!!) / DisplayMetrics.DENSITY_DEFAULT)).toInt()
fun Fragment.dpToPx(value: Float) = (value * (getDensity(context!!) / DisplayMetrics.DENSITY_DEFAULT)).toInt()

fun View.dpToPx(value: Int) = (value * (getDensity(context) / DisplayMetrics.DENSITY_DEFAULT)).toInt()
fun View.dpToPx(value: Float) = (value * (getDensity(context) / DisplayMetrics.DENSITY_DEFAULT)).toInt()

private fun getDensity(context: Context) = context.resources.displayMetrics.density