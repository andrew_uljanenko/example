package com.example.util.extension

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.hideKeyboardOnScroll() {
  addOnScrollListener(object : RecyclerView.OnScrollListener() {
    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
      if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {
        hideKeyboard()
      }
    }
  })
}

@Suppress("UNCHECKED_CAST")
inline fun <T : RecyclerView.Adapter<*>> RecyclerView.initRecyclerViewIfNeeded(initAdapterFun: (recyclerView: RecyclerView) -> T): T {
  return adapter as? T ?: initAdapterFun.invoke(this)
}

fun RecyclerView.onScrollEnd(visibleThreshold: Int = 10, f:() -> Unit) {
  addOnScrollListener(object : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
      super.onScrolled(recyclerView, dx, dy)

      if (dy > 0) {
        val layoutManager = this@onScrollEnd.layoutManager as? LinearLayoutManager ?: return

        val totalItemsCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        if (totalItemsCount <= firstVisibleItemPosition + visibleThreshold) {
          f.invoke()
        }
      }
    }
  })
}