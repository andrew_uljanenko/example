package com.example.util.extension

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.ViewHolder.getContext(): Context = itemView.context

fun RecyclerView.ViewHolder.getString(@StringRes stringResId: Int): String = getContext().getString(stringResId)
fun RecyclerView.ViewHolder.getString(@StringRes stringResId: Int, vararg args: Any): String = getContext().getString(stringResId, *args)
fun RecyclerView.ViewHolder.getColor(@ColorRes colorResId: Int): Int = ContextCompat.getColor(getContext(), colorResId)
fun RecyclerView.ViewHolder.getDrawable(@DrawableRes drawableIdRes: Int): Drawable = ContextCompat.getDrawable(getContext(), drawableIdRes)!!
fun RecyclerView.ViewHolder.getDimensionPixelSize(@DimenRes dimenRes: Int): Int = getContext().resources.getDimensionPixelSize(dimenRes)