package com.example.util.extension

import android.app.Activity
import android.content.Context.*
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment

fun View.hideKeyboard() {
  val imm = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
  imm.hideSoftInputFromWindow(windowToken, 0)
}

fun Activity.hideKeyboard() {
  window?.decorView?.hideKeyboard()
}

fun Fragment.hideKeyboard() {
  activity?.hideKeyboard()
}

fun EditText.openKeyboard() {
  requestFocus()

  val imm = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
  imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}