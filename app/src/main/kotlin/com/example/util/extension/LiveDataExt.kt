package com.example.util.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T : Any> LiveData<T>.observe(lifecycleOwner: LifecycleOwner, f: (T?) -> Unit) {
  observe(lifecycleOwner, Observer { f.invoke(it) })
}