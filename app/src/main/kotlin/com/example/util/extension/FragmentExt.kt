package com.example.util.extension

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection
import java.io.File
import kotlin.reflect.KClass

fun Fragment.daggerInject() = AndroidSupportInjection.inject(this)

fun Fragment.getColor(@ColorRes colorIdRes: Int) = ContextCompat.getColor(context!!, colorIdRes)

fun <T : Activity> Fragment.startNewActivity(clazz: KClass<T>, animate: Boolean = true) {
  startActivityWithFlags(clazz, animate)
}

fun <T : Activity> Fragment.startNewRootActivity(clazz: KClass<T>, animate: Boolean = true) {
  startActivityWithFlags(
    clazz,
    animate,
    Intent.FLAG_ACTIVITY_NEW_TASK,
    Intent.FLAG_ACTIVITY_CLEAR_TASK
  )
}

fun <T : Activity> Fragment.startActivityWithFlags(
    clazz: KClass<T>,
    animate: Boolean = false,
    vararg flags: Int
) {
  if (!animate) {
    activity?.overridePendingTransition(0, 0)
  }

  val intent = Intent(activity, clazz.java)
  for (flag in flags) {
    intent.addFlags(flag)
  }

  startActivity(intent)
}

fun <T : Activity> Fragment.replaceActivity(clazz: KClass<T>, animate: Boolean = true) {
  startNewActivity(clazz)
  activity?.let {
    it.finish()
    if (!animate) {
      it.overridePendingTransition(0, 0)
    }
  }
}

fun Fragment.cacheDir() = context?.cacheDir
fun Fragment.fileDir() = context?.filesDir

fun Fragment.showDialog(
    dialogFragment: DialogFragment,
    fragmentTag: String,
    arguments: Bundle? = null,
    requestCode: Int = -1
) {
  val fm = fragmentManager ?: return
  val ft = fm.beginTransaction()
  val prev = fm.findFragmentByTag(fragmentTag)
  prev?.let { ft.remove(it) }
  ft.addToBackStack(null)

  dialogFragment.arguments = arguments
  dialogFragment.setTargetFragment(this, requestCode)
  dialogFragment.show(ft, fragmentTag)
}

fun Fragment.createImageFile(): File? {
  return try {
    val fileName = System.currentTimeMillis()
      .toString()

    File.createTempFile(fileName, ".jpg", cacheDir())

  } catch (ex: Exception) {
    null
  }
}

fun Fragment.deleteFile(fileUri: Uri?) {
  if (fileUri != null) {
    activity?.contentResolver?.delete(fileUri, null, null)
  }
}

