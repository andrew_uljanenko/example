package com.example.util.extension

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import dagger.android.AndroidInjection

fun Activity.clearLightStatusBar() {
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    val view = window.decorView
    var flags = view.systemUiVisibility
    flags = flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
    view.systemUiVisibility = flags
  }
}

fun Activity.setLightStatusBar() {
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    val view = window.decorView
    var flags = view.systemUiVisibility
    flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    view.systemUiVisibility = flags
    window.statusBarColor = Color.WHITE
  }
}

fun Activity.enableFullScreenMode() {
  window.decorView.apply {
    systemUiVisibility = (
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN
            or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        )
  }
}

fun Activity.daggerInject() {
  AndroidInjection.inject(this)
}

fun Activity.rootView(): View {
  return findViewById<View>(android.R.id.content)
}

fun AppCompatActivity.showDialog(
    dialogFragment: DialogFragment?,
    fragmentTag: String,
    arguments: Bundle? = null
) {
  val dialog = dialogFragment ?: return
  val fm = supportFragmentManager ?: return

  val ft = fm.beginTransaction()
  val prev = fm.findFragmentByTag(fragmentTag)
  prev?.let { ft.remove(it) }
  ft.addToBackStack(null)

  dialog.arguments = arguments
  if (!dialog.isAdded) {
    dialog.show(ft, fragmentTag)
  }
}

fun Activity.getToolbarHeight(): Int {
  val tv = TypedValue()
  return if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true))
    TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
  else 0
}