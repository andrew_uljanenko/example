package com.example.util

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewTreeObserver.*
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.lang.ref.WeakReference

open class KeyboardListenerManager(
    activity: Activity,
    var onKeyboardToggleListener: OnKeyboardToggleListener? = null
) : LifecycleObserver {

  private val contextWeakReference: WeakReference<Activity?> = WeakReference(activity)
  private val onGlobalLayoutListener: OnGlobalLayoutListener = OnGlobalLayoutKeyboardToggleListener()
  private var rootViewWeakReference: WeakReference<View?> = getRootView()
    get() {
      if (field.get() == null) {
        field = getRootView()
      }

      return field
    }

  private var previousBottom: Int = 0


  private fun getRootView(): WeakReference<View?> {
    return WeakReference(contextWeakReference.get()?.findViewById(android.R.id.content))
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
  fun registerListener() {
    rootViewWeakReference
      .get()
      ?.viewTreeObserver
      ?.addOnGlobalLayoutListener(onGlobalLayoutListener)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
  fun unregisterListener() {
    rootViewWeakReference
      .get()
      ?.viewTreeObserver
      ?.removeOnGlobalLayoutListener(onGlobalLayoutListener)
  }


  interface OnKeyboardToggleListener {
    fun onKeyboardToggle(opened: Boolean)
  }

  private inner class OnGlobalLayoutKeyboardToggleListener : OnGlobalLayoutListener {
    override fun onGlobalLayout() {
      val contentView = rootViewWeakReference.get() ?: return
      val r = Rect()

      contentView.getWindowVisibleDisplayFrame(r)
      if (previousBottom == r.bottom) {
        return
      }

      val screenHeight = contentView.rootView.height

      // r.bottom is the position above soft keypad or device button.
      // if keypad is shown, the r.bottom is smaller than that before.
      val keypadHeight = screenHeight - r.bottom

      // 0.15 ratio is perhaps enough to determine keypad height.
      onKeyboardToggleListener?.onKeyboardToggle(keypadHeight > screenHeight * 0.15)
      previousBottom = r.bottom
    }
  }
}
