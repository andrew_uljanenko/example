package com.example.util

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder

object BarcodeUtils {

  fun generateBarcodeBitmap(barcodeValue: String?, width: Int, height: Int): Bitmap? {
    return try {
      //+100px because BarcodeEncoder creates bitmap with white corners
      BarcodeEncoder().encodeBitmap(barcodeValue, BarcodeFormat.EAN_13, width + 100, height)
    } catch (ex: Exception) {
      null
    }
  }
}