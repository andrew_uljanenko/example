package com.example.util.animation

import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator

object Interpolators {
  val ACCELERATE_DECELERATE by lazy { AccelerateDecelerateInterpolator() }
  val LINEAR by lazy { LinearInterpolator() }
  val OVERSHOOT by lazy { OvershootInterpolator() }
  val ANTICIPATE_OVERSHOOT by lazy { AnticipateOvershootInterpolator() }
  val BOUNCE by lazy { BounceInterpolator() }
}