package com.example.util.animation

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import androidx.annotation.ColorInt

typealias OnColorChangeListener = (color: Int) -> Unit

object ColorChangeAnimator {

  fun changeColor(
      @ColorInt startColor: Int,
      @ColorInt newColor: Int,
      listener: OnColorChangeListener
  ) {
    ValueAnimator().apply {
      setIntValues(startColor, newColor)
      setEvaluator(ArgbEvaluator())
      addUpdateListener { valueAnimator ->
        val animatedValue = valueAnimator.animatedValue as Int
        listener.invoke(animatedValue)
      }

      duration = 200
      interpolator = Interpolators.LINEAR
      start()
    }
  }
}
