package com.example.util

import android.os.Build

fun checkVersionNotLessThan(versionCode: Int) = Build.VERSION.SDK_INT >= versionCode

fun checkVersionMoreThan(versionCode: Int) = Build.VERSION.SDK_INT > versionCode

fun checkVersioNotMoreThan(versionCode: Int) = Build.VERSION.SDK_INT <= versionCode

fun checkVersionLessThan(versionCode: Int) = Build.VERSION.SDK_INT < versionCode